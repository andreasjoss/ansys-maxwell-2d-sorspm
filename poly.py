from __future__ import division
from math import pi, sin, asin, cos, sqrt, atan
from cmath import rect #from 2.6
from numpy import append, array, linspace


#def rect(r,phi):
#  """Until Python 2.6 is available"""
#  return r*cos(phi)+1j*r*sin(phi)  
### code commented by Gert Oosthuizen since rect is now available in cmath



def peel(inner_radius,thickness,theta_start=0, theta_end=pi/2, vertices=25):
  """Draws a poly 'peel' with the following parameters:
      inner radius, 'inner_radius',
      outer radius, 'outer_radius',
      thickness, 'thickness',
      starting angle, 'theta_start=0',
      ending angle, 'theta_end=pi/2' and
      number of vertices for each arc, 'vertices=25'"""
  
  phi=linspace(theta_start,theta_end,vertices)
  
  inner_vertices=  array([rect(inner_radius,val) for val in phi])
  outer_vertices=  array([rect(inner_radius+thickness,val) for val in reversed(phi)])
  return append(append(inner_vertices,outer_vertices),inner_vertices[0]) 
  
def peel2(inner_radius,thickness,theta_start=0, theta_end=pi/2, vertices=25):
  """Draws a poly 'peel' with the following parameters:
      inner radius, 'inner_radius',
      outer radius, 'outer_radius',
      thickness, 'thickness',
      starting angle, 'theta_start=0',
      ending angle, 'theta_end=pi/2' and
      number of vertices for each arc, 'vertices=25'"""
  
  thetaB= pi-(theta_end-theta_start)/2
  radExtend = sqrt(pow(inner_radius,2)+pow(thickness,2)-inner_radius*thickness*2*cos(thetaB))
  thetaC = asin((thickness*sin(thetaB))/radExtend)
  #phi=linspace(theta_start,theta_end,vertices)
  cplx1= rect(inner_radius,theta_start)
  cplx2= rect(inner_radius,theta_end)
  thetaDiff=theta_end-(thetaC.real)
  cplx3= rect(radExtend,thetaDiff)
  cplx4= rect(radExtend,(theta_start+thetaC))
  inner_vertices=  array([cplx1,cplx2])#array([rect(inner_radius,val) for val in phi])
  outer_vertices=  array([cplx3, cplx4])#array([rect(inner_radius+thickness,val) for val in reversed(phi)])
  return append(append(inner_vertices,outer_vertices),inner_vertices[0])
  
def peel2o(inner_radius,thickness,theta_start=0, theta_end=pi/2, vertices=25):
  """Draws a poly 'peel' with the following parameters:
      inner radius, 'inner_radius',
      outer radius, 'outer_radius',
      thickness, 'thickness',
      starting angle, 'theta_start=0',
      ending angle, 'theta_end=pi/2' and
      number of vertices for each arc, 'vertices=25'"""
  thetaA= (theta_end-theta_start)/2
  a1 = inner_radius*sin(thetaA)
  r1 = sqrt(pow(inner_radius,2)+pow(a1,2))
  thetaB= atan(a1/r1)
  r2 = sqrt(pow(inner_radius+thickness,2)+pow(a1,2))
  thetaC = atan(a1/r2)
  #phi=linspace(theta_start,theta_end,vertices)
  cplx1= rect(r1,theta_start+thetaA-thetaB)
  cplx2= rect(r1,theta_start+thetaA+thetaB)
  cplx3= rect(r2,theta_start+thetaA+thetaC)
  cplx4= rect(r2,theta_start+thetaA-thetaC)
  inner_vertices=  array([cplx1,cplx2])#array([rect(inner_radius,val) for val in phi])
  outer_vertices=  array([cplx3, cplx4])#array([rect(inner_radius+thickness,val) for val in reversed(phi)])
  return append(append(inner_vertices,outer_vertices),inner_vertices[0])
  
####peel for middle magnets  
def peel2m(angle, inner_radius,thickness,theta_start=0, theta_end=pi/2, vertices=25):
  """Draws a poly 'peel' with the following parameters:
      inner radius, 'inner_radius',
      outer radius, 'outer_radius',
      thickness, 'thickness',
      starting angle, 'theta_start=0',
      ending angle, 'theta_end=pi/2' and
      number of vertices for each arc, 'vertices=25'"""
  
  thetaB=(angle)*(pi/180)
  radExtend = sqrt(pow(inner_radius,2)+pow(thickness,2)-inner_radius*thickness*2*cos(thetaB))
  thetaC = asin((thickness*sin(thetaB))/radExtend)
  #phi=linspace(theta_start,theta_end,vertices)
  cplx1= rect(inner_radius,theta_start)
  cplx2= rect(inner_radius,theta_end)
  cplx3= rect(radExtend,theta_end+thetaC)
  cplx4= rect(radExtend,(theta_start-thetaC))
  inner_vertices=  array([cplx1,cplx2])#array([rect(inner_radius,val) for val in phi])
  outer_vertices=  array([cplx3, cplx4])#array([rect(inner_radius+thickness,val) for val in reversed(phi)])
  return append(append(inner_vertices,outer_vertices),inner_vertices[0])
  
def peel2mo(angle, inner_radius,thickness,theta_start=0, theta_end=pi/2, vertices=25):
  """Draws a poly 'peel' with the following parameters:
      inner radius, 'inner_radius',
      outer radius, 'outer_radius',
      thickness, 'thickness',
      starting angle, 'theta_start=0',
      ending angle, 'theta_end=pi/2' and
      number of vertices for each arc, 'vertices=25'"""
  
  thetaB=(180-angle)*(pi/180)
  radExtend = sqrt(pow(inner_radius,2)+pow(thickness,2)-inner_radius*thickness*2*cos(thetaB))
  thetaC = asin((thickness*sin(thetaB))/radExtend)
  #phi=linspace(theta_start,theta_end,vertices)
  cplx1= rect(inner_radius,theta_start)
  cplx2= rect(inner_radius,theta_end)
  cplx3= rect(radExtend,theta_end+thetaC)
  cplx4= rect(radExtend,(theta_start-thetaC))
  inner_vertices=  array([cplx1,cplx2])#array([rect(inner_radius,val) for val in phi])
  outer_vertices=  array([cplx3, cplx4])#array([rect(inner_radius+thickness,val) for val in reversed(phi)])
  return append(append(inner_vertices,outer_vertices),inner_vertices[0])  
  

def slice(radius,theta_start=0, theta_end=pi/2, vertices=25):
  """Draws a poly 'slice' with the following parameters:
      radius, 'radius',
      starting angle, 'theta_start=0',
      ending angle, 'theta_end=pi/2' and
      number of vertices for the arc, 'vertices=25'"""
  phi=linspace(theta_start,theta_end,vertices)
  vertices=array([rect(radius,val) for val in phi])
  return append(append(array([0]),vertices),array([0]))

def arc(radius,theta_start=0, theta_end=pi/2, vertices=25):
  """Draws a poly 'arc' with the following parameters:
      radius, 'radius',
      starting angle, 'theta_start=0',
      ending angle, 'theta_end=pi/2' and
      number of vertices, 'vertices=25'"""
  phi=linspace(theta_start,theta_end,vertices)
  return array([rect(radius,val) for val in phi])
