# -*- coding: utf-8 -*-
from __future__ import division

from math import pi

middleMagnetAngle = 0
thicknessFactor = 1.0
# ############ #
# Machine Data #
# ############ #

rfapm_type= 2									# type machine, 0, 1 (I) or 2 (II)
ONESIDED = False              # magnets on the inner rotor True/False
IPM = 0.0

p   			= 28 								# the total number of poles (i.e. magnets) on the inner/outer rotor yoke

k_q 			= 1/4   						# coils-per-phase to number-of-poles ratio

Q   			= int(p*k_q*3)  		# total number of coils

k_m 			= 0.5   						# the magnet angle to pole-pitch angle ratio

r_n 			= 120.  						# nominal stator radius                  [mm]
l         =  40.              # active stack/copper lenth              [mm]

w         = 22.4729*((r_n)/232)*(18/Q)          # coil side width                        [mm]
Delta     = w/(2*r_n)         # coil side width angle                  [rad]
Delta_max = pi/(2*Q)					# maximum coil side-width angle          [rad]
#k_Delta   = Delta/Delta_max   # coil side-width factor
k_Delta   = 0.71757

h   			=  9.0 #10.0*(130/232) 						# height/thickness of the stator coils     [mm]
h_m 			=   5.5 #8.2*(130/232) 					  # magnet height/thickness                      [mm]
h_iy 			=   9.0 #8.0*(100/232)  	 	  		  # yoke height/thickness                              [mm]
h_oy 			=   9.0 #8.0*(100/232) 						  # yoke height/thickness                  [mm]
l_g 			=   1.0 #*(130/232)  						# air gap length                           [mm]
h_b 			=  15.0*(80/232)  						# boundary height/thickness                [mm]

##Auto-Calculate turns
#r_o=r_n+(h/2)
#r_i=r_n-(h/2)
#
#A_c = ((r_o**2)-(r_i**2))*(Delta)*0.7
#
#d_wire = 1.32
#
#a_wire = pi*(d_wire/2)**2

N = 19 #= A_c//a_wire
#N=154
coilMaterial = "copper"

#N         =  110               # number of turns per coil               [turns]
#a         =   1               # number of parallel branches

n         = 290               # [rated] speed                          [rpm]
f         = n*p/120           # [rated] frequency                      [Hz]

mag_name  ="NdBFe N48"        # the name of permanent magnets used
H_c       = 1050000           # the maximum coercivity force           [A/m]
B_r       =  1.4              # the average remanent flux density      [T]

I_p       = 20             # [rated] peak current value per phase   [A]