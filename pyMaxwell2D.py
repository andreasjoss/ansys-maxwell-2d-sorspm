#!/usr/bin/python

class Design(object):
	"""A Python wrapper fot Ansoft's:
	'InsertDesign' AxtiveX command"""

class Polyline(object):
  """A Python wrapper for Ansoft Maxwell2D's:
	'Polyline' AxtiveX command"""
  
  def __init__(cls, cplx_points_array,
                     IsPolylineCovered=False,
                     IsPolylineClosed=False,
                     Name="Polyline",
                     Flags="",
                     Color=(132, 132, 193),
                     Transparency=0.5,
                     PartCoordinateSystem="Global",
                     MaterialName="vacuum",
                     SolveInside=True):
    """Initialises the Maxwell Polyline object"""
    
    #Initialising all the Parameters
    cls._cplx_points_array=cplx_points_array
    cls._IsPolylineCovered=IsPolylineCovered
    cls._IsPolylineClosed=IsPolylineClosed
    cls._PointsArray=cls._update_PointsArray(cplx_points_array)
    cls._SegmentsArray=cls._update_SegmentsArray(cplx_points_array)
    cls._ParametersArray=cls._update_ParametersArray()
    
    #Initialising all the Attributes
    cls._Name=Name
    cls._Flags=Flags
    cls._Color=Color
    cls._Transparency=Transparency
    cls._PartCoordinateSystem=PartCoordinateSystem
    cls._MaterialName=MaterialName
    cls._SolveInside=SolveInside
    cls._AttributesArray=cls._update_AttributesArray()


  def _update_PointsArray(cls,cplx_points_array):
    """Updating the PointsArray"""
    
    PointsArray=["NAME:PolylinePoints"]
    for cplx_point in cplx_points_array:
      PointsArray.append(["NAME:PLPoint", "X:=", "%smm" % cplx_point.real, "Y:=", "%smm" % cplx_point.imag, "Z:=", "0mm"])
    return PointsArray


  def _update_SegmentsArray(cls,cplx_points_array):
    """Updating the SegmentsArray"""
    SegmentsArray=["NAME:PolylineSegments"]
    for segment in range(len(cplx_points_array)-1):
      SegmentsArray.append(["NAME:PLSegment", "SegmentType:=", "Line", "StartIndex:=", segment, "NoOfPoints:=", 2])
    return SegmentsArray


  def _update_ParametersArray(cls):
    """Updating the ParametersArray"""
    
    return ["NAME:PolylineParameters",
            "IsPolylineCovered:=", cls._IsPolylineCovered,
            "IsPolylineClosed:=", cls._IsPolylineClosed,
            cls._PointsArray,
            cls._SegmentsArray]


  def _update_AttributesArray(cls):
    """Updating the AttributesArray"""
    
    return ["NAME:Attributes",
            "Name:=", cls._Name,
            "Flags:=", cls._Flags,
            "Color:=", "(%d %d %d)" % cls._Color,
            "Transparency:=", cls._Transparency,
            "PartCoordinateSystem:=", cls._PartCoordinateSystem,
            "MaterialName:=", cls._MaterialName,
            "SolveInside:=", cls._SolveInside]

  
  def _get_cplx_points_array(cls):
    return cls._cplx_points_array
  
  
  def _set_cplx_points_array(cls,new_cplx_points_array):
    cls._cplx_points_array=new_cplx_points_array
    cls._PointsArray=cls._update_PointsArray(cplx_points_array)
    cls._SegmentsArray=cls._update_SegmentsArray(cplx_points_array)
    cls._ParametersArray=cls._update_ParametersArray()

  
  cplx_points_array=property(fget=_get_cplx_points_array,
                             fset=_set_cplx_points_array,
                             doc="""the numpy complex points array""")

  
  def _get_IsPolylineCovered(cls):
    return cls._IsPolylineCovered
  
  
  def _set_IsPolylineCovered(cls,IsPolylineCovered):
    cls._IsPolylineCovered=IsPolylineCovered
    cls._ParametersArray=cls._update_ParametersArray()

  
  IsPolylineCovered=property(fget=_get_IsPolylineCovered,
                             fset=_set_IsPolylineCovered,
                             doc="""is the surface area covered or not""")

  
  def _get_IsPolylineClosed(cls):
    return cls._IsPolylineClosed
  
  
  def _set_IsPolylineClosed(cls,IsPolylineClosed):
    cls._IsPolylineClosed=IsPolylineClosed
    cls._ParametersArray=cls._update_ParametersArray()

  
  IsPolylineClosed=property(fget=_get_IsPolylineClosed,
                             fset=_set_IsPolylineClosed,
                             doc="""is the polyline closed or not""")


  def _get_ParametersArray(cls):
    return cls._ParametersArray
  
  
  def _set_ParametersArray(cls,*args,**kwargs):
    print "this is a read only property"

  
  ParametersArray=property(fget=_get_ParametersArray,
                             fset=_set_ParametersArray,
                             doc="""the Ansoft Maxwell COM ParametersArray""")


  def _get_Name(cls):
    return cls._Name
  
  
  def _set_Name(cls,Name):
    cls._Name=Name
    cls._AttributesArray=cls._update_AttributesArray()
  
  
  Name=property(fget=_get_Name,
                fset=_set_Name,
                doc="""the name of the polygon""")
  
  
  def _get_Flags(cls):
    return cls._Flags
  
  
  def _set_Flags(cls,Flags):
    cls._Flags=Flags
    cls._AttributesArray=cls._update_AttributesArray()
    
  
  Flags=property(fget=_get_Flags,
                 fset=_set_Flags,
                 doc="""polygon flags""")
  
  
  def _get_Color(cls):
    return cls._Color
  
  
  def _set_Color(cls,Color):
    cls._Color=Color
    cls._AttributesArray=cls._update_AttributesArray()
    
  
  Color=property(fget=_get_Color,
                 fset=_set_Color,
                 doc="""the color of the polygon""")
  
  
  def _get_Transparency(cls):
    return cls._Transparency
  
  
  def _set_Transparency(cls,Transparency):
    cls._Transparency=Transparency
    cls._AttributesArray=cls._update_AttributesArray()
    
  
  Transparency=property(fget=_get_Transparency,
                        fset=_set_Transparency,
                        doc="""the transparency fo the polygon""")
  
  
  def _get_PartCoordinateSystem(cls):
    return cls._PartCoordinateSystem
  
  
  def _set_PartCoordinateSystem(cls,PartCoordinateSystem):
    cls._PartCoordinateSystem=PartCoordinateSystem
    cls._AttributesArray=cls._update_AttributesArray()
    
  
  PartCoordinateSystem=property(fget=_get_PartCoordinateSystem,
                                fset=_set_PartCoordinateSystem,
                                doc="""the polygons coordinate system""")
  
  
  def _get_MaterialName(cls):
    return cls._MaterialName
  
  
  def _set_MaterialName(cls,MaterialName):
    cls._MaterialName=MaterialName
    cls._AttributesArray=cls._update_AttributesArray()
    
  
  MaterialName=property(fget=_get_MaterialName,
                        fset=_set_MaterialName,
                        doc="""the name of the material of the polygon""")
  
  
  def _get_SolveInside(cls):
    return cls._SolveInside
  
  
  def _set_SolveInside(cls,SolveInside):
    cls._SolveInside=SolveInside
    cls._AttributesArray=cls._update_AttributesArray()

  
  SolveInside=property(fget=_get_SolveInside,
                       fset=_set_SolveInside,
                       doc="""should Maxwell solve inside the polygon or not""")


  def _get_AttributesArray(cls):
    return cls._AttributesArray
  
  
  def _set_AttributesArray(cls,*args,**kwargs):
    print "this is a read only property"


  AttributesArray=property(fget=_get_AttributesArray,
                           fset=_set_AttributesArray,
                           doc="""the Ansoft Maxwell COM AttributesArray""")


  def GetEdges(cls, oEditor):
    edges=[]
    pnts=cls._cplx_points_array
    for i in range(len(pnts)-1):
      centre_pnt=pnts[i]+(pnts[i+1]-pnts[i])/2
      edges.append(oEditor.GetEdgeByPosition(["NAME:EdgeParameters", "BodyName:=", "%s" % cls._Name, "XPosition:=", "%smm" % centre_pnt.real, "YPosition:=", "%smm" % centre_pnt.imag, "ZPosition:=", "0mm"]))
    return edges


  def Create(cls,oEditor):
    oEditor.CreatePolyline(cls.ParametersArray,cls.AttributesArray)
    return cls.GetEdges(oEditor)
    
    
class Magnetic_Coercivity(object):
	def __init__(cls, Magnitude=-923000,
										 DirComp1=1.0,
										 DirComp2=0.0,
										 DirComp3=0.0):
		cls.Magnitude=Magnitude
		cls.DirComp1=DirComp1
		cls.DirComp2=DirComp2
		cls.DirComp3=DirComp3
    
	def __get__(cls, obj, objtype):
		return ["NAME:magnetic_coercivity",
						"property_type:=",  "VectorProperty",
						"Magnitude:=", "%dA_per_meter" % cls.Magnitude,
						"DirComp1:=", "%g" % cls.DirComp1,
						"DirComp2:=", "%g" % cls.DirComp2,
						"DirComp3:=", "%g" % cls.DirComp3]
	
class Material(object):
  def __init__(cls, CoordinateSystemType="Cylindrical",
                     Relative_Permeability=1.0,
                     Bulk_Conductivity=625000,
                     Magnetic_Coercivity=Magnetic_Coercivity()):
    cls.CoordinateSystemType=CoordinateSystemType
    cls.Relative_Permeability=Relative_Permeability
    cls.Bulk_Conductivity=Bulk_Conductivity
    cls.Magnetic_Coercivity=Magnetic_Coercivity
    
  def AddMaterial(cls,oDefinitionManager, material_name):
    oDefinitionManager.AddMaterial(["NAME:%s" % material_name,
                                    "CoordinateSystemType:=", "%s" % cls.CoordinateSystemType,
                                   ["NAME:AttachedData"],
                                   ["NAME:ModifierData"],
                                    "permeability:=",  "%s" % cls.Relative_Permeability,
                                    "conductivity:=", "%s" % cls.Bulk_Conductivity,
                                   ["NAME:magnetic_coercivity",
                                    "property_type:=", "VectorProperty",
                                    "Magnitude:=", "%dA_per_meter" % cls.Magnetic_Coercivity.Magnitude,
                                    "DirComp1:=", "%g" % cls.Magnetic_Coercivity.DirComp1,
                                    "DirComp2:=", "%g" % cls.Magnetic_Coercivity.DirComp2,
                                    "DirComp3:=", "%g" % cls.Magnetic_Coercivity.DirComp3]])


class EddyEffect(object):
	def __init__(cls, module):
		cls.Module=module
		cls.Vector=["NAME:EddyEffectVector"]
		
	def Append(cls, name, calc):
		cls.Vector.append(["NAME:Data", "Object Name:=", "%s" % name, "Eddy Effect:=", calc])
		
	def Set(cls):
			cls.Module.SetEddyEffect(["NAME:Eddy Effect Setting",cls.Vector])
 
 

 