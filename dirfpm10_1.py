from __future__ import division

from math import pi, radians
from numpy import append, exp, linspace

from farey import farey

from win32com.client import Dispatch

import pyMaxwell2D
import poly

from test_machine_di10_1 import *
import numpy as np
#from numpy import *

a=1
#I_p=I_p/8

RED    = (255,0,0)
YELLOW = (255,255,0)
BLUE   = (0,0,255)
GRAY   = (192,192,192)
BROWN  = (139,69,19)

Type='II'

# ############### #
# Simulation Data #
# ############### #
PM      = False        # permanent magnets switched "ON"
AR      = True          # windings switched "ON"
WIND    = True           # draw windings
LMC     = True          # inductance matrix calculations
CLIP_COILS = True

if (not PM) and (not AR):
  raise ValueError, "Either 'PM' must be 'True' or 'AR' must be 'True' or both..."
if AR and (not WIND):
  WIND = True

if Type=='O':
  k_Delta=1.0
  k_q=0.5
  N=64

if PM and not AR:
  I_p=0                       # if I_p=0 - the windings are effectively switched "OFF"

sim_slice = 2/p if Type=='O' else 4/p   # (minimum) simulation slice

sim_time  = (1/f)/2               # simulation end time
sim_steps = 50            # number of simulation time steps between 0 and sim_time
out_steps = 50             # number of field and output variable steps


# ################################# #
# Calculation of Machine Parameters #
# ################################# #

theta_p 	= 2*pi/p						    # pole pitch angle	[rad]

n         = 120*f/p               # speed                                  [rpm]

q         = int(p*k_q)            # number of coils per phase
Q   			= 3*q  		              # total number of coils

Delta_max = pi/(2*Q)					    # maximum coil side-width
Delta			= k_Delta*Delta_max	    # coil side-width angle [rad]

h_ip=h_m    # inner pole thickness
h_op=h_m    # outer pole thickness

# ############################################# #
# Calculation of Simulation Geometry Parameters #
# ############################################# #

alpha = 0.0 if Type=='II' \
  else (Delta_max-Delta)                 # 1st coil's starting angle

theta = (4*pi/Q-pi/p)*(p/2)+2*pi/3 if Type=='O' \
  else -(pi/Q-pi/p)*(p/2)                # phase current's phase angle for a PF=1.0

hipg_seg,mag_seg=farey((1-k_m)/(2*k_m))  # calculate the half inter-pole gap number of segment &
                                         # the magnet number of segment

#print('hipg_seg=%g' % hipg_seg)

k_m       = mag_seg/(2*hipg_seg+mag_seg) # calculate the revised k_m value#
mag_seg = 4
theta_m 	= theta_p*k_m 			           # the "revised" magnet's arc angle  [rad]

# ############## #
# Some Functions #
# ############## #

def dq_to_abc(u_a,u_b,u_c,theta):
    u_d = (2/3)*(u_a*np.cos(theta) + u_b*np.cos(theta-(2*np.pi)/3) + u_c*np.cos(theta+(2*np.pi)/3))
    u_q = (2/3)*(-u_a*np.sin(theta) - u_b*np.sin(theta-(2*np.pi)/3) - u_c*np.sin(theta+(2*np.pi)/3))
    u_0 = (1/3)*(u_a + u_b + u_c)
    return u_d,u_q,u_0

# ###################### #
# Start Ansoft Maxwell2D #
# ###################### #

oAnsoftApp=Dispatch("AnsoftMaxwell.MaxwellScriptInterface")
#oAnsoftApp.SetNumberOfProcessors(2, "Maxwell 2D")

oDesktop=oAnsoftApp.GetAppDesktop()
oDesktop.RestoreWindow()

oProject=oDesktop.NewProject()

oDesign=oProject.InsertDesign("Maxwell 2D", "RFAPM", "Transient", "")

oDesign.SetDesignSettings(["NAME:Design Settings Data",
                           "PreserveTranSolnAfterDatasetEdit:=", False,
                           "ComputeTransientInductance:=", LMC,
                           "PerfectConductorThreshold:=", 1E+030,
                           "InsulatorThreshold:=", 1,
                           "ModelDepth:=", "%gmeter" % (l/1000),
                           "EnableTranTranLinkWithSimplorer:=", False,
                           "BackgroundMaterialName:=", "vacuum",
                           "Multiplier:=", "%d" % (int(round(1/sim_slice)))])
                              
#oDesign.ChangeProperty(["NAME:AllTabs",
#                        ["NAME:LocalVariableTab",
#                         ["NAME:PropServers", "LocalVariables"],
#                         ["NAME:NewProps",
#                          ["NAME:p", "PropType:=", "VariableProp", "UserDef:=", True, "Value:=", "16"]],
#                         ["NAME:ChangedProps",
#                          ["NAME:p", "Description:=", "Number of Poles"]]]])

oEditor=oDesign.SetActiveEditor("3D Modeler")
#oEditor.SetModelUnits("mm")
oEditor.SetModelUnits(["NAME:Units Parameter", "Units:=", "mm"])
oEditor.PageSetup(["NAME:PageSetupData",
                   "margins:=", ["left:=", 500, "right:=", 500,  "top:=", 500,  "bottom:=", 500],
                   "border:=", 1,
                   "DesignVars:=", 0])

oModule=oDesign.GetModule("BoundarySetup")

EddyEffectVector=pyMaxwell2D.EddyEffect(oModule)
  # ######################## #
  # Create Magnetic Material #
  # ######################## #

if PM:

  oDefinitionManager=oProject.GetDefinitionManager()
  
  mu_0=4*pi*1E-7
  mu_recoil=B_r/(mu_0*H_c)
  
  # Create Magnet in the Cylindrical Coordinate System with a Positive Radial Magnitisation
  mag_name_pos_rad=mag_name+" (+R)"
  if not oDefinitionManager.DoesMaterialExist("%s" % mag_name_pos_rad):
    oDefinitionManager.AddMaterial(["NAME:%s" % mag_name_pos_rad,
                                    "CoordinateSystemType:=",  "Cylindrical",
                                    ["NAME:AttachedData"],
                                    ["NAME:ModifierData"],
                                    "permeability:=", "%g" % mu_recoil,
                                    ["NAME:magnetic_coercivity",
                                     "property_type:=", "VectorProperty",
                                     "Magnitude:=", "-%gA_per_meter" % H_c,
                                     "DirComp1:=", "1",
                                     "DirComp2:=", "0",
                                     "DirComp3:=", "0"]])
                                     
  # Create Magnet in the Cylindrical Coordinate System with a Negative Radial Magnitisation
  mag_name_neg_rad=mag_name+" (-R)"
  if not oDefinitionManager.DoesMaterialExist("%s" % mag_name_neg_rad):
    oDefinitionManager.AddMaterial(["NAME:%s" % mag_name_neg_rad,
                                    "CoordinateSystemType:=",  "Cylindrical",
                                    ["NAME:AttachedData"],
                                    ["NAME:ModifierData"],
                                    "permeability:=", "%g" % mu_recoil,
                                    ["NAME:magnetic_coercivity",
                                     "property_type:=", "VectorProperty",
                                     "Magnitude:=", "-%gA_per_meter" % H_c,
                                     "DirComp1:=", "-1",
                                     "DirComp2:=", "0",
                                     "DirComp3:=", "0"]])
                                     
   # material with positive circumferential magnetisation.                                  
  mag_name_pos_ang=mag_name+" (+A)"
  if not oDefinitionManager.DoesMaterialExist("%s" % mag_name_pos_ang):
    oDefinitionManager.AddMaterial(["NAME:%s" % mag_name_pos_ang,
                                    "CoordinateSystemType:=",  "Cylindrical",
                                    ["NAME:AttachedData"],
                                    ["NAME:ModifierData"],
                                    "permeability:=", "%g" % mu_recoil,
                                    ["NAME:magnetic_coercivity",
                                     "property_type:=", "VectorProperty",
                                     "Magnitude:=", "-%gA_per_meter" % H_c,
                                     "DirComp1:=", "0",
                                     "DirComp2:=", "1",
                                     "DirComp3:=", "0"]])
                                   

   # material with negative circumferential magnetisation.                                  
  mag_name_neg_ang=mag_name+" (-A)"
  if not oDefinitionManager.DoesMaterialExist("%s" % mag_name_neg_ang):
    oDefinitionManager.AddMaterial(["NAME:%s" % mag_name_neg_ang,
                                    "CoordinateSystemType:=",  "Cylindrical",
                                    ["NAME:AttachedData"],
                                    ["NAME:ModifierData"],
                                    "permeability:=", "%g" % mu_recoil,
                                    ["NAME:magnetic_coercivity",
                                     "property_type:=", "VectorProperty",
                                     "Magnitude:=", "-%gA_per_meter" % H_c,
                                     "DirComp1:=", "0",
                                     "DirComp2:=", "-1",
                                     "DirComp3:=", "0"]])
# ############ #
# Draw Magnets #
# ############ #

# Compute Inner Magnet's Complex Coordinates
inner_mag_cplx_pnts=poly.peel(r_n-h/2-l_g-h_ip, h_ip, (theta_p-theta_m)/2-pi*sim_slice, (theta_p-theta_m)/2+theta_m-pi*sim_slice, vertices=mag_seg+1)

# Compute Outer Magnet's Complex Coordinates
outer_mag_cplx_pnts=poly.peel(r_n+h/2+l_g, h_op, (theta_p-theta_m)/2-pi*sim_slice, (theta_p-theta_m)/2+theta_m-pi*sim_slice, vertices=mag_seg+1)

# Compute outer Middle Magnet's complex Coordinates.
outer_mid_mag_cplx_pnts = poly.peel(r_n+h/2+l_g+(h_ip*IPM), h_ip*thicknessFactor, (theta_p-theta_m)/2+theta_m-pi*sim_slice, (theta_p-theta_m)/2+(1.0/k_m)*theta_m-pi*sim_slice, vertices=mag_seg+1)


# Compute inner Middle Magnet's complex Coordinates.
inner_mid_mag_cplx_pnts = poly.peel(r_n-h/2-l_g-h_ip*(1+IPM), h_ip*thicknessFactor, (theta_p-theta_m)/2+theta_m-pi*sim_slice, (theta_p-theta_m)/2+(1.0/k_m)*theta_m-pi*sim_slice, vertices=mag_seg+1)
#-(h_ip*1.1)


inner_magnet_names=[]
outer_magnet_names=[]
inner_mid_magnet_names=[]
outer_mid_magnet_names=[]
# Draws all Inner & Outer Magnets
for i in range(int(p*sim_slice)):
  if (i % 2):
    # Odd pair of magnets
    if Type=='O':
      color=RED
      magnet_material="vacuum" if not PM else mag_name_pos_rad
    else:
      color=BLUE
      magnet_material="vacuum" if not PM else mag_name_neg_rad
  else:
    # Even pair of magnets
    if Type=='O':
      color=BLUE
      magnet_material="vacuum" if not PM else mag_name_neg_rad
    else:
      color=RED
      magnet_material="vacuum" if not PM else mag_name_pos_rad

  inner_magnet_name="Inner_Magnet_%d" % (i)
  inner_magnet_names.append(inner_magnet_name)
  
  InnerMagnet=pyMaxwell2D.Polyline(inner_mag_cplx_pnts,
    IsPolylineCovered=True,
    IsPolylineClosed=True,
    Name=inner_magnet_name,
    Color=color,
    MaterialName=magnet_material)
      
  InnerMagnet.Create(oEditor)
  
  EddyEffectVector.Append(inner_magnet_name,True)
  
  # Shift inner poles one pole pitch
  inner_mag_cplx_pnts*=exp(2j*pi/p)
      
  outer_magnet_name="Outer_Magnet_%d" % (i)
  outer_magnet_names.append(outer_magnet_name)
  
  OuterMagnet=pyMaxwell2D.Polyline(outer_mag_cplx_pnts,
    IsPolylineCovered=True,
    IsPolylineClosed=True,
    Name=outer_magnet_name,
    Color=color,
    MaterialName=magnet_material)

  OuterMagnet.Create(oEditor)

  EddyEffectVector.Append(outer_magnet_name,True)

  # Shift outer poles one pole pitch
  outer_mag_cplx_pnts*=exp(2j*pi/p)
  

#Draws middle magnets

for i in range(int(p*sim_slice)-1):
  if (i % 2):
    # Odd pair of magnets
    color=BLUE
    magnet_material="vacuum" if not PM else mag_name_pos_ang
  else:
    # Even pair of magnets
    color=RED
    magnet_material="vacuum" if not PM else mag_name_neg_ang

  inner_mid_magnet_name="Inner_mid_Magnet_%d" % (i)
  inner_mid_magnet_names.append(inner_mid_magnet_name)
  
  InnerMagnet=pyMaxwell2D.Polyline(inner_mid_mag_cplx_pnts,
    IsPolylineCovered=True,
    IsPolylineClosed=True,
    Name=inner_mid_magnet_name,
    Color=color,
    MaterialName=magnet_material)
      
  InnerMagnet.Create(oEditor)
  
  EddyEffectVector.Append(inner_mid_magnet_name,True)
  
  # Shift inner poles one pole pitch
  inner_mid_mag_cplx_pnts*=exp(2j*pi/p)
  
  if (i % 2):
    # Odd pair of magnets
    color=RED
    magnet_material="vacuum" if not PM else mag_name_neg_ang
  else:
    # Even pair of magnets
    color=BLUE
    magnet_material="vacuum" if not PM else mag_name_pos_ang
    
  outer_mid_magnet_name="Outer_mid_Magnet_%d" % (i)
  outer_mid_magnet_names.append(outer_mid_magnet_name)
  
  OuterMagnet=pyMaxwell2D.Polyline(outer_mid_mag_cplx_pnts,
    IsPolylineCovered=True,
    IsPolylineClosed=True,
    Name=outer_mid_magnet_name,
    Color=color,
    MaterialName=magnet_material)

  OuterMagnet.Create(oEditor)

  EddyEffectVector.Append(outer_mid_magnet_name,True)

  # Shift outer poles one pole pitch
  outer_mid_mag_cplx_pnts*=exp(2j*pi/p)
  
  
#Draw half middle magnets on ends of slice  


#first half magnets
# Compute outer Middle Magnet's complex Coordinates.
outer_mid_mag_cplx_pnts = poly.peel(r_n+h/2+l_g+(h_ip*IPM), h_ip*thicknessFactor, (theta_p-theta_m)/2-pi*sim_slice-(1-k_m)*theta_m, (theta_p-theta_m)/2-pi*sim_slice, vertices=mag_seg+1)
# Compute inner Middle Magnet's complex Coordinates.
inner_mid_mag_cplx_pnts = poly.peel(r_n-h/2-l_g-h_ip*(1+IPM), h_ip*thicknessFactor, (theta_p-theta_m)/2-pi*sim_slice-(1-k_m)*theta_m, (theta_p-theta_m)/2-pi*sim_slice, vertices=mag_seg+1)

# first inner half magnet
magnet_material="vacuum" if not PM else mag_name_pos_ang
inner_mid_magnet_name = "Inner_mid_Magnet_00"
inner_mid_magnet_names.append(inner_mid_magnet_name)
  
InnerMagnet=pyMaxwell2D.Polyline(inner_mid_mag_cplx_pnts,
  IsPolylineCovered=True,
  IsPolylineClosed=True,
  Name=inner_mid_magnet_name,
  Color=color,
  MaterialName=magnet_material)    
  
InnerMagnet.Create(oEditor)
  
EddyEffectVector.Append(inner_mid_magnet_name,True)  

#first outer half magnet
magnet_material="vacuum" if not PM else mag_name_neg_ang
outer_mid_magnet_name = "Outer_mid_Magnet_00"
outer_mid_magnet_names.append(outer_mid_magnet_name)
 
OuterMagnet=pyMaxwell2D.Polyline(outer_mid_mag_cplx_pnts,
  IsPolylineCovered=True,
  IsPolylineClosed=True,
  Name=outer_mid_magnet_name,
  Color=color,
  MaterialName=magnet_material)

OuterMagnet.Create(oEditor)

EddyEffectVector.Append(outer_mid_magnet_name,True)
color = BROWN
#last half magnets
# Compute outer Middle Magnet's complex Coordinates.
outer_mid_mag_cplx_pnts = poly.peel(r_n+h/2+l_g+(h_ip*IPM), h_ip*thicknessFactor, pi*sim_slice-(theta_p-theta_m)/2+(1-k_m)*theta_m, pi*sim_slice-(theta_p-theta_m)/2, vertices=mag_seg+1)
# Compute inner Middle Magnet's complex Coordinates.
inner_mid_mag_cplx_pnts = poly.peel(r_n-h/2-l_g-h_ip*(1+IPM), h_ip*thicknessFactor, pi*sim_slice-(theta_p-theta_m)/2+(1-k_m)*theta_m, pi*sim_slice-(theta_p-theta_m)/2, vertices=mag_seg+1)

# first inner half magnet
magnet_material="vacuum" if not PM else mag_name_pos_ang
inner_mid_magnet_name = "Inner_mid_Magnet_01"
inner_mid_magnet_names.append(inner_mid_magnet_name)
  
InnerMagnet=pyMaxwell2D.Polyline(inner_mid_mag_cplx_pnts,
  IsPolylineCovered=True,
  IsPolylineClosed=True,
  Name=inner_mid_magnet_name,
  Color=color,
  MaterialName=magnet_material)    
  
InnerMagnet.Create(oEditor)
  
EddyEffectVector.Append(inner_mid_magnet_name,True)  

#first outer half magnet
magnet_material="vacuum" if not PM else mag_name_neg_ang
outer_mid_magnet_name = "Outer_mid_Magnet_01"
outer_mid_magnet_names.append(outer_mid_magnet_name)
 
OuterMagnet=pyMaxwell2D.Polyline(outer_mid_mag_cplx_pnts,
  IsPolylineCovered=True,
  IsPolylineClosed=True,
  Name=outer_mid_magnet_name,
  Color=color,
  MaterialName=magnet_material)

OuterMagnet.Create(oEditor)

EddyEffectVector.Append(outer_mid_magnet_name,True)

  
magnet_names=inner_mid_magnet_names+outer_mid_magnet_names+inner_magnet_names+outer_magnet_names
  
#####  
##Shapes to subtract from half magnets
halfSubtractPnts = poly.peel(r_n-h/2-l_g-h_ip-h_iy, h_iy*5,pi*sim_slice,pi*sim_slice*2,20)

halfSub1=pyMaxwell2D.Polyline(halfSubtractPnts,
  IsPolylineCovered=True,
  IsPolylineClosed=True,
  Name="halfSub1",
  MaterialName="vacuum")

halfSubtractPnts = poly.peel(r_n-h/2-l_g-h_ip-h_iy, h_iy*5,-pi*sim_slice,-pi*sim_slice*2,20)

halfSub2=pyMaxwell2D.Polyline(halfSubtractPnts,
  IsPolylineCovered=True,
  IsPolylineClosed=True,
  Name="halfSub2",
  MaterialName="vacuum")

halfSub1.Create(oEditor)
halfSub2.Create(oEditor)
  
oEditor=oDesign.SetActiveEditor("3D Modeler")
oEditor.Subtract(["NAME:Selections", 
                  "Blank Parts:=", 
                  "Inner_mid_Magnet_00,Inner_mid_Magnet_01,Outer_mid_Magnet_00,Outer_mid_Magnet_01", 
                  "Tool Parts:=", 
                  "halfSub1,halfSub2"], 
                  ["NAME:SubtractParameters", "KeepOriginals:=", False])  


# ######### #
# Draw Yoke #
# ######### #

inner_yoke_name="Inner_Yoke"
outer_yoke_name="Outer_Yoke"

yoke_material_name="vacuum"

# Inner Yoke 
inner_yoke_cplx_pnts=poly.peel(r_n-h/2-l_g-h_ip-h_iy, h_iy, -pi*sim_slice, pi*sim_slice, vertices=(2*hipg_seg+mag_seg)*int(sim_slice*p)+1)

InnerYoke=pyMaxwell2D.Polyline(inner_yoke_cplx_pnts,
  IsPolylineCovered=True,
  IsPolylineClosed=True,
  Name=inner_yoke_name,
  MaterialName=yoke_material_name)

# Outer Yoke
outer_yoke_cplx_pnts=poly.peel(r_n+h/2+l_g+h_op, h_oy, -pi*sim_slice, pi*sim_slice, vertices=(2*hipg_seg+mag_seg)*int(sim_slice*p)+1)

OuterYoke=pyMaxwell2D.Polyline(outer_yoke_cplx_pnts,
  IsPolylineCovered=True,
  IsPolylineClosed=True,
  Name=outer_yoke_name,
  MaterialName=yoke_material_name)

InnerYoke.Create(oEditor)
OuterYoke.Create(oEditor)

EddyEffectVector.Append(inner_yoke_name,True)
EddyEffectVector.Append(outer_yoke_name,True)

magnetNameString = "Inner_Magnet_0,Inner_Magnet_1,Inner_Magnet_2,Inner_Magnet_3,Outer_Magnet_0,Outer_Magnet_1,Outer_Magnet_2,Outer_Magnet_3,Inner_mid_Magnet_0,Inner_mid_Magnet_00,Inner_mid_Magnet_01,Inner_mid_Magnet_1,Inner_mid_Magnet_2,Outer_mid_Magnet_0,Outer_mid_Magnet_00,Outer_mid_Magnet_01,Outer_mid_Magnet_1,Outer_mid_Magnet_2"
oEditor=oDesign.SetActiveEditor("3D Modeler")
oEditor.Subtract(["NAME:Selections", 
                  "Blank Parts:=", 
                  "Inner_Yoke,Outer_Yoke", 
                  "Tool Parts:=", 
                  "%s" % magnetNameString], 
                  ["NAME:SubtractParameters", "KeepOriginals:=", True])

# ################### #
# Draw Rotor Surfaces #
# ################### #

inner_rotor_cplx_pnts=poly.peel(r_n-h/2-l_g-h_ip-h_iy-l_g/2, h_iy+h_ip+l_g, -pi*sim_slice, pi*sim_slice, vertices=(2*hipg_seg+mag_seg)*int(sim_slice*p)+1)

Inner_Rotor=pyMaxwell2D.Polyline(inner_rotor_cplx_pnts,
  IsPolylineCovered=True,
  IsPolylineClosed=True,
  Name="InnerRotor",
  Color=GRAY)

Inner_Rotor.Create(oEditor)

outer_rotor_cplx_pnts=poly.peel(r_n+h/2+l_g/2, h_oy+h_op+l_g, -pi*sim_slice, pi*sim_slice, vertices=(2*hipg_seg+mag_seg)*int(sim_slice*p)+1)

Outer_Rotor=pyMaxwell2D.Polyline(outer_rotor_cplx_pnts,
  IsPolylineCovered=True,
  IsPolylineClosed=True,
  Name="OuterRotor",
  Color=GRAY)

Outer_Rotor.Create(oEditor)

if WIND:
  # ################ #	
  # Windings & Coils #
  # ################ #
  oModule.AssignWindingGroup(["NAME:Winding_A",
                              "Type:=", "Current",
                              "IsSolid:=", False,
                              "Current:=", "%g*cos(2*pi*%g*Time)" % (I_p,f),
                              "ParallelBranchesNum:=", "%d" % (a)])

  oModule.AssignWindingGroup(["NAME:Winding_B",
                              "Type:=", "Current",
                              "IsSolid:=", False,
                              "Current:=", "%g*cos(2*pi*%g*Time-2*pi/3)" % (I_p,f),
                              "ParallelBranchesNum:=", "%d" % (a)])

  oModule.AssignWindingGroup(["NAME:Winding_C",
                              "Type:=", "Current",
                              "IsSolid:=", False,
                              "Current:=", "%g*cos(2*pi*%g*Time-4*pi/3)" % (I_p,f),
                              "ParallelBranchesNum:=", "%d" % (a)])

  # Compute first Coil's Complex Coordinates
  coil_cplx_pnts=poly.peel(r_n-h/2, h, alpha-pi*sim_slice, alpha+2*Delta-pi*sim_slice, vertices=5)

  if Type=='O':
    LABEL=['Bn', 'Ap', 'Cn', 'Bp', 'An', 'Cp']
    COLOR=[YELLOW, RED, BLUE, YELLOW, RED, BLUE]
  else:
    LABEL=['Bp', 'Bn', 'Ap', 'An', 'Cp', 'Cn']
    COLOR=[BLUE, BLUE, RED, RED, YELLOW, YELLOW]

  circuit=-1

  coil_names=[]
  for i in range(int(2*Q*sim_slice)):
    i = i-6 if i>=6 else i
    circuit = circuit+1 if (i % 6 == 0) else circuit
    coil_name='Coil_%s%d' % (LABEL[i % 6],circuit)
    coil_names.append(coil_name)
    coil_color=COLOR[i % 6]
    coil_polarity='Positive' if coil_name[-2]=='p' else 'Negative'
    coil_phase=coil_name[-3]
    
    if CLIP_COILS:
      if Type=='II':
        if i%2==0:
          coil_cplx_pnts*=exp(1j*radians(0.05))
        else:
          coil_cplx_pnts*=exp(-1j*radians(0.05))
    
    CoilPolyline=pyMaxwell2D.Polyline(coil_cplx_pnts,
      IsPolylineCovered=True,
      IsPolylineClosed=True,
      Name=coil_name,
      Color=coil_color,
      MaterialName="%s" % coilMaterial)
    
    CoilPolyline.Create(oEditor)

    if CLIP_COILS:
      if Type=='II':
        if i%2==0:
          coil_cplx_pnts*=exp(-1j*radians(0.05))
        else:
          coil_cplx_pnts*=exp(1j*radians(0.05))
      
    oModule.AssignCoil(["NAME:%s" % coil_name,
                        "Objects:=", ["%s" % coil_name],
                        "Conductor number:=", "%s" % N,
                        "PolarityType:=", "%s" % coil_polarity])

    oModule.EditCoil("%s" % coil_name,
                     ["NAME:%s" % coil_name,
                      "ParentBndID:=", "Winding_%s" % coil_phase,
                      "Winding:=", "Winding_%s" % coil_phase])
    
    EddyEffectVector.Append(coil_name,True)

    if Type=='II':
      if coil_polarity=='Negative':
        coil_cplx_pnts*=exp(1j*2*Delta)
      else:
        coil_cplx_pnts*=exp(2j*(pi/Q-Delta))
    else:
      coil_cplx_pnts*=exp(1j*pi/Q)


# ############## #	
# Stator Surface #
# ############## #
statvert=(2*hipg_seg+mag_seg)*int(sim_slice*p)+1

stator_cplx_pnts=poly.peel(r_n-h/2-l_g/2, h+l_g, -pi*sim_slice, pi*sim_slice, vertices=statvert)

Stator=pyMaxwell2D.Polyline(stator_cplx_pnts,
  IsPolylineCovered=True,
  IsPolylineClosed=True,
  Name="Stator",
  Color=GRAY)

Stator.Create(oEditor)

EddyEffectVector.Append("Stator",True)

EddyEffectVector.Set()

# ################# #	
# Moving Band Setup #
# ################# #

oModule=oDesign.GetModule("ModelSetup")

oModule.AssignBand(["NAME:Data",
                    "Move Type:=", "Rotate",
                    "Coordinate System:=", "Global",
                    "Axis:=", "Z",
                    "Is Positive:=", True,
                    "InitPos:=", "0deg",
                    "HasRotateLimit:=", False,
                    "NegativePos:=", "0deg",
                    "PositivePos:=", "-90deg",
                    "NonCylindrical:=", False,
                    "Consider Mechanical Transient:=", False,
                    "Angular Velocity:=", "%grpm" % n,
                    "Objects:=", ["InnerRotor"]])

oModule.AssignBand(["NAME:Data",
                    "Move Type:=", "Rotate",
                    "Coordinate System:=", "Global",
                    "Axis:=", "Z",
                    "Is Positive:=", True,
                    "InitPos:=", "0deg",
                    "HasRotateLimit:=", False,
                    "NegativePos:=", "0deg",
                    "PositivePos:=", "-90deg",
                    "NonCylindrical:=", False,
                    "Consider Mechanical Transient:=", False,
                    "Angular Velocity:=", "%grpm" % n,
                    "Objects:=", ["OuterRotor"]])

# ################# #
# Flux Density Arcs #
# ################# #
arcvert=int((statvert-1)/2)

Br_rn_cplx_pnts=poly.arc(r_n,-pi*sim_slice,pi*sim_slice,vertices=arcvert)
Br_rn=pyMaxwell2D.Polyline(Br_rn_cplx_pnts,True,False,"Br_rn",GRAY)
Br_rn.Create(oEditor)

Br_cim_cplx_pnts=poly.arc(r_n-h/2-l_g-h_m/2,-pi*sim_slice,pi*sim_slice,vertices=arcvert)
Br_cim=pyMaxwell2D.Polyline(Br_cim_cplx_pnts,True,False,"Br_cim",GRAY)
Br_cim.Create(oEditor)

Br_com_cplx_pnts=poly.arc(r_n+h/2+l_g+h_m/2,-pi*sim_slice,pi*sim_slice,vertices=arcvert)
Br_com=pyMaxwell2D.Polyline(Br_com_cplx_pnts,True,False,"Br_com",GRAY)
Br_com.Create(oEditor)    

if not WIND:
  Br_r_ciy_cplx_pnts=poly.arc(r_n-h/2-l_g-h_ip-h_iy/2,-pi*sim_slice,pi*sim_slice,vertices=arcvert*8)
  Br_r_ciy=pyMaxwell2D.Polyline(Br_r_ciy_cplx_pnts,True,False,"Br_r_ciy",GRAY)
  Br_r_ciy.Create(oEditor)

  Br_r_coy_cplx_pnts=poly.arc(r_n+h/2+l_g+h_op+h_oy/2,-pi*sim_slice,pi*sim_slice,vertices=arcvert*8)
  Br_r_coy=pyMaxwell2D.Polyline(Br_r_coy_cplx_pnts,True,False,"Br_r_coy",GRAY)
  Br_r_coy.Create(oEditor)

  Br_rnph2_cplx_pnts=poly.arc(r_n+h/2,-pi*sim_slice,pi*sim_slice,vertices=arcvert)
  Br_rnph2=pyMaxwell2D.Polyline(Br_rnph2_cplx_pnts,True,False,"Br_rnph2",GRAY)
  Br_rnph2.Create(oEditor)

  Br_rnmh2_cplx_pnts=poly.arc(r_n-h/2,-pi*sim_slice,pi*sim_slice,vertices=arcvert)
  Br_rnmh2=pyMaxwell2D.Polyline(Br_rnmh2_cplx_pnts,True,False,"Br_rnmh2",GRAY)
  Br_rnmh2.Create(oEditor)
  
  Br_rnmh14_cplx_pnts=poly.arc(r_n-h/4,-pi*sim_slice,pi*sim_slice,vertices=arcvert)
  Br_rnmh14=pyMaxwell2D.Polyline(Br_rnmh14_cplx_pnts,True,False,"Br_rnmh14",GRAY)
  Br_rnmh14.Create(oEditor)
  
  Br_rnph14_cplx_pnts=poly.arc(r_n+h/4,-pi*sim_slice,pi*sim_slice,vertices=arcvert)
  Br_rnph14=pyMaxwell2D.Polyline(Br_rnph14_cplx_pnts,True,False,"Br_rnph14",GRAY)
  Br_rnph14.Create(oEditor)

# ########## #	
# Boundaries #
# ########## #

boundary_cplx_pnts=poly.peel(r_n-h/2-l_g-h_ip-h_iy-h_b, h+h_ip+h_op+h_iy+h_oy+(l_g+h_b)*2, -pi*sim_slice, pi*sim_slice, vertices=int(80*sim_slice+1))

Boundary=pyMaxwell2D.Polyline(boundary_cplx_pnts,
  IsPolylineCovered=True,
  IsPolylineClosed=True,
  Name="Boundary",
  Color=GRAY)

boundary_edges=Boundary.Create(oEditor)

num_boundary_edges=len(boundary_edges)
slave_boundary_edge=boundary_edges.pop(num_boundary_edges//2-1)
master_boundary_edge=boundary_edges.pop(-1)

master_boudary_name="Master"
slave_boudary_name="Slave"

oModule=oDesign.GetModule("BoundarySetup")

oModule.AssignMaster(["NAME:%s" % (master_boudary_name),
                      "Edges:=", [master_boundary_edge],
                      "ReverseV:=", True])

oModule.AssignSlave(["NAME:%s" % (slave_boudary_name),
                     "Edges:=", [slave_boundary_edge],
                     "ReverseU:=", False,
                     "Master:=", "%s" % (master_boudary_name),
                     "SameAsMaster:=", True])

oModule.AssignVectorPotential(["NAME:VectorPotential",
                               "Edges:=", boundary_edges,
                               "Value:=", "0",
                               "CoordinateSystem:=", ""])

# ########## #	
# Mesh Setup #
# ########## #

# Lines to mesh on
if PM and AR:
  M_inner_cplx_pnts = poly.arc(r_n - h/2 - l_g/2, -pi*sim_slice,pi*sim_slice,(2*hipg_seg+mag_seg)*4+1)
  M_inner = pyMaxwell2D.Polyline(M_inner_cplx_pnts,True,False,"M_inner",GRAY)
  M_inner.Create(oEditor)

  M_outer_cplx_pnts = poly.arc(r_n + h/2 + l_g/2, -pi*sim_slice,pi*sim_slice,(2*hipg_seg+mag_seg)*4+1)
  M_outer = pyMaxwell2D.Polyline(M_outer_cplx_pnts,True,False,"M_outer",GRAY)
  M_outer.Create(oEditor)

oModule=oDesign.GetModule("MeshSetup")

if PM and AR:
  oModule.AssignLengthOp(["NAME:LineMesh",
                          "RefineInside:=", False,
                          "Objects:=", ["M_inner", "M_outer"],
                          "RestrictElem:=", False,
                          "NumMaxElem:=", "5000",
                          "RestrictLength:=", True,
                          "MaxLength:=", "%gmm" % (0.2)])

oModule.AssignLengthOp(["NAME:Boundary",
                        "RefineInside:=", False,
                        "Objects:=", ["Boundary"],
                        "RestrictElem:=", False,
                        "NumMaxElem:=", "1000",
                        "RestrictLength:=", True,
                        "MaxLength:=", "%gmm" %(h_iy/2)])
                        
oModule.AssignLengthOp(["NAME:InnerYoke",
                        "RefineInside:=", False,
                        "Objects:=", [inner_yoke_name],
                        "RestrictElem:=", False,
                        "NumMaxElem:=", "1000",
                        "RestrictLength:=", True,
                        "MaxLength:=", "%gmm" % (h_iy/8)])

oModule.AssignLengthOp(["NAME:OuterYoke",
                        "RefineInside:=", False,
                        "Objects:=", [outer_yoke_name],
                        "RestrictElem:=", False,
                        "NumMaxElem:=", "1000",
                        "RestrictLength:=", True,
                        "MaxLength:=", "%gmm" % (h_oy/8)])

if PM:
  oModule.AssignLengthOp(["NAME:Magnets",
                          "RefineInside:=", False,
                          "Objects:=", magnet_names,
                          "RestrictElem:=", False,
                          "NumMaxElem:=", "1000",
                          "RestrictLength:=", True,
                          "MaxLength:=", "%gmm" % (h_m/2)])

if WIND:
  oModule.AssignLengthOp(["NAME:Coils",
                          "RefineInside:=", False,
                          "Objects:=", coil_names,
                          "RestrictElem:=", False,
                          "NumMaxElem:=", "1000",
                          "RestrictLength:=", True,
                          "MaxLength:=", "%gmm" % (h/2)])

oModule.AssignLengthOp(["NAME:Stator", 
                        "RefineInside:=", False,
                        "Objects:=", ["Stator"],
                        "RestrictElem:=", False,
                        "NumMaxElem:=", "1000",
                        "RestrictLength:=", True,
                        "MaxLength:=", "%gmm" % (h/10)])

  #oModule.AssignLengthOp(["NAME:Inner_Air_Gap", 
  #                        "RefineInside:=", False,
  #                        "Objects:=", ["Inner_Air_Gap"],
  #                        "RestrictElem:=", False,
  #                        "NumMaxElem:=", "1000",
  #                        "RestrictLength:=", True,
  #                        "MaxLength:=", "%gmm" % (l_g/2)])

# # oModule.AssignLengthOp(["NAME:Outer_Air_Gap", 
  #                        "RefineInside:=", False,
  #                        "Objects:=", ["Outer_Air_Gap"],
  #                        "RestrictElem:=", False,
  #                        "NumMaxElem:=", "1000",
  #                        "RestrictLength:=", True,
  #                        "MaxLength:=", "%gmm" % (l_g/2)])

#


# ############## #	
# Analysis Setup #
# ############## #

oModule=oDesign.GetModule("AnalysisSetup")

oModule.InsertSetup("Transient",
                    ["NAME:Setup1",
                     "NonlinearSolverResidual:=",  1e-6,
                     "TimeIntegrationMethod:=", 0,
                     "StopTime:=", "%g" % (sim_time),
                     "TimeStep:=",  "%g" % (sim_time/sim_steps),
                     "OutputError:=", False,
                     "UseControlProgram:=", False,
                     "ControlProgramName:=", "",
                     "ControlProgramArg:=", " ",
                     "CallCtrlProgAfterLastStep:=", False,
                     "HasSweepSetup:=",  True,
                     "SweepSetupType:=", "LinearStep",
                     "StartValue:=", "0s",
                     "StopValue:=", "%g" % (sim_time),
                     "StepSize:=", "%g" % (sim_time/out_steps),
                     "OutputVarCalTimeStep:=", "%g" % (sim_time/out_steps),
                     "OutputVarCalNumOfSolveSteps:=", 10,
                     "OutputVarCalTimeMethod:=", 0,
                     "NumberOfOutputVars:=", 0,
                     "TransientHcNonLinearBH:=", True,
                     "TransientComputeHc:=", False,
                     "PreAdaptMesh:=", False,
                     "UseAdaptiveTimeStep:=", False,
                     "InitialTimeStep:=", "0.0002s",
                     "MinTimeStep:=", "0.0001s",
                     "MaxTimeStep:=", "0.0003s",
                     "TimeStepErrTolerance:=", 0.00001])


# ################################## #
# Save project with unique file name #
# ################################## #

# pathname="D:/M/simulations/Maxwell/10/"
pathname="D:/Documents/Meesters/Ansys_Maxwell_Simulations/2d/results/"
filename="DIRFPM-10_1"
#filename+=Type
filename+="_l=%.2f" % l
filename+="_r=%.2f" % r_n
filename+="_Ip=%g" % I_p
#filename+="_IPM=%.2f" % IPM
#filename+="_km=%.2f" % k_m
filename+="_N=%g" % N
filename+="_hm=%.1f" % h_m
filename+="_p=%g" % p
filename+="_hc=%.1f" % h
#filename+="_n=%g" % n
#filename+="_coils=%s" % coilMaterial
filename+="_n=%g" % n
filename+="_w=%g" % w


oDesktop.RestoreWindow()
if not WIND:
    filename+="_noWIND"

if not PM:
    filename+="_noPM"

#if not(PM and AR):
#  if PM and WIND:
#    filename+="_PM"
#  elif PM and not WIND:
#    filename+="_NO_WIND"
#  else:
#   filename+="_AR"

#if CLIP_COILS:
#  filename+="_CLIP"
  
oProject.SaveAs("%s%s.mxwl" % (pathname,filename), True)
#oProject.SaveAs("C:\Users\Gert\Documents\akademies\finale jaar\SKRIPSIE\simData")

# #################################################################
# Close the project and reopen it in order to see the field plots #
# #################################################################
oProject.Close()
oDesktop = oAnsoftApp.GetAppDesktop()
#oDesktop.RestoreWindow
oDesktop.OpenProject("%s%s.mxwl" % (pathname,filename))
oProject = oDesktop.SetActiveProject(filename)
oDesign = oProject.SetActiveDesign("RFAPM")
# #################################################################

oDesign.AnalyzeAll()

oModule = oDesign.GetModule("FieldsReporter")

# ################## #
# Mesh Display Setup #
# ################## #

oModule.CreateFieldPlot(["NAME:Mesh1",
                         "SolutionName:=", "Setup1 : Transient",
                         "QuantityName:=", "Mesh",
                         "PlotFolder:=", "MeshPlots",
                         "FieldType:=", "Fields",
                         "UserSpecifyName:=", 0,
                         "UserSpecifyFolder:=", 0,
                         "IntrinsicVar:=", "Time='-1s'",
                         "PlotGeomInfo:=", [1, "Surface", "CutPlane", 1, "Global:XY"],
                         "FilterBoxes:=", [0],
                         "Real time mode:=", True,
                         ["NAME:MeshSettings",
                          "Scale factor:=", 100,
                          "Transparency:=", 0,
                          "Mesh type:=",  "Shaded",
                          "Surface only:=", True,
                          "Add grid:=", True,
                          "Mesh line color:=", [0, 0, 255],
                          "Filled color:=", [255, 255, 255]]])

oModule.SetPlotsViewSolutionContext(["Mesh1"], "Setup1 : Transient","Time='0s'")

# ####################### #
# Flux Line Display Setup #
# ####################### #

oModule.CreateFieldPlot(["NAME:Flux_Lines1",
                         "SolutionName:=", "Setup1 : Transient",
                         "QuantityName:=", "Flux_Lines",
                         "PlotFolder:=", "A",
                         "UserSpecifyName:=", 0,
                         "UserSpecifyFolder:=", 0,
                         "IntrinsicVar:=", "Time='0.00325s''",
                         "PlotGeomInfo:=", [1, "Surface", "CutPlane", 1, "Global:XY"],
                         "FilterBoxes:=", [0],
                         ["NAME:PlotOnSurfaceSettings",
                          "Filled:=", False,
                          "IsoValType:=", "Line",
                          "SmoothShade:=", True,
                          "AddGrid:=", False,
                          "MapTransparency:=", True,
                          "Refinement:=", 0,
                          "Transparency:=", 0,
                          ["NAME:Arrow3DSpacingSettings",
                           "ArrowUniform:=", True,
                           "ArrowSpacing:=", 0,
                           "MinArrowSpacing:=", 0,
                           "MaxArrowSpacing:=", 0],
                          "GridColor:=", [255, 255, 255]]])

oModule.SetPlotsViewSolutionContext(["Flux_Lines1"], "Setup1 : Transient","Time='0s'")

# ############################# #
# Flux Density Display Settings #
# ############################# #

oModule.CreateFieldPlot(["NAME:Mag_B1",
                         "SolutionName:=", "Setup1 : Transient",
                         "QuantityName:=", "Mag_B",
                         "PlotFolder:=", "B",
                         "UserSpecifyName:=", 0,
                         "UserSpecifyFolder:=", 0,
                         "IntrinsicVar:=", "Time='0s'",
                         "PlotGeomInfo:=", [1, "Surface", "CutPlane", 1, "Global:XY"],
                         "FilterBoxes:=", [0],
                         ["NAME:PlotOnSurfaceSettings",
                          "Filled:=", False,
                          "IsoValType:=", "Fringe",
                          "SmoothShade:=", True,
                          "AddGrid:=", False,
                          "MapTransparency:=", True,
                          "Refinement:=", 0,
                          "Transparency:=", 0,
                          ["NAME:Arrow3DSpacingSettings",
                           "ArrowUniform:=", True,
                           "ArrowSpacing:=", 0,
                           "MinArrowSpacing:=", 0,
                           "MaxArrowSpacing:=", 0],
                          "GridColor:=", [255, 255, 255]]])

oModule.SetPlotsViewSolutionContext(["Mag_B1"], "Setup1 : Transient","Time='0s'")

# ############################### #
# Radial Flux Density Caluclation #
# ############################### #
  
oModule.CalcStack("clear")
oModule.EnterQty("B")
oModule.CalcOp("ScalarX")
oModule.EnterScalarFunc("PHI")
oModule.CalcOp("Cos")
oModule.CalcOp("*")
oModule.EnterQty("B")
oModule.CalcOp("ScalarY")
oModule.EnterScalarFunc("PHI")
oModule.CalcOp("Sin")
oModule.CalcOp("*")
oModule.CalcOp("+")
oModule.AddNamedExpression("B_radial", "Fields")

oModule.CopyNamedExprToStack("B_radial")
oModule.EnterLine("Br_rn")
oModule.CalcOp("Integrate")
oModule.EnterScalar(1)
oModule.EnterLine("Br_rn")
oModule.CalcOp("Integrate")
oModule.CalcOp("/")
oModule.AddNamedExpression("Br_rn", "Fields")

oModule.CopyNamedExprToStack("B_radial")
oModule.EnterLine("Br_cim")
oModule.CalcOp("Integrate")
oModule.EnterScalar(1)
oModule.EnterLine("Br_cim")
oModule.CalcOp("Integrate")
oModule.CalcOp("/")
oModule.AddNamedExpression("Br_cim", "Fields")

oModule.CopyNamedExprToStack("B_radial")
oModule.EnterLine("Br_com")
oModule.CalcOp("Integrate")
oModule.EnterScalar(1)
oModule.EnterLine("Br_com")
oModule.CalcOp("Integrate")
oModule.CalcOp("/")
oModule.AddNamedExpression("Br_com", "Fields")

if not WIND:
  oModule.CopyNamedExprToStack("B_radial")
  oModule.EnterLine("Br_r_ciy")
  oModule.CalcOp("Integrate")
  oModule.EnterScalar(1)
  oModule.EnterLine("Br_r_ciy")
  oModule.CalcOp("Integrate")
  oModule.CalcOp("/")
  oModule.AddNamedExpression("Br_r_ciy", "Fields")

  oModule.CopyNamedExprToStack("B_radial")
  oModule.EnterLine("Br_r_coy")
  oModule.CalcOp("Integrate")
  oModule.EnterScalar(1)
  oModule.EnterLine("Br_r_coy")
  oModule.CalcOp("Integrate")
  oModule.CalcOp("/")
  oModule.AddNamedExpression("Br_r_coy", "Fields")

  oModule.CopyNamedExprToStack("B_radial")
  oModule.EnterLine("Br_rnph2")
  oModule.CalcOp("Integrate")
  oModule.EnterScalar(1)
  oModule.EnterLine("Br_rnph2")
  oModule.CalcOp("Integrate")
  oModule.CalcOp("/")
  oModule.AddNamedExpression("Br_rnph2", "Fields")

  oModule.CopyNamedExprToStack("B_radial")
  oModule.EnterLine("Br_rnmh2")
  oModule.CalcOp("Integrate")
  oModule.EnterScalar(1)
  oModule.EnterLine("Br_rnmh2")
  oModule.CalcOp("Integrate")
  oModule.CalcOp("/")
  oModule.AddNamedExpression("Br_rnmh2", "Fields")
  
  oModule.CopyNamedExprToStack("B_radial")
  oModule.EnterLine("Br_rnmh14")
  oModule.CalcOp("Integrate")
  oModule.EnterScalar(1)
  oModule.EnterLine("Br_rnmh14")
  oModule.CalcOp("Integrate")
  oModule.CalcOp("/")
  oModule.AddNamedExpression("Br_rnmh14", "Fields")
  
  oModule.CopyNamedExprToStack("B_radial")
  oModule.EnterLine("Br_rnph14")
  oModule.CalcOp("Integrate")
  oModule.EnterScalar(1)
  oModule.EnterLine("Br_rnph14")
  oModule.CalcOp("Integrate")
  oModule.CalcOp("/")
  oModule.AddNamedExpression("Br_rnph14", "Fields")

# ################################## #
# Azimuthal Flux Density Caluclation #
# ################################## #
  
oModule.CalcStack("clear")
oModule.EnterQty("B")
oModule.CalcOp("ScalarX")
oModule.EnterScalarFunc("PHI")
oModule.CalcOp("Sin")
oModule.CalcOp("*")
oModule.EnterQty("B")
oModule.CalcOp("ScalarY")
oModule.EnterScalarFunc("PHI")
oModule.CalcOp("Cos")
oModule.CalcOp("*")
oModule.CalcOp("-")
oModule.AddNamedExpression("B_azimuthal", "Fields")

oModule.CopyNamedExprToStack("B_azimuthal")
oModule.EnterLine("Br_rn")
oModule.CalcOp("Integrate")
oModule.EnterScalar(1)
oModule.EnterLine("Br_rn")
oModule.CalcOp("Integrate")
oModule.CalcOp("/")
oModule.AddNamedExpression("Bt_ag", "Fields")

oModule.CopyNamedExprToStack("B_azimuthal")
oModule.EnterLine("Br_cim")
oModule.CalcOp("Integrate")
oModule.EnterScalar(1)
oModule.EnterLine("Br_cim")
oModule.CalcOp("Integrate")
oModule.CalcOp("/")
oModule.AddNamedExpression("Bt_mci", "Fields")

oModule.CopyNamedExprToStack("B_azimuthal")
oModule.EnterLine("Br_com")
oModule.CalcOp("Integrate")
oModule.EnterScalar(1)
oModule.EnterLine("Br_com")
oModule.CalcOp("Integrate")
oModule.CalcOp("/")
oModule.AddNamedExpression("Bt_mco", "Fields")

if not WIND:
  oModule.CopyNamedExprToStack("B_azimuthal")
  oModule.EnterLine("Br_r_ciy")
  oModule.CalcOp("Integrate")
  oModule.EnterScalar(1)
  oModule.EnterLine("Br_r_ciy")
  oModule.CalcOp("Integrate")
  oModule.CalcOp("/")
  oModule.AddNamedExpression("Bt_r_ciy", "Fields")

  oModule.CopyNamedExprToStack("B_azimuthal")
  oModule.EnterLine("Br_r_coy")
  oModule.CalcOp("Integrate")
  oModule.EnterScalar(1)
  oModule.EnterLine("Br_r_coy")
  oModule.CalcOp("Integrate")
  oModule.CalcOp("/")
  oModule.AddNamedExpression("Bt_r_coy", "Fields")


  # ####################### #
  # Torque Rectangular Plot #
  # ####################### #

if PM and AR:
  oModule = oDesign.GetModule("OutputVariable")

  oModule.CreateOutputVariable("Output_Torque",
                               "Moving1.Torque+Moving2.Torque",
                               "Setup1 : Transient", "Transient",
                               [])

  oModule = oDesign.GetModule("ReportSetup")

  torque_plot_name="Torque Rectangular Plot"
  oModule.CreateReport("%s" % torque_plot_name,
                       "Transient",
                       "Rectangular Plot",
                       "Setup1 : Transient",
                       [],
                       ["Time:=", ["All"]],
                       ["X Component:=", "Time",
                        "Y Component:=", ["-Output_Torque"]],
                       [])

  oModule.ChangeProperty(["NAME:AllTabs",
                          ["NAME:Scaling",
                           ["NAME:PropServers", "%s:AxisY1" % torque_plot_name],
                           ["NAME:ChangedProps",
                            ["NAME:Specify Min", "Value:=", True],
                            ["NAME:Min", "MustBeInt:=", False, "Value:=", "-10"]]]])


oModule = oDesign.GetModule("ReportSetup")

if WIND:
  # ######################## #
  # Winding Rectangular Plot #
  # ######################## #

  winding_data_plot_name="Winding Rectangular Plot"

  oModule.CreateReport("%s" % winding_data_plot_name,
                       "Transient",
                       "Rectangular Plot",
                       "Setup1 : Transient",
                       ["Domain:=", "Sweep"],
                       ["Time:=", ["All"]],
                       ["X Component:=", "Time",
                        "Y Component:=", ["FluxLinkage(Winding_A)",
                                          "FluxLinkage(Winding_B)",
                                          "FluxLinkage(Winding_C)",
                                          "InducedVoltage(Winding_A)",
                                          "InducedVoltage(Winding_B)",
                                          "InducedVoltage(Winding_C)",
                                          "InputCurrent(Winding_A)",
                                          "InputCurrent(Winding_B)",
                                          "InputCurrent(Winding_C)"]],
                       [])

if LMC:
  # ################################## #
  # Inductance Matrix Rectangular Plot #
  # ################################## #

  inductance_matrix_data_plot_name="Inductance Matrix Rectangular Plot"

  oModule.CreateReport("%s" % inductance_matrix_data_plot_name,
                       "Transient",
                       "Rectangular Plot",
                       "Setup1 : Transient",
                       ["Domain:=", "Sweep"],
                       ["Time:=", ["All"]],
                       ["X Component:=", "Time",
                        "Y Component:=", ["L(Winding_A,Winding_A)",
                                          "L(Winding_A,Winding_B)",
                                          "L(Winding_A,Winding_C)",
                                          "L(Winding_B,Winding_A)",
                                          "L(Winding_B,Winding_B)",
                                          "L(Winding_B,Winding_C)",
                                          "L(Winding_C,Winding_A)",
                                          "L(Winding_C,Winding_B)",
                                          "L(Winding_C,Winding_C)"]],
                       [])
  
# ##################################### #
# Radial Flux Density Rectangular  Plot #
# ##################################### #

radial_flux_density_plot_name="Radial Flux Density Rectangular Plot"

oModule.CreateReport("%s" % radial_flux_density_plot_name,
                     "Fields",
                     "Rectangular Plot",
                     "Setup1 : Transient",
                     ["Context:=", "Br_rn", "PointCount:=", 721, "Domain:=", "Sweep"],
                     ["Distance:=", ["All"],
                      "Time:=", ["0.00s"]],
                     ["X Component:=", "normalize(Distance)",
                      "Y Component:=", ["B_radial"]],
                     [])

oModule.AddTraces("%s" % radial_flux_density_plot_name,
                  "Setup1 : Transient",
                  ["Context:=", "Br_cim", "PointCount:=", 721, "Domain:=", "Sweep"],
                  ["Distance:=", ["All"],
                   "Time:=", ["0.00s"]],
                  ["X Component:=", "normalize(Distance)",
                   "Y Component:=", ["B_radial"]],
                  [])

oModule.AddTraces("%s" % radial_flux_density_plot_name,
                  "Setup1 : Transient",
                  ["Context:=", "Br_com", "PointCount:=", 721, "Domain:=", "Sweep"],
                  ["Distance:=", ["All"],
                   "Time:=", ["0.00s"]],
                  ["X Component:=", "normalize(Distance)",
                   "Y Component:=", ["B_radial"]],
                  [])

if not WIND:
  oModule.AddTraces("%s" % radial_flux_density_plot_name,
                    "Setup1 : Transient",
                    ["Context:=", "Br_r_ciy", "PointCount:=", 721, "Domain:=", "Sweep"],
                    ["Distance:=", ["All"],
                     "Time:=", ["0.00s"]],
                    ["X Component:=", "normalize(Distance)",
                     "Y Component:=", ["B_radial"]],
                    [])

  oModule.AddTraces("%s" % radial_flux_density_plot_name,
                    "Setup1 : Transient",
                    ["Context:=", "Br_r_coy", "PointCount:=", 721, "Domain:=", "Sweep"],
                    ["Distance:=", ["All"],
                     "Time:=", ["0.00s"]],
                    ["X Component:=", "normalize(Distance)",
                     "Y Component:=", ["B_radial"]],
                    [])

  oModule.AddTraces("%s" % radial_flux_density_plot_name,
                    "Setup1 : Transient",
                    ["Context:=", "Br_rnmh2", "PointCount:=", 721, "Domain:=", "Sweep"],
                    ["Distance:=", ["All"],
                     "Time:=", ["0.00s"]],
                    ["X Component:=", "normalize(Distance)",
                     "Y Component:=", ["B_radial"]],
                    [])

  oModule.AddTraces("%s" % radial_flux_density_plot_name,
                    "Setup1 : Transient",
                    ["Context:=", "Br_rnph2", "PointCount:=", 721, "Domain:=", "Sweep"],
                    ["Distance:=", ["All"],
                     "Time:=", ["0.00s"]],
                    ["X Component:=", "normalize(Distance)",
                     "Y Component:=", ["B_radial"]],
                    [])
                    
  oModule.AddTraces("%s" % radial_flux_density_plot_name,
                    "Setup1 : Transient",
                    ["Context:=", "Br_rnmh14", "PointCount:=", 721, "Domain:=", "Sweep"],
                    ["Distance:=", ["All"],
                     "Time:=", ["0.00s"]],
                    ["X Component:=", "normalize(Distance)",
                     "Y Component:=", ["B_radial"]],
                    [])
                    
  oModule.AddTraces("%s" % radial_flux_density_plot_name,
                    "Setup1 : Transient",
                    ["Context:=", "Br_rnph14", "PointCount:=", 721, "Domain:=", "Sweep"],
                    ["Distance:=", ["All"],
                     "Time:=", ["0.00s"]],
                    ["X Component:=", "normalize(Distance)",
                     "Y Component:=", ["B_radial"]],
                    [])

# ######################################## #
# Azimuthal Flux Density Rectangular  Plot #
# ######################################## #

azimuthal_flux_density_plot_name="Azimuthal Flux Density Rectangular Plot"

oModule.CreateReport("%s" % azimuthal_flux_density_plot_name,
                     "Fields",
                     "Rectangular Plot",
                     "Setup1 : Transient",
                     ["Context:=", "Br_rn", "PointCount:=", 721, "Domain:=", "Sweep"],
                     ["Distance:=", ["All"],
                      "Time:=", ["0.00s"]],
                     ["X Component:=", "normalize(Distance)",
                      "Y Component:=", ["B_azimuthal"]],
                     [])

oModule.AddTraces("%s" % azimuthal_flux_density_plot_name,
                  "Setup1 : Transient",
                  ["Context:=", "Br_cim", "PointCount:=", 721, "Domain:=", "Sweep"],
                  ["Distance:=", ["All"],
                   "Time:=", ["0.00s"]],
                  ["X Component:=", "normalize(Distance)",
                   "Y Component:=", ["B_azimuthal"]],
                  [])

oModule.AddTraces("%s" % azimuthal_flux_density_plot_name,
                  "Setup1 : Transient",
                  ["Context:=", "Br_com", "PointCount:=", 721, "Domain:=", "Sweep"],
                  ["Distance:=", ["All"],
                   "Time:=", ["0.00s"]],
                  ["X Component:=", "normalize(Distance)",
                   "Y Component:=", ["B_azimuthal"]],
                  [])

if not WIND:
  oModule.AddTraces("%s" % azimuthal_flux_density_plot_name,
                    "Setup1 : Transient",
                    ["Context:=", "Br_r_ciy", "PointCount:=", 721, "Domain:=", "Sweep"],
                    ["Distance:=", ["All"],
                     "Time:=", ["0.00s"]],
                    ["X Component:=", "normalize(Distance)",
                     "Y Component:=", ["B_azimuthal"]],
                    [])

  oModule.AddTraces("%s" % azimuthal_flux_density_plot_name,
                    "Setup1 : Transient",
                    ["Context:=", "Br_r_coy", "PointCount:=", 721, "Domain:=", "Sweep"],
                    ["Distance:=", ["All"],
                     "Time:=", ["0.00s"]],
                    ["X Component:=", "normalize(Distance)",
                     "Y Component:=", ["B_azimuthal"]],
                    [])


oProject.SaveAs("%s%s.mxwl" % (pathname,filename), True)

# ####################### #
# Quick Report CSV Export #
# ####################### #

pathname+=filename
pathname+=".mxwlresults/"


if WIND:
  oModule.ExportToFile("%s" % winding_data_plot_name,
                       "%s%s-lambda.txt" % (pathname,filename))
                       
if LMC:                       
  oModule.ExportToFile("%s" % inductance_matrix_data_plot_name,
                       "%s%s-LM.txt" % (pathname,filename))

if AR and PM:
  oModule.ExportToFile("%s" % torque_plot_name,
                       "%s%s-Tm.txt" % (pathname,filename))

oModule.ExportToFile("%s" % radial_flux_density_plot_name,
                     "%s%s-Br.txt" % (pathname,filename))

oModule.ExportToFile("%s" % azimuthal_flux_density_plot_name,
                     "%s%s-Bt.txt" % (pathname,filename))


##Post Processing:
if LMC:
  laa,lab,lac,lba,lbb,lbc,lca,lcb,lcc=np.loadtxt('%s%s-LM.txt' % (pathname,filename) ,skiprows=7,usecols=(1,2,3,4,5,6,7,8,9),unpack=True)
#  laa,lab=loadtxt('%s%s-LM.txt' % (pathname,filename) ,skiprows=7,usecols=(1,2),unpack=True)
  Ls = np.average(laa) - np.average(lab)
  Ls_extra_b = np.average(lbb) - np.average(lbc)
  Ls_extra_c = np.average(lcc) - np.average(lcb)
  print("Ls = %g [uH]"%(Ls))#text file reports values already in [uH]
  print("Ls_extra_b = %g [uH]"%(Ls_extra_b))
  print("Ls_extra_c = %g [uH]"%(Ls_extra_c))
  fla,flb,flc=np.loadtxt('%s%s-lambda.txt' % (pathname,filename) ,skiprows=7,usecols=(1,2,3),unpack=True) 
  
  #this method is valid if only Iq currents are injected
  w_e = 2*pi*f
  vd_calculated = np.zeros(sim_steps)
  vq_calculated = np.zeros(sim_steps)
  fld = np.zeros(sim_steps)
  flq = np.zeros(sim_steps)
  fl0 = np.zeros(sim_steps)    
  for i in range(0,sim_steps):
    fld[i],flq[i],fl0[i] = dq_to_abc(fla[i],flb[i],flc[i],0)
    vd_calculated[i] = -w_e*flq[i]#/turnsPerCoil #minus is present with vd, see Umans p578
    vq_calculated[i] = w_e*fld[i]#/turnsPerCoil 
  
  va_calculated = np.sqrt(vd_calculated[0]**2 + vq_calculated[0]**2)  
  print("va_calculated = %g [V]"%(va_calculated))
  Ls_dq_method = abs(max(vd_calculated)/(w_e*I_p))
  print("Ls_dq_method = %g [uH]"%(Ls_dq_method*1e6))
        
  #this method is valid only if AR alone is active
  if AR and (not PM):
    Ls_AR = max(fla)/(I_p)    
    print("Ls_AR = %g [uH]"%(Ls_AR*1e6))
    
        
# Post Processing: Draw plots
#os.chdir(pathname)
from pylab import *

rcParams['font.family'] = 'serif'

rcParams['text.usetex'] = 'True'
rcParams['text.latex.preamble']='\usepackage{times}'

figure(1)
dist,Br=np.loadtxt('%s%s-Br.txt' % (pathname,filename) ,skiprows=7,usecols=(0,1),unpack=True)

figure(1)
plot(dist*720,Br,label="$B_r$")
xlabel('Electrical Degrees [$^\circ$]')
ylabel('Radial Flux Density [T]')
#legend()
axis(xmax=720)
grid(True)

savefig('%srn%.2fhc%.2fRadial_flux_density_distribution.pdf' % (pathname,r_n,h))

N=size(Br[:-1])      # Only want 720 samples, not 721
Brh=2/N*abs(fft(Br)) # The FFT function does not include the divide by N/2
h=arange(0,40,2)     # Because our waveform is over two periods, the harmonics is every second value

print("Br|max=%g T" % max(Br))
print("Br1   =%g T" % Brh[2]) # The third value is the first harmonic, 0=DC, 2=1st, 4=2nd, etc.

figure(2)
bar(h/2,Brh[h],width=0.5)
xlabel('Radial Density Harmonic Number')
ylabel('Flux Density [T]')


annotate("%.3f" % Brh[2],
         size='x-large',
         xy=(0,Brh[2]),
         xytext=(3,Brh[2]*0.95),
         arrowprops='',
         color='black',
         horizontalalignment='center')

savefig('%sHarmonic_spectrum.pdf' % (pathname))

#x,Lambda=loadtxt('%s%s-lambda.txt' % (pathname,filename) ,skiprows=7,usecols=(0,1),unpack=True)
#x=x[0:100]
#dx=1.0/3.0
#V=diff(Lambda,1)/dx
#figure(3)
#plot(x,V)
#xlabel('Time')
#ylabel('Voltage[V]')

#oProject.Close()

#oDesktop.CloseProject(my_project_name)
#oDesktop.QuitApplication()
