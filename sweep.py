# -*- coding: utf-8 -*-
"""
Created on Tue Nov 01 18:54:54 2016

@author: Andreas
"""

from __future__ import division				#ensures division always returns the correct answer (eg. 1/2 = 0.5 instead of 1/2 = 0)
import numpy as np
from prettytable import PrettyTable
import sqlite3
#import argparse
import sys, os
import time as time_mod

begin2 = time_mod.time()

initial_electrical_angle = 0
end_electrical_angle_position = 360
electrical_angle_increment = 5

primary_key = 17
db_name = 'SORSPM.db'
pathname="D:/Documents/Meesters/Ansys_Maxwell_Simulations/2d/results/"
filename="SORSPM"
#sys.argv = ['SORSPM_maxwell_2d.py','-pk %d'%(primary_key)]


pretty_terminal_table = PrettyTable(['Variable','Value','Unit'])
pretty_terminal_table.align = 'l'
pretty_terminal_table.border= True  

#flags
result_already_exists = 1
already_commited_something_now = 0
deleted_row_entry = 0  
database_verbosity = 0

conn_sweep = sqlite3.connect(db_name)
c_sweep = conn_sweep.cursor()

def db_input_select(table,column,primary_key):
  string = "SELECT " +column+ " FROM " +table+ " WHERE primary_key=%d"%(primary_key)
  c_sweep.execute(string)
  return c_sweep.fetchone()[0]

def db_output_insert(table,column,primary_key,value):
#  global already_commited_something_now
#  if already_commited_something_now==0:
#    try:
#      c.execute("INSERT INTO "+table+"(primary_key,"+column+") "+ "VALUES (?,?)",(primary_key,value))
#      already_commited_something_now = 1
#      if database_verbosity == 1: print "created new row and added some value to a column"
#    except:
#      pass
#  else:
    try:
      c_sweep.execute("UPDATE "+table+" SET "+column+" = ? WHERE primary_key=?",(value,primary_key))
      if database_verbosity == 1: print "added new value (%.2f) to column (%s) of existing primary_key (%d)"%(value,column,primary_key)
    except:
      print "Failed to UPDATE value in existing column and row"
      pass

#initial_electrical_angle = db_input_select("machine_input_parameters","initial_position_defined_by_electrical_angle_deg",primary_key)  
#print "initial_electrical_angle"
#print initial_electrical_angle  

db_output_insert("machine_input_parameters","initial_position_defined_by_electrical_angle_deg",primary_key,initial_electrical_angle)
conn_sweep.commit()

l = db_input_select("machine_input_parameters","stacklength_in_mm",primary_key)
kmr = db_input_select("machine_input_parameters","radially_magnetized_PM_width_ratio",primary_key)
poles = db_input_select("machine_input_parameters","poles",primary_key)
hmr = db_input_select("machine_input_parameters","radially_magnetized_PM_height_in_mm",primary_key)
hc  = db_input_select("machine_input_parameters","coil_height_in_mm",primary_key)
J = db_input_select("machine_input_parameters","current_density_rms",primary_key)

#conn_sweep.close()
#sys.exit()

filename+="_l=%.2f" % l
filename+="_Jrms=%g" % J
filename+="_kmr=%.2f" % kmr
filename+="_hmr=%.1f" % hmr
filename+="_p=%g" % poles
filename+="_hc=%.1f" % hc

pathname+=filename
pathname+=".mxwlresults/"

steps = int(end_electrical_angle_position/electrical_angle_increment)-1
torque_np_array = np.zeros(steps+1)
electrical_angle_np_array = np.zeros(steps+1)

first_time = 1
i=0
while initial_electrical_angle < end_electrical_angle_position: 
  conn_sweep.close()
    
#  execfile('SORSPM_maxwell_2d.py') #does not accept any arguments and causes database commits to fail in this external sweep script.
  os.system("SORSPM_maxwell_2d.py -pk %d"%(primary_key))
  
  conn_sweep = sqlite3.connect(db_name)
  c_sweep = conn_sweep.cursor()     
  
  time,Tm=np.loadtxt('%s%s-Tm.txt' % (pathname,filename) ,skiprows=7,usecols=(0,1),unpack=True)

#  Tm = 5

  average_torque = np.average(Tm)
  torque_np_array[i] = average_torque
  electrical_angle_np_array[i] = initial_electrical_angle
  max_torque = np.max(Tm)
  min_torque = np.min(Tm)
  torque_ripple = ((max_torque-min_torque)/min_torque)*100  

  pretty_terminal_table.add_row(["initial_electrical_angle", initial_electrical_angle, "[deg]"])
  pretty_terminal_table.add_row(["average_torque", average_torque, "[Nm]"])
  pretty_terminal_table.add_row(["torque_ripple", torque_ripple, "[%]"])
  pretty_terminal_table.add_row(["max_torque", max_torque, "[Nm]"])
  pretty_terminal_table.add_row(["min_torque", min_torque, "[Nm]"])
  
  print pretty_terminal_table  
  pretty_terminal_table.clear_rows()
  
  initial_electrical_angle = initial_electrical_angle+electrical_angle_increment
#  print initial_electrical_angle
  
  i=i+1

  db_output_insert("machine_input_parameters","initial_position_defined_by_electrical_angle_deg",primary_key,initial_electrical_angle)
  conn_sweep.commit()  
    
conn_sweep.close()  
np.savetxt('maxwell_script_output.csv', np.array([electrical_angle_np_array,torque_np_array]).T,delimiter = ',',newline='\n',header='equivelent_electrical_angle_position,maxwell_torque',fmt='%f')

print "End of sweep reached."
print "Time elapsed during total sweep %.2f [s]"%(time_mod.time()-begin2)