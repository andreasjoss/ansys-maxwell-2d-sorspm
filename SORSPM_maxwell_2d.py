# -*- coding: utf-8 -*-
"""
Created on Tue Oct 18 16:23:42 2016

@author: Andreas
"""

from __future__ import division				#ensures division always returns the correct answer (eg. 1/2 = 0.5 instead of 1/2 = 0)
import numpy as np
import pylab as pl
import farey as farey
from win32com.client import Dispatch
from prettytable import PrettyTable
from fractions import gcd
import pyMaxwell2D
import poly
import sqlite3
import argparse
import sys
import time as time_mod

begin = time_mod.time()

pi = np.pi
intersect_spacing = 0.01

use_machine_periodicity = 1
run_band_solver = 1
database_verbosity = 0
store_results_in_database = 1
overwrite_previous_result = 1
enable_terminal_printout = 1
plot_all = 0
debug_offset = 0
mmf_wave_clockwise = 0
close_project = 0

#analyse_specific_step = [4,8,12,16,20,24]
analyse_specific_step = []

# ############### #
# Simulation Data #
# ############### #
#PM      = True        # permanent magnets switche "ON"
#AR      = True          # windings switched "ON"
WIND    = True          # draw windings
LMC     = True          # inductance matrix calculations
CLIP_COILS = False      # puts a small airgap between opposing coil sides
EDDY_LOSSES = True
CORE_LOSSES = True
AIR_YOKES = False

dont_draw_stator_teeth = 0
dont_draw_stator_teeth_air = 0
#draw_single_coil_only = 1




#SCRIPT CONSTANTS -- DO NOT CHANGE THESE
RADIALLY_STRAIGHT = 0
SQUARE_SLOTS = 1

if (not RADIALLY_STRAIGHT==1) and (not SQUARE_SLOTS==1):
  raise ValueError, "Either 'RADIALLY_STRAIGHT' must be 'True' or 'SQUARE_SLOTS' must be 'True' or both..."
#if AR and (not WIND):
#  WIND = True

VERBOSITY = 0
LINEWIDTH=1
FONTSIZE = 14

#flags
result_already_exists = 1
already_commited_something_now = 0
deleted_row_entry = 0  

#if (not AR):
#    J = 0
#    I_p = 0
#if (not PM):
#    Brem = 0
    
parser = argparse.ArgumentParser(description="SORSPM semfem simulation script",formatter_class=argparse.RawTextHelpFormatter)
parser.add_argument('-pk','--primary_key', dest='primary_key', type=int, default=17, help='Specify the primary_key of the input row of the database that should be used')
parser.add_argument('-db','--database', dest='database', default='SORSPM.db', help='Specify the database name which should be used')

args = parser.parse_args()

#control variables
primary_key = args.primary_key
db_name = args.database
#print "Notice that pk %d is being used."%primary_key

conn = sqlite3.connect(db_name)
c = conn.cursor()

def db_input_select(table,column,primary_key):
  string = "SELECT " +column+ " FROM " +table+ " WHERE primary_key=%d"%(primary_key)
  c.execute(string)
  return c.fetchone()[0]

def db_output_insert(table,column,primary_key,value):
  global already_commited_something_now
  if already_commited_something_now==0:
    try:
      c.execute("INSERT INTO "+table+"(primary_key,"+column+") "+ "VALUES (?,?)",(primary_key,value))
      already_commited_something_now = 1
      if database_verbosity == 1: print "created new row and added some value to a column"
    except:
      pass
  else:
    try:
      c.execute("UPDATE "+table+" SET "+column+" = ? WHERE primary_key=?",(value,primary_key))
      if database_verbosity == 1: print "added new value to column of existing row"
    except:
      pass

#check if a result is already stored for this primary_key
if store_results_in_database == 1:
  #try:
  if database_verbosity == 1: print "trying to select"
  c.execute("SELECT magnet_mass FROM machine_semfem_output WHERE primary_key = ?", (primary_key,))
  data=c.fetchone()
  
  if database_verbosity == 1:
    if data is None:
	print('There is no primary_key named %s'%primary_key)
    else:
	print('primary_key %s found with magnet_mass %s'%(primary_key,data[0]))    
  
  if data is None:
    if database_verbosity == 1: print "No entry found"
    result_already_exists = 0
  else:
    if database_verbosity == 1: print "Entry found"
    result_already_exists = 1
  #except:
    ##executes if this entry could not be found
    #print "except clause"
    #result_already_exists = 0

#delete entry if it exists and if we are to overwrite any previous results
if overwrite_previous_result == 1 and result_already_exists == 1:
  #conn.commit()
  c.execute("DELETE FROM machine_semfem_output WHERE primary_key=?",(primary_key,))
  #conn.commit()
  deleted_row_entry = 1
  if database_verbosity == 1: print "deleted previous entry"  

if enable_terminal_printout == 1:
  pretty_terminal_table = PrettyTable(['Variable','Value','Unit'])
  pretty_terminal_table.align = 'l'
  pretty_terminal_table.border= True  


poles = db_input_select("machine_input_parameters","poles",primary_key)
slots = db_input_select("machine_input_parameters","slots",primary_key)

if slots%3 != 0:
  print "number of slots (%d) is invalid, it should be multiple of 3"%(slots)
  sys.exit()  
  
if poles%2 != 0:  
  print "number of poles (%d) is invalid, it should be multiple of 2"%(poles)
  sys.exit()  
  
if slots == poles:
  print "number of slots (%d) cannot be equal to number of poles (%d), it produces significant cogging torque (Skaar2006) and (Martinez2012 p.41)"%(slots,poles)
  sys.exit()

  
J = db_input_select("machine_input_parameters","current_density_rms",primary_key)
system_voltage = db_input_select("machine_input_parameters","system_voltage",primary_key)
#r -- radius, h -- height, m -- magnet, o -- outer, i -- inner, k -- ratio, r -- radially, t -- tangentially
ryo = db_input_select("machine_input_parameters","rotor_yoke_outer_radius_in_mm",primary_key)
hyo = db_input_select("machine_input_parameters","rotor_yoke_height_in_mm",primary_key)
hyi = db_input_select("machine_input_parameters","stator_yoke_height_in_mm",primary_key)
hc  = db_input_select("machine_input_parameters","coil_height_in_mm",primary_key)
hs_ratio= db_input_select("machine_input_parameters","stator_teeth_to_coil_height_ratio",primary_key)
hs_square_slots_ratio=db_input_select("machine_input_parameters","stator_teeth_height_ratio_square_shape",primary_key)
hmr = db_input_select("machine_input_parameters","radially_magnetized_PM_height_in_mm",primary_key)
kmr = db_input_select("machine_input_parameters","radially_magnetized_PM_width_ratio",primary_key)
kmt = (1-kmr)*db_input_select("machine_input_parameters","tangentially_magnetized_PM_width_ratio",primary_key)
km_gap = 1 - kmr - kmt
kc  = db_input_select("machine_input_parameters","coil_width_ratio",primary_key)
coil_shape = db_input_select("machine_input_parameters","coil_shape",primary_key)
coil_side_center_placement = db_input_select("machine_input_parameters","coil_side_center_placement",primary_key)
coil_yoke_spacing = db_input_select("machine_input_parameters","coil_yoke_spacing_in_mm",primary_key)
coil_teeth_spacing = db_input_select("machine_input_parameters","coil_teeth_spacing_in_mm",primary_key)
coil_opposing_spacing = db_input_select("machine_input_parameters","coil_opposing_spacing_in_mm",primary_key)
#kc == ratio of stator coil side width out of maximum size 1
#ks == ratio of coil core, i.e. stator teeth width 
#kq  = db_input_select("machine_input_parameters","coils_per_phase_vs_rotor_pole_pair_ratio",primary_key)
l = db_input_select("machine_input_parameters","stacklength_in_mm",primary_key)
g = db_input_select("machine_input_parameters","airgap_in_mm",primary_key)
muR_yo = db_input_select("machine_input_parameters","rotor_yoke_relative_permeability",primary_key)
muR_yi = db_input_select("machine_input_parameters","stator_yoke_relative_permeability",primary_key)
fill_factor = db_input_select("machine_input_parameters","fill_factor",primary_key)
n_rpm = db_input_select("machine_input_parameters","n_rpm",primary_key)
Brem= db_input_select("machine_input_parameters","B_rem",primary_key)						#remnant flux density of NdBFe N48 PMs        [T]
ryoke_minimum_ratio= db_input_select("machine_input_parameters","rotor_yoke_height_minimum_ratio",primary_key)
minimum_rotor_yoke_thickness = ryoke_minimum_ratio*hyo
km_gap_teeth= 1-db_input_select("machine_input_parameters","rotor_gap_teeth_height_ratio",primary_key)
rotor_yoke_iron_N_al= db_input_select("machine_input_parameters","rotor_yoke_iron_N_al",primary_key)
stator_yoke_iron_N_al= db_input_select("machine_input_parameters","stator_yoke_iron_N_al",primary_key)
N = db_input_select("machine_input_parameters","force_this_number_of_turns",primary_key)
coil_temperature = db_input_select("machine_input_parameters","coil_temperature",primary_key)
hmt_slot_depth_ratio = 1-db_input_select("machine_input_parameters","rotor_slot_depth_ratio",primary_key)
hmt_magnet_height_ratio_of_slot = db_input_select("machine_input_parameters","tangentially_magnetised_PM_height_ratio_of_slot",primary_key)
initial_electrical_angle = db_input_select("machine_input_parameters","initial_position_defined_by_electrical_angle_deg",primary_key)
shift_current_electrical_angle = db_input_select("machine_input_parameters","shift_current_by_electrical_angle_deg",primary_key)
square_magnets = db_input_select("machine_input_parameters","square_magnets",primary_key)
magnetisation_radial0_normal1 = db_input_select("machine_input_parameters","magnetisation_radial0_normal1",primary_key)
inverter_squarewave0_sinusoidalPWM1 = db_input_select("machine_input_parameters","inverter_squarewave0_sinusoidalPWM1",primary_key)

print "SORSPM_maxwell_2d.py: initial_electrical_angle = %.2f"%(initial_electrical_angle)

AR_0_PM_1_ALL_2 = db_input_select("machine_input_parameters","AR_0_PM_1_ALL_2",primary_key)
if AR_0_PM_1_ALL_2 == 0: #AR on, PM off
    PM = False
    AR = True
#  Brem = 0
##  replace_magnets_with_iron = 1
#  replace_magnets_with_iron = 1
#  hmt_slot_depth_ratio = 0
#  km_gap_teeth = 0
elif AR_0_PM_1_ALL_2 == 1: #AR off, PM on
    PM = True
    AR = False
#  id = 0
#  iq = 0
#  J = 0
#  replace_magnets_with_iron = 0
elif AR_0_PM_1_ALL_2 == 2: #all on
    PM = True
    AR = True
#  replace_magnets_with_iron = 0

if (not PM) and (not AR):
  raise ValueError, "Either 'PM' must be 'True' or 'AR' must be 'True' or both..."
if AR and (not WIND):
#  WIND = True
   WIND = False
   AR = False
  
if PM and not AR:
  I_p=0                       # if I_p=0 - the windings are effectively switched "OFF"
  
steps = db_input_select("machine_input_parameters","steps",primary_key)
#steps=15

if steps == 1:
    analyse_specific_step = []

q = int(slots/3)#number of coils per phase

f_e = (n_rpm*(poles))/120      #since comparison with Ansys Maxwell flux linkage and induced voltages for single turn series branches, I suspect that this frequency is incorrect with factor two ( 2016/10/14).
					  #the electrical frequency f_e (time dependant frequency) is established by the number of poles in the rotor.
					  #this f_e will produce also a f_e flux linkage in the stator coils, and thus we need to inject f_e current into the stator to produce torque
					  #do not confuse f_e with the space harmonic distributions, that is something entirely different

if VERBOSITY > 0:
    print("f_e_second_harmonic (induced voltage frequency, input current frequency) = %.3f [Hz]"%f_e)

Q = slots
coil_pitch = (2*pi)/Q

if np.isclose(kc, 1, rtol=1e-02, atol=1e-02, equal_nan=False): kc = 1
if np.isclose(kc, 0, rtol=1e-02, atol=1e-02, equal_nan=False): kc = 1e-02 #there needs to be at least some coil copper area

ks = 1-kc

#kc == ratio of stator coil side width out of maximum size 1
#ks == ratio of coil core, i.e. stator teeth width 

ks_pitch = ks*coil_pitch
kc_pitch = kc*coil_pitch  

if coil_shape ==  RADIALLY_STRAIGHT:
  if np.isclose(hs_ratio, 1, rtol=1e-02, atol=1e-02, equal_nan=False):
      hs_ratio = 1 #only iron exists in teeth areas
      dont_draw_stator_teeth_air = 1
  if np.isclose(hs_ratio, 0, rtol=1e-02, atol=1e-02, equal_nan=False):
      hs_ratio = 0 #only air exists in teeth areas
      dont_draw_stator_teeth = 1
elif coil_shape == SQUARE_SLOTS:
  if np.isclose(hs_square_slots_ratio, 1, rtol=1e-02, atol=1e-02, equal_nan=False):  
      hs_square_slots_ratio = 1 #only iron exists in teeth areas
      dont_draw_stator_teeth_air = 1
  if np.isclose(hs_square_slots_ratio, 0, rtol=1e-02, atol=1e-02, equal_nan=False):
      hs_square_slots_ratio = 0 #only air exists in teeth areas
      dont_draw_stator_teeth = 1    
    
#periodicity_in_one_full_rotation = gcd(slots,int(poles/2)) #Martinez2012 p.29
periodicity_in_one_full_rotation = gcd(int(slots/3),int(poles/2))
#print(periodicity_in_one_full_rotation)
cogging_torque_harmonic = (slots/gcd(slots,poles))*(poles/gcd(slots,poles)) #works well so far for integral-slot machines
if int(periodicity_in_one_full_rotation) == 2:
  use_machine_periodicity = 0
  
#direction=1
pole_pairs_per_slot_per_phase = int(poles/2)/int(slots/3)
slots_per_phase_per_pole = slots/(poles*3)
#get clockwise harmonics (and maybe their kw polaraties too?)
clockwise = []
#get anti-clockwise harmonics
anti_clockwise = []

#these harmonic directions of the mmf waves are obtained from dr. Randewijk's thesis
#the directions are also confirmed from dr. Rix's theses, p.15 for tau=1

#equations used by dr. Rix chapter 2
M_s = gcd(int(poles/2), slots)
W_s = gcd(poles,slots)
tau = int(W_s/M_s) #answer for this machine is 1, thus positive periodicity, and the following assumptions are valid

#print "M_s = %f"%(M_s)
#print "W_s = %f"%(W_s)
#print "tau = %f"%(tau)

#tau = 1 -- positive periodicity
#tau = 2 -- negative periodicity

stator_mmf_harmonic_direction = []

#if tau == 1:
#  print "positive periodicity"
#elif tau == 2:
#  print "negative periodicity -- even stator MMF harmonics (2,4,6...) will not exist."

for i in range (-5,5):
  stator_mmf_harmonic_direction.append(int(3*tau*i-1))

  
sorted_stator_mmf_harmonic_direction = sorted(stator_mmf_harmonic_direction,key=abs)    
#print(sorted_stator_mmf_harmonic_direction)
#
#print("pole_pairs_per_slot_per_phase = %.3f"%(pole_pairs_per_slot_per_phase))
#print("slots per pole per phase = %.3f"%(slots_per_phase_per_pole))

working_harmonic = 0
working_fractional = 0
#print(pole_pairs_per_slot_per_phase%1)
if pole_pairs_per_slot_per_phase%1 != 0:
  working_fractional = int(pole_pairs_per_slot_per_phase) + 1/(pole_pairs_per_slot_per_phase%1)
  working_fractional = int(round(working_fractional,0))
  print "working harmonic for this fractional slot machine should be number = %f"%working_fractional

for i in range(0,len(sorted_stator_mmf_harmonic_direction)):
  #print "working_fractional"
  #print int(working_fractional)
  #print int(round(working_fractional,0))
  #print "sorted_stator_mmf_harmonic_direction[i]"
  #print abs(sorted_stator_mmf_harmonic_direction[i])
  
  if working_fractional == abs(sorted_stator_mmf_harmonic_direction[i]):
    working_harmonic = abs(sorted_stator_mmf_harmonic_direction[i])
    break
    
  elif pole_pairs_per_slot_per_phase == abs(sorted_stator_mmf_harmonic_direction[i]):
    working_harmonic = abs(sorted_stator_mmf_harmonic_direction[i])
    break
  
if sorted_stator_mmf_harmonic_direction[i] > 0:
  direction = 1
elif sorted_stator_mmf_harmonic_direction[i] < 0:
  direction = -1
if VERBOSITY == 1 and working_harmonic != 0:
  print "direction is (%d) and match is found in sorted_stator_mmf_harmonic_direction[%d] with working harmonic %d"%(direction,i,working_harmonic)
  
if working_harmonic == 0:
  print "there is no common harmonic between poles and stator slots to form a working harmonic"
  sys.exit()
 
#if minimum_working_harmonic<working_harmonic:
#  print "machine might be very ineffective since a high working harmonic is being used"
#  #sys.exit()

if pole_pairs_per_slot_per_phase%1 != 0:
  print "pole_pairs_per_slot_per_phase (%.3f) is not a whole number, meaning fractional-slot pitching should be done"%(pole_pairs_per_slot_per_phase)
  #sys.exit()

if slots_per_phase_per_pole<0.25 or slots_per_phase_per_pole>0.5:
  print "slots_per_phase_per_pole (%.3f) is not within range of 0.25 and 0.50"%(slots_per_phase_per_pole)
  sys.exit()

if np.isclose(periodicity_in_one_full_rotation, 1, rtol=1e-04, atol=1e-04, equal_nan=False):
  use_machine_periodicity = 0

peak_voltage_allowed = 0
if inverter_squarewave0_sinusoidalPWM1 == 0:
  peak_voltage_allowed = (2*np.sqrt(3)/np.pi)*system_voltage
elif inverter_squarewave0_sinusoidalPWM1 == 1:
  peak_voltage_allowed = (np.sqrt(3)/2)*system_voltage

peak_voltage_allowed = 0.95*peak_voltage_allowed  
  
#peak_voltage_allowed = (system_voltage*0.95*1.15)/np.sqrt(3)#The input file refers to a peak (not RMS) Line-to-Line (LL) voltage, 0.95 takes into account voltage source drop, 1.15 takes into account THD of the sinusoidal voltage source

N_L = 2							#Number of layer stack windings (if N_L == 1, then an error is given)
#N_L=2 means there are only 2 coil sides within one "slot". In this prototype, the teeth are centered between the outgoing and ingoing current of a phase.
#and the sides of different phases are right next to each other (in the same "slot")
N_S = 1							#Overlap number. This is a non-overlapping coil structure, thus N_S = 1
phase = [1,-1,3,-3,2,-2]
number_of_parallel_circuits = 1				#all phase coils will be connected in series

muR= 1.05 						#recoil permeability of the PMs
cu_resistivity_25 = 1.678e-8				#[Ohm.m] at STD 25 Degrees Celcius
copper_resistivity = cu_resistivity_25*(1+0.0039*(coil_temperature-20)) #copper resistivity is given in [Ohm.m]

#density values obtained from SEMFEM (all stated as kg/m3)
steel_density = 7600
copper_density = 8954
magnet_density = 7500

#assume only single turn, in order to satisfy voltage limit
turnsPerCoil = N #is the same as N
 #is the same as turnsPerCoil
a=1

RED    = (255,0,0)
YELLOW = (255,255,0)
BLUE   = (0,0,255)
GRAY   = (192,192,192)
BROWN  = (139,69,19)

Type='II'

  
sim_slice = 2/poles if Type=='O' else 4/poles   # (minimum) simulation slice

sim_time  = (1/f_e)               # simulation end time
sim_steps = steps            # number of simulation time steps between 0 and sim_time
out_steps = steps             # number of field and output variable steps

#time_at_specific_step = []
#
#for i in range(0,len(analyse_specific_step)):
#    time_at_specific_step.append(((analyse_specific_step[i]-1)/steps)*(1/f_e))
#    print "time_at_specific_step[%d] = %.9f"%(i,time_at_specific_step[i])
# ################################# #
# Calculation of Machine Parameters #
# ################################# #

#imported from semfem script
magnet_ns_total = poles #assuming Q-H topology
magnet_radially_pitch = (2*pi)/magnet_ns_total

magnet_radially_outer_radius = ryo-hyo
magnet_radially_inner_radius = ryo-hyo-hmr
magnet_tangentially_slot_radius = ryo-hyo-hmt_slot_depth_ratio*hmr
gap_teeth_radius = magnet_radially_inner_radius + km_gap_teeth*(magnet_radially_outer_radius-magnet_radially_inner_radius)
magnet_tangentially_slot_height = magnet_tangentially_slot_radius-magnet_radially_inner_radius
magnet_tangentially_PM_height = hmt_magnet_height_ratio_of_slot*(magnet_tangentially_slot_height)
magnet_tangentially_inner_radius = magnet_tangentially_slot_radius-hmt_magnet_height_ratio_of_slot*(magnet_tangentially_slot_height)
magnet_tangentially_outer_radius = magnet_tangentially_inner_radius+magnet_tangentially_PM_height

stator_yoke_inner_radius = magnet_radially_inner_radius - g - hyi - hc
stator_yoke_outer_radius = magnet_radially_inner_radius - g - hc
stator_coil_radius = magnet_radially_inner_radius - g

if coil_shape == RADIALLY_STRAIGHT:
  coil_side_area_in_mm = (((stator_coil_radius)**2-(stator_yoke_outer_radius)**2)*np.pi)*(0.5*kc_pitch/(2*np.pi))  #this is the coil_side_area in [m^2] for ONE side (not a pair of sides)
  coil_side_area = coil_side_area_in_mm/(1000**2)
  current_through_slot = J*(1000**2)*coil_side_area*fill_factor

  stator_yoke_area = (stator_yoke_outer_radius**2-stator_yoke_inner_radius**2)*np.pi + (stator_coil_radius**2-stator_yoke_outer_radius**2)*np.pi*ks*hs_ratio
  rotor_yoke_area = (ryo**2-magnet_radially_outer_radius**2)*np.pi
  magnet_area = (magnet_radially_outer_radius**2-magnet_radially_inner_radius**2)*np.pi*kmr
  copper_area = coil_side_area_in_mm*2*Q*fill_factor

  stator_yoke_mass = ((stator_yoke_area*l)/(1000**3))*steel_density
  rotor_yoke_mass = ((rotor_yoke_area*l)/(1000**3))*steel_density
  magnet_mass = ((magnet_area*l)/(1000**3))*magnet_density
  copper_mass = ((copper_area*l)/(1000**3))*copper_density
  
  total_mass = stator_yoke_mass + rotor_yoke_mass + magnet_mass + copper_mass
  
  I_p = (current_through_slot*np.sqrt(2))/N

elif coil_shape == SQUARE_SLOTS:
  #COPIED PRECISELY as is from semfem script
  r = stator_yoke_outer_radius
  coil_side_width = 0.5*kc_pitch*r
  coil_side_height = stator_coil_radius-stator_yoke_outer_radius
  teeth_width = ks_pitch*r
  half_teeth_width = 0.5*teeth_width*coil_side_center_placement
  x = stator_yoke_outer_radius + coil_side_height
  y = half_teeth_width+coil_side_width-coil_opposing_spacing

  R = np.sqrt(y**2 + x**2)
  x_old = x
  if R > stator_coil_radius:
    x = np.sqrt(stator_coil_radius**2 - y**2)      

  #the following three lines are variables only being used for purposes of terminal output print
  diff = x_old-x
  coil_side_width = coil_side_width - coil_opposing_spacing - coil_teeth_spacing
  coil_side_height = coil_side_height - coil_yoke_spacing - diff
    
  center_of_side_x = 0.5*(stator_yoke_outer_radius + x)
  center_of_side_y = 0.5*(half_teeth_width+y)
    
  center_of_side_r = np.sqrt(center_of_side_x**2 + center_of_side_y**2)
  center_of_side_t = np.arcsin(center_of_side_y/center_of_side_r)
  temp_angle0 = np.arcsin(half_teeth_width/stator_coil_radius)
  temp_angle1 = np.arctan(y/x)
    
  temp_x = stator_yoke_outer_radius + hs_ratio*(x-stator_yoke_outer_radius)
  temp_y = y
  ###END OF COPY

#    cplx1 = complex(x,half_teeth_width)
#    cplx3 = complex(stator_yoke_outer_radius,half_teeth_width)
#    cplx4 = complex(stator_yoke_outer_radius,y)
#    cplx5 = complex(x,y)
#    cplx6 = complex(x,half_teeth_width)  
  
#    cplx1 = complex(x-coil_yoke_spacing,half_teeth_width+coil_teeth_spacing)
#    cplx3 = complex(stator_yoke_outer_radius+coil_yoke_spacing,half_teeth_width+coil_teeth_spacing)
#    cplx4 = complex(stator_yoke_outer_radius+coil_yoke_spacing,y-coil_teeth_spacing-coil_opposing_spacing)
#    cplx5 = complex(x-coil_yoke_spacing,y-coil_teeth_spacing-coil_opposing_spacing)
#    cplx6 = complex(x-coil_yoke_spacing,half_teeth_width+coil_teeth_spacing)

  #calculate areas, masses and current to be injected
  coil_side_area_in_mm = coil_side_width*coil_side_height
#  coil_side_area_in_mm = ((x-coil_yoke_spacing)-stator_yoke_outer_radius)*(coil_side_width-coil_teeth_spacing-coil_opposing_spacing)
  coil_side_area = coil_side_area_in_mm/(1000**2)
  current_through_slot = J*(1000**2)*coil_side_area*fill_factor

  stator_teeth_area = teeth_width*coil_side_height*hs_square_slots_ratio*Q #hopefully a good enough approximation
  #(stator_coil_radius**2-stator_yoke_outer_radius**2)*np.pi*ks
  stator_yoke_area = (stator_yoke_outer_radius**2-stator_yoke_inner_radius**2)*np.pi+stator_teeth_area
  rotor_yoke_area = (ryo**2-magnet_radially_outer_radius**2)*np.pi
  magnet_area = (magnet_radially_outer_radius**2-magnet_radially_inner_radius**2)*np.pi*kmr
  copper_area = coil_side_area_in_mm*2*Q*fill_factor

  stator_yoke_mass = ((stator_yoke_area*l)/(1000**3))*steel_density
  rotor_yoke_mass = ((rotor_yoke_area*l)/(1000**3))*steel_density
  magnet_mass = ((magnet_area*l)/(1000**3))*magnet_density
  copper_mass = ((copper_area*l)/(1000**3))*copper_density
  
  total_mass = stator_yoke_mass + rotor_yoke_mass + magnet_mass + copper_mass  
  
  I_p = (current_through_slot*np.sqrt(2))/N  
  
  
#mechRotation = (4*pi)/poles
mechRotation = (2*pi)/periodicity_in_one_full_rotation
step_angle = mechRotation/steps
#initial_angle = 0.5*magnet_radially_pitch*(180/(np.pi))

initial_angle = initial_electrical_angle/periodicity_in_one_full_rotation #initial mechanical angle in degrees
#initial_angle = 0

#time_shift = (initial_electrical_angle/720)*(1/f_e)
time_shift = (initial_electrical_angle/360)*(1/f_e)
#time_shift = 0

min_time_step = 0.0001
max_time_step = 0.0003
if steps == 1:
    stop_time = sim_time
#    stop_time = time_shift+sim_time
#    stop_time = sim_time+min_time_step
    step_size = sim_time
#    time_step_err_tolerance = 0
    time_step_err_tolerance = 0.00001
#    start_time_value = time_shift
    start_time_value = 0
    
#    print 'time_shift = %f [ms]'%(time_shift*1000)
#    print 'sim_time = %f'%sim_time
#    print 'stop_time = %f'%stop_time
#    print 'step_size = %f'%step_size
    
else:
    stop_time = sim_time
    time_step_err_tolerance = 0.00001
    step_size = stop_time/out_steps
    start_time_value = 0
    
temp_angle1 = 0.5*kc_pitch + 0.5*ks_pitch
temp_angle2 = coil_pitch-temp_angle1 

if coil_shape == RADIALLY_STRAIGHT: #radial slots
  coil_offset = 0
elif coil_shape == SQUARE_SLOTS: #square slots
  coil_offset = 0.5*(temp_angle1+temp_angle2)


#mesh = 1e-3
mesh_scale = db_input_select("machine_input_parameters","mesh_scale",primary_key)
#reference_surface_area = db_input_select("machine_input_parameters","reference_surface_area",primary_key)
#surface_area = pi*(ryo**2-stator_yoke_inner_radius**2)
#surface_area_factor = surface_area/reference_surface_area
#mesh = mesh*mesh_scale*surface_area_factor
#
#mesh_yoke_layer = mesh*mesh_yoke_ratio
#mesh_stator_layer = mesh*mesh_stator_ratio
#mesh_rotor_layer = mesh*mesh_rotor_ratio

# ############################################# #
# Calculation of Simulation Geometry Parameters #
# ############################################# #

Delta_max = pi/(2*Q)					# maximum coil side-width angle          [rad]
#Delta = kc_pitch #coil side width angle in radians
Delta = kc*Delta_max
#k_Delta   = Delta/Delta_max   # coil side-width factor
k_Delta = kc

alpha = 0.0 if Type=='II' \
  else (Delta_max-Delta)                 # 1st coil's starting angle

theta = (4*pi/Q-pi/poles)*(poles/2)+2*pi/3 if Type=='O' \
  else -(pi/Q-pi/poles)*(poles/2)                # phase current's phase angle for a PF=1.0

k_m = kmr
hipg_seg,mag_seg=farey.farey((1-k_m)/(2*k_m))  # calculate the half inter-pole gap number of segment &
#                                         # the magnet number of segment
#                                         
k_m       = mag_seg/(2*hipg_seg+mag_seg) # calculate the revised k_m value#
mag_seg = 4 #this line was not present in rfapm.py

theta_p = magnet_radially_pitch
theta_m 	= theta_p*k_m 			           # the "revised" magnet's arc angle  [rad]                                         
#theta_m = kmr*theta_p#might need to multiply also with 0.5 (Andreas Joss edit)


# ###################### #
# Start Ansoft Maxwell2D #
# ###################### #

oAnsoftApp=Dispatch("AnsoftMaxwell.MaxwellScriptInterface")
#oAnsoftApp.SetNumberOfProcessors(2, "Maxwell 2D") #obsolete statement

oDesktop=oAnsoftApp.GetAppDesktop()
oDesktop.RestoreWindow()

oProject=oDesktop.NewProject()
design_name = "SORSPM"
oDesign=oProject.InsertDesign("Maxwell 2D", "SORSPM", "Transient", "")

oDesign.SetDesignSettings(["NAME:Design Settings Data",
                           "PreserveTranSolnAfterDatasetEdit:=", False,
                           "ComputeTransientInductance:=", LMC,
                           "PerfectConductorThreshold:=", 1E+030,
                           "InsulatorThreshold:=", 1,
                           "ModelDepth:=", "%gmeter" % (l/1000),#stacklength
                           "EnableTranTranLinkWithSimplorer:=", False,
                           "BackgroundMaterialName:=", "vacuum",
                           "Multiplier:=", "%d" % (periodicity_in_one_full_rotation)])

oEditor=oDesign.SetActiveEditor("3D Modeler")

oEditor.SetModelUnits(["NAME:Units Parameter", "Units:=", "mm"])
#oEditor.SetModelUnits(["NAME:Units Parameter", "Units:=", "m"])
#oEditor.SetModelUnits("m")

oEditor.PageSetup(["NAME:PageSetupData",
                   "margins:=", ["left:=", 500, "right:=", 500,  "top:=", 500,  "bottom:=", 500],
                   "border:=", 1,
                   "DesignVars:=", 0])

oModule=oDesign.GetModule("BoundarySetup")

EddyEffectVector=pyMaxwell2D.EddyEffect(oModule)

  # ######################## #
  # Create Magnetic Material #
  # ######################## #
if PM and magnetisation_radial0_normal1 == 0:

  oDefinitionManager=oProject.GetDefinitionManager()
  
  B_r = Brem
  H_c       = 1050000           # the maximum coercivity force           [A/m]
  mag_name  ="NdBFe N48"        # the name of permanent magnets used
  mu_0=4*pi*1E-7
#  mu_recoil=B_r/(mu_0*H_c)
  mu_recoil = muR
  
  # Create Magnet in the Cylindrical Coordinate System with a Positive Radial Magnitisation
  mag_name_pos_rad=mag_name+" (+R)"
  if not oDefinitionManager.DoesMaterialExist("%s" % mag_name_pos_rad):
      oDefinitionManager.AddMaterial(["NAME:%s" % mag_name_pos_rad,
                                "CoordinateSystemType:=",  "Cylindrical",
                                ["NAME:AttachedData"],
                                ["NAME:ModifierData"],
                                "permeability:=", "%g" % mu_recoil,
                                ["NAME:magnetic_coercivity",
                                 "property_type:=", "VectorProperty",
                                 "Magnitude:=", "-%gA_per_meter" % H_c,
                                 "DirComp1:=", "1",
                                 "DirComp2:=", "0",
                                 "DirComp3:=", "0"]])
                                 
      # Create Magnet in the Cylindrical Coordinate System with a Negative Radial Magnitisation
  mag_name_neg_rad=mag_name+" (-R)"
  if not oDefinitionManager.DoesMaterialExist("%s" % mag_name_neg_rad):
      oDefinitionManager.AddMaterial(["NAME:%s" % mag_name_neg_rad,
                                "CoordinateSystemType:=",  "Cylindrical",
                                ["NAME:AttachedData"],
                                ["NAME:ModifierData"],
                                "permeability:=", "%g" % mu_recoil,
                                ["NAME:magnetic_coercivity",
                                 "property_type:=", "VectorProperty",
                                 "Magnitude:=", "-%gA_per_meter" % H_c,
                                 "DirComp1:=", "-1",
                                 "DirComp2:=", "0",
                                 "DirComp3:=", "0"]])
                                     
#   # material with positive circumferential magnetisation.                                  
#  mag_name_pos_ang=mag_name+" (+A)"
#  if not oDefinitionManager.DoesMaterialExist("%s" % mag_name_pos_ang):
#    oDefinitionManager.AddMaterial(["NAME:%s" % mag_name_pos_ang,
#                                    "CoordinateSystemType:=",  "Cylindrical",
#                                    ["NAME:AttachedData"],
#                                    ["NAME:ModifierData"],
#                                    "permeability:=", "%g" % mu_recoil,
#                                    ["NAME:magnetic_coercivity",
#                                     "property_type:=", "VectorProperty",
#                                     "Magnitude:=", "-%gA_per_meter" % H_c,
#                                     "DirComp1:=", "0",
#                                     "DirComp2:=", "1",
#                                     "DirComp3:=", "0"]])
#                                   
#
#   # material with negative circumferential magnetisation.                                  
#  mag_name_neg_ang=mag_name+" (-A)"
#  if not oDefinitionManager.DoesMaterialExist("%s" % mag_name_neg_ang):
#    oDefinitionManager.AddMaterial(["NAME:%s" % mag_name_neg_ang,
#                                    "CoordinateSystemType:=",  "Cylindrical",
#                                    ["NAME:AttachedData"],
#                                    ["NAME:ModifierData"],
#                                    "permeability:=", "%g" % mu_recoil,
#                                    ["NAME:magnetic_coercivity",
#                                     "property_type:=", "VectorProperty",
#                                     "Magnitude:=", "-%gA_per_meter" % H_c,
#                                     "DirComp1:=", "0",
#                                     "DirComp2:=", "-1",
#                                     "DirComp3:=", "0"]])
    
# ############ #
# Draw Magnets #
# ############ #

# Compute Inner Magnet's Complex Coordinates
#inner_mag_cplx_pnts=poly.peel(r_n-h/2-l_g-h_ip, h_ip, (theta_p-theta_m)/2-pi*sim_slice, (theta_p-theta_m)/2+theta_m-pi*sim_slice, vertices=mag_seg+1)

# Compute Outer Magnet's Complex Coordinates
#outer_mag_cplx_pnts=poly.peel(r_n+h/2+l_g, h_op, (theta_p-theta_m)/2-pi*sim_slice, (theta_p-theta_m)/2+theta_m-pi*sim_slice, vertices=mag_seg+1)
mag_center_pnt_cplx = []
mag_center_pnt_cplx.append(complex(magnet_radially_inner_radius+0.5*hmr,0))
mag_center_pnt_cplx[0]=mag_center_pnt_cplx[0]*np.exp(0.5*(2j*pi/poles))

for i in range(int(poles*sim_slice)):
  # Shift outer poles one pole pitch
  if square_magnets == 0:
      mag_center_pnt_cplx.append(mag_center_pnt_cplx[i]*np.exp(2j*pi/poles))
  elif square_magnets == 1:
      if i==0:
          mag_center_pnt_cplx.append(mag_center_pnt_cplx[i]*np.exp(2j*pi/poles))
      elif i == 1:
          mag_center_pnt_cplx.append(mag_center_pnt_cplx[i]*np.exp(-6j*pi/poles))
      elif i == 2:
          mag_center_pnt_cplx.append(mag_center_pnt_cplx[i]*np.exp(2j*pi/poles))

if square_magnets == 0:
    vertices=mag_seg+1
    outer_mag_cplx_pnts=poly.peel(magnet_radially_inner_radius, hmr-intersect_spacing, (theta_p-theta_m)/2-pi*sim_slice, (theta_p-theta_m)/2+theta_m-pi*sim_slice, vertices=vertices)
elif square_magnets == 1:
    magnet_side_length = kmr*magnet_radially_pitch*magnet_radially_inner_radius
    
    mag_cplx_1 = complex(magnet_radially_inner_radius,0.5*magnet_side_length)
    mag_cplx_2 = complex(magnet_radially_inner_radius+hmr,0.5*magnet_side_length)
    mag_cplx_3 = complex(magnet_radially_inner_radius+hmr,-0.5*magnet_side_length)
    mag_cplx_4 = complex(magnet_radially_inner_radius,-0.5*magnet_side_length)
    mag_cplx_5 = complex(magnet_radially_inner_radius,0.5*magnet_side_length)

    outer_mag_cplx_pnts=np.array([mag_cplx_1,mag_cplx_2,mag_cplx_3,mag_cplx_4,mag_cplx_5])
    outer_mag_cplx_pnts*=np.exp(0.5*(2j*pi/poles))
    
        
################################################################## 
  # ######################## #
  # Create Magnetic Material #
  # ######################## #
    if PM and magnetisation_radial0_normal1 == 1:
        oDefinitionManager=oProject.GetDefinitionManager()
      
        B_r = Brem
        H_c       = 1050000           # the maximum coercivity force           [A/m]
        mag_name  ="NdBFe N48"        # the name of permanent magnets used
        mag_pos_names = []
        mag_neg_names = []
        mu_0=4*pi*1E-7
    #   mu_recoil=B_r/(mu_0*H_c)
        mu_recoil = muR
      
        for i in range(int(poles*sim_slice)): #give the magnets numbers as names, starting from the bottom and ending with highest number at the top
          # Create Magnet in the Cylindrical Coordinate System with a Positive Radial Magnitisation
          mag_pos_names.append(mag_name+" (+R%d)"%(i+1))
          if not oDefinitionManager.DoesMaterialExist("%s" % mag_pos_names[i]):
              oDefinitionManager.AddMaterial(["NAME:%s" % mag_pos_names[i],
                                        "CoordinateSystemType:=",  "Cartesian",
                                        ["NAME:AttachedData"],
                                        ["NAME:ModifierData"],
                                        "permeability:=", "%g" % mu_recoil,
                                        ["NAME:magnetic_coercivity",
                                         "property_type:=", "VectorProperty",
                                         "Magnitude:=", "-%gA_per_meter" % H_c,
                                         "DirComp1:=", "%g"%(mag_center_pnt_cplx[i].real/abs(mag_center_pnt_cplx[i])),
                                         "DirComp2:=", "%g"%(mag_center_pnt_cplx[i].imag/abs(mag_center_pnt_cplx[i])),
                                         "DirComp3:=", "0"]])
                                         
              # Create Magnet in the Cylindrical Coordinate System with a Negative Radial Magnitisation
          mag_neg_names.append(mag_name+" (-R%d)"%(i+1))
          if not oDefinitionManager.DoesMaterialExist("%s" % mag_neg_names[i]):
              oDefinitionManager.AddMaterial(["NAME:%s" % mag_neg_names[i],
                                        "CoordinateSystemType:=",  "Cartesian",
                                        ["NAME:AttachedData"],
                                        ["NAME:ModifierData"],
                                        "permeability:=", "%g" % mu_recoil,
                                        ["NAME:magnetic_coercivity",
                                         "property_type:=", "VectorProperty",
                                         "Magnitude:=", "-%gA_per_meter" % H_c,
                                         "DirComp1:=", "%g"%(-mag_center_pnt_cplx[i].real/abs(mag_center_pnt_cplx[i])),
                                         "DirComp2:=", "%g"%(-mag_center_pnt_cplx[i].imag/abs(mag_center_pnt_cplx[i])),
                                         "DirComp3:=", "0"]])
#################################################################    

outer_magnet_names=[]
# Draws all Inner & Outer Magnets
for i in range(int(poles*sim_slice)):
  if magnetisation_radial0_normal1 == 0:
    if (i % 2):
      # Odd pair of magnets
      if Type=='O':
        color=RED
        magnet_material="vacuum" if not PM else mag_name_pos_rad
      else:
        color=BLUE
        magnet_material="vacuum" if not PM else mag_name_neg_rad
    else:
      # Even pair of magnets
      if Type=='O':
        color=BLUE
        magnet_material="vacuum" if not PM else mag_name_neg_rad
      else:
        color=RED
        magnet_material="vacuum" if not PM else mag_name_pos_rad

  elif magnetisation_radial0_normal1 == 1:
    if (i % 2):
        color=BLUE
        magnet_material="vacuum" if not PM else mag_neg_names[i]
    else:
        color=RED
        magnet_material="vacuum" if not PM else mag_pos_names[i]        
        
        
#  inner_magnet_name="Inner_Magnet_%d" % (i)
#  inner_magnet_names.append(inner_magnet_name)
  
#  InnerMagnet=pyMaxwell2D.Polyline(inner_mag_cplx_pnts,
#    IsPolylineCovered=True,
#    IsPolylineClosed=True,
#    Name=inner_magnet_name,
#    Color=color,
#    MaterialName=magnet_material)
#      
#  InnerMagnet.Create(oEditor)
#  
#  EddyEffectVector.Append(inner_magnet_name,True)
#  
#  # Shift inner poles one pole pitch
#  inner_mag_cplx_pnts*=np.exp(2j*pi/poles)
      
  outer_magnet_name="Outer_Magnet_%d" % (i)
  outer_magnet_names.append(outer_magnet_name)
  
  OuterMagnet=pyMaxwell2D.Polyline(outer_mag_cplx_pnts,
    IsPolylineCovered=True,
    IsPolylineClosed=True,
    Name=outer_magnet_name,
    Color=color,
    MaterialName=magnet_material)

  OuterMagnet.Create(oEditor)

  EddyEffectVector.Append(outer_magnet_name,True)

  # Shift outer poles one pole pitch
  if square_magnets == 0:
      outer_mag_cplx_pnts*=np.exp(2j*pi/poles)
  elif square_magnets == 1:
      if i==0:
          outer_mag_cplx_pnts*=np.exp(2j*pi/poles)
      elif i == 1:
          outer_mag_cplx_pnts*=np.exp(-6j*pi/poles)
      elif i == 2:
          outer_mag_cplx_pnts*=np.exp(2j*pi/poles)
      if i == 3:
          outer_mag_cplx_pnts*=np.exp(-2j*pi/poles) #this last rotation does not affect magnet placement, but is done to conveniantly place points for the rotor yoke calculations which follow later
      
#  magnet_names=inner_magnet_names+outer_magnet_names
  magnet_names=outer_magnet_names


# ######### #
# Draw Yoke #
# ######### #

inner_yoke_name="Inner_Yoke"
outer_yoke_name="Outer_Yoke"

if AIR_YOKES == True:
    yoke_material_name="vacuum"
elif AIR_YOKES == False:
    yoke_material_name="steel_1010"

oDefinitionManager = oProject.GetDefinitionManager()
oDefinitionManager.EditMaterial("steel_1010", 
	[
		"NAME:steel_1010",
		"CoordinateSystemType:=", "Cartesian",
		[
			"NAME:AttachedData"
		],
		[
			"NAME:ModifierData"
		],
		"permittivity:="	, "1",
		[
			"NAME:permeability",
			"property_type:="	, "nonlinear",
			"BType:="		, "normal",
			"HUnit:="		, "A_per_meter",
			"BUnit:="		, "tesla",
			[
				"NAME:BHCoordinates",
				[
					"NAME:Coordinate",
					"X:="			, 0,
					"Y:="			, 0
				],
				[
					"NAME:Coordinate",
					"X:="			, 238.7,
					"Y:="			, 0.2003
				],
				[
					"NAME:Coordinate",
					"X:="			, 318.3,
					"Y:="			, 0.3204
				],
				[
					"NAME:Coordinate",
					"X:="			, 358.1,
					"Y:="			, 0.40045
				],
				[
					"NAME:Coordinate",
					"X:="			, 437.7,
					"Y:="			, 0.50055
				],
				[
					"NAME:Coordinate",
					"X:="			, 477.5,
					"Y:="			, 0.5606
				],
				[
					"NAME:Coordinate",
					"X:="			, 636.6,
					"Y:="			, 0.7908
				],
				[
					"NAME:Coordinate",
					"X:="			, 795.8,
					"Y:="			, 0.931
				],
				[
					"NAME:Coordinate",
					"X:="			, 1114.1,
					"Y:="			, 1.1014
				],
				[
					"NAME:Coordinate",
					"X:="			, 1273.2,
					"Y:="			, 1.2016
				],
				[
					"NAME:Coordinate",
					"X:="			, 1591.5,
					"Y:="			, 1.302
				],
				[
					"NAME:Coordinate",
					"X:="			, 2228.2,
					"Y:="			, 1.4028
				],
				[
					"NAME:Coordinate",
					"X:="			, 3183.1,
					"Y:="			, 1.524
				],
				[
					"NAME:Coordinate",
					"X:="			, 4774.6,
					"Y:="			, 1.626
				],
				[
					"NAME:Coordinate",
					"X:="			, 6366.2,
					"Y:="			, 1.698
				],
				[
					"NAME:Coordinate",
					"X:="			, 7957.7,
					"Y:="			, 1.73
				],
				[
					"NAME:Coordinate",
					"X:="			, 15915.5,
					"Y:="			, 1.87
				],
				[
					"NAME:Coordinate",
					"X:="			, 31831,
					"Y:="			, 1.99
				],
				[
					"NAME:Coordinate",
					"X:="			, 47746.5,
					"Y:="			, 2.04
				],
				[
					"NAME:Coordinate",
					"X:="			, 63662,
					"Y:="			, 2.07
				],
				[
					"NAME:Coordinate",
					"X:="			, 79577.5,
					"Y:="			, 2.095
				],
				[
					"NAME:Coordinate",
					"X:="			, 159155,
					"Y:="			, 2.2
				],
				[
					"NAME:Coordinate",
					"X:="			, 318310,
					"Y:="			, 2.4
				]
			]
		],
		"conductivity:="	, "2000000",
		"dielectric_loss_tangent:=", "0",
		"magnetic_loss_tangent:=", "0",
		[
			"NAME:magnetic_coercivity",
			"property_type:="	, "VectorProperty",
			"Magnitude:="		, "0",
			"DirComp1:="		, "1",
			"DirComp2:="		, "0",
			"DirComp3:="		, "0"
		],
		"thermal_conductivity:=", "45",
		"saturation_mag:="	, "0gauss",
		"lande_g_factor:="	, "2",
		"delta_H:="		, "0Oe",
		"mass_density:="	, "7872",
		[
			"NAME:stacking_type",
			"property_type:="	, "ChoiceProperty",
			"Choice:="		, "Lamination"
		],
		"stacking_factor:="	, "0.95",
		[
			"NAME:stacking_direction",
			"property_type:="	, "ChoiceProperty",
			"Choice:="		, "V(3)"
		],
		"specific_heat:="	, "448",
		"youngs_modulus:="	, "205000000000",
		"poissons_ratio:="	, "0.28",
		"thermal_expansion_coeffcient:=", "1.22e-005"
	])

# Inner Yoke 
inner_yoke_cplx_pnts=poly.peel(stator_yoke_inner_radius, hyi-intersect_spacing, -pi*sim_slice, pi*sim_slice, vertices=(2*hipg_seg+mag_seg)*int(sim_slice*poles)+1)

InnerYoke=pyMaxwell2D.Polyline(inner_yoke_cplx_pnts,
  IsPolylineCovered=True,
  IsPolylineClosed=True,
  Name=inner_yoke_name,
  MaterialName=yoke_material_name)

# Outer Yoke
#outer_yoke_cplx_pnts=poly.peel(magnet_radially_outer_radius, hyo, -pi*sim_slice, pi*sim_slice, vertices=(2*hipg_seg+mag_seg)*int(sim_slice*poles)+1) hipg_seg=3,mag_seg=4
#print "Outer Yoke vertices"
vertices = (2*hipg_seg+mag_seg)*int(sim_slice*poles)+1 #hipg_seg=3,mag_seg=4,sim_slice=0.1111,poles=36
#print(vertices)
#vertices = 3*vertices
#print(vertices)

if square_magnets == 0:
    outer_yoke_cplx_pnts=poly.peel(magnet_radially_outer_radius, hyo, -pi*sim_slice, pi*sim_slice, vertices=vertices)
elif square_magnets == 1:
    outer_yoke_cplx_pnts = []

    #inner arc of rotor yoke
    #add points from the bottom, going to the top
    outer_yoke_cplx_pnts.append(magnet_radially_outer_radius*np.exp(1j*(-pi*sim_slice))) #start point
    
    for i in range(0,4):
        outer_yoke_cplx_pnts.append(outer_mag_cplx_pnts[2])
        outer_yoke_cplx_pnts.append(outer_mag_cplx_pnts[1])
    
        outer_mag_cplx_pnts*=np.exp(2j*pi/poles)
    
    outer_yoke_cplx_pnts.append(magnet_radially_outer_radius*np.exp(1j*(pi*sim_slice))) #end point   
       
    #outer arc of rotor yoke
    #add points from the top, going to the bottom
    outer_yoke_cplx_pnts.append(ryo*np.exp(1j*(pi*sim_slice))) #start point
    
    for i in range(0,vertices):
        outer_yoke_cplx_pnts.append(ryo*np.exp(1j*((pi*sim_slice)-(((i+1)/vertices)*2*pi*sim_slice))))
    
    outer_yoke_cplx_pnts.append(magnet_radially_outer_radius*np.exp(1j*(-pi*sim_slice))) #end point    
        
OuterYoke=pyMaxwell2D.Polyline(outer_yoke_cplx_pnts,
  IsPolylineCovered=True,
  IsPolylineClosed=True,
  Name=outer_yoke_name,
  MaterialName=yoke_material_name)

InnerYoke.Create(oEditor)
OuterYoke.Create(oEditor)

EddyEffectVector.Append(inner_yoke_name,True)
EddyEffectVector.Append(outer_yoke_name,True)  


# ################### #
# Draw Rotor Surfaces #
# ################### #

#creates mesh region for rotor magnets and the empty spaces between the magnets (TO BE USED AS BAND REGION)
outer_rotor_cplx_pnts=poly.peel(magnet_radially_inner_radius-0.25*g, hmr+hyo+g, -pi*sim_slice, pi*sim_slice, vertices=(2*hipg_seg+mag_seg)*int(sim_slice*poles)+1)

Outer_Rotor=pyMaxwell2D.Polyline(outer_rotor_cplx_pnts,
  IsPolylineCovered=True,
  IsPolylineClosed=True,
  Name="OuterRotor",
  Color=GRAY)

Outer_Rotor.Create(oEditor)
#EddyEffectVector.Append("OuterRotor",True) #Andreas Joss added this (it didnt affect anything looks like it)

if WIND:
  # ################ #	
  # Windings & Coils #
  # ################ #
  oModule.AssignWindingGroup(["NAME:Winding_A",
                              "Type:=", "Current",
                              "IsSolid:=", False,
                              "Current:=", "%g*cos(2*pi*%g*(Time+%g)-(%g))" % (I_p,f_e,time_shift,shift_current_electrical_angle*(np.pi/180)),
                              "ParallelBranchesNum:=", "%d" % (a)])

  if mmf_wave_clockwise == 0: #original
      oModule.AssignWindingGroup(["NAME:Winding_B",
                                  "Type:=", "Current",
                                  "IsSolid:=", False,
                                  "Current:=", "%g*cos(2*pi*%g*(Time+%g)-2*pi/3-(%g))" % (I_p,f_e,time_shift,shift_current_electrical_angle*(np.pi/180)),
                                  "ParallelBranchesNum:=", "%d" % (a)])
    
      oModule.AssignWindingGroup(["NAME:Winding_C",
                                  "Type:=", "Current",
                                  "IsSolid:=", False,
                                  "Current:=", "%g*cos(2*pi*%g*(Time+%g)-4*pi/3-(%g))" % (I_p,f_e,time_shift,shift_current_electrical_angle*(np.pi/180)),
                                  "ParallelBranchesNum:=", "%d" % (a)])
  elif mmf_wave_clockwise == 1:
      oModule.AssignWindingGroup(["NAME:Winding_B",
                                  "Type:=", "Current",
                                  "IsSolid:=", False,
                                  "Current:=", "%g*cos(2*pi*%g*(Time+%g)-4*pi/3-(%g))" % (I_p,f_e,time_shift,shift_current_electrical_angle*(np.pi/180)),
                                  "ParallelBranchesNum:=", "%d" % (a)])
    
      oModule.AssignWindingGroup(["NAME:Winding_C",
                                  "Type:=", "Current",
                                  "IsSolid:=", False,
                                  "Current:=", "%g*cos(2*pi*%g*(Time+%g)-2*pi/3-(%g))" % (I_p,f_e,time_shift,shift_current_electrical_angle*(np.pi/180)),
                                  "ParallelBranchesNum:=", "%d" % (a)])
      
  # Compute first Coil's Complex Coordinates
  if coil_shape == RADIALLY_STRAIGHT:
    coil_cplx_pnts=poly.peel(stator_yoke_outer_radius, hc, alpha-pi*sim_slice, alpha+2*Delta-pi*sim_slice, vertices=5)

    LABEL=['Bp', 'Bn', 'Ap', 'An', 'Cp', 'Cn'] #original sequence as obtained from rfapm.py
    COLOR=[BLUE, BLUE, RED, RED, YELLOW, YELLOW] #original sequence    
    
  elif coil_shape == SQUARE_SLOTS:   
#    cplx1 = complex(x-coil_yoke_spacing,half_teeth_width+coil_teeth_spacing)
#    cplx3 = complex(stator_yoke_outer_radius+coil_yoke_spacing,half_teeth_width+coil_teeth_spacing)
#    cplx4 = complex(stator_yoke_outer_radius+coil_yoke_spacing,y-coil_opposing_spacing)
#    cplx5 = complex(x-coil_yoke_spacing,y-coil_opposing_spacing)
#    cplx6 = complex(x-coil_yoke_spacing,half_teeth_width+coil_teeth_spacing)

    cplx1 = complex(x,half_teeth_width+coil_teeth_spacing)
    cplx3 = complex(stator_yoke_outer_radius+coil_yoke_spacing,half_teeth_width+coil_teeth_spacing)
    cplx4 = complex(stator_yoke_outer_radius+coil_yoke_spacing,y)
    cplx5 = complex(x,y)
    cplx6 = complex(x,half_teeth_width+coil_teeth_spacing)
    
    coil_cplx_pnts=np.array([cplx1,cplx3,cplx4,cplx5,cplx6])
    start = coil_cplx_pnts
#    coil_cplx_pnts=poly.peel(stator_yoke_outer_radius, hc, alpha-pi*sim_slice, alpha+2*Delta-pi*sim_slice, vertices=5)

#      LABEL=['0', '1', '2', '3', '4', '5'] 
    LABEL=['An', 'Bn', 'Cn', 'Bp', 'Ap', 'Cp']
    COLOR=[RED, BLUE, YELLOW, BLUE, RED, YELLOW]

  circuit=-1

  coil_names=[]

  if coil_shape == RADIALLY_STRAIGHT:  

    for i in range(int(2*Q*sim_slice)):
      i = i-6 if i>=6 else i
      circuit = circuit+1 if (i % 6 == 0) else circuit
      coil_name='Coil_%s%d' % (LABEL[i % 6],circuit)
      coil_names.append(coil_name)
      coil_color=COLOR[i % 6]
      coil_polarity='Positive' if coil_name[-2]=='p' else 'Negative'
      coil_phase=coil_name[-3]
    
      if CLIP_COILS:
        if Type=='II':
          if i%2==0:
            coil_cplx_pnts*=np.exp(1j*np.radians(0.05))
          else:
            coil_cplx_pnts*=np.exp(-1j*np.radians(0.05))
    
      #the following instructions create a single coil side (START)
      CoilPolyline=pyMaxwell2D.Polyline(coil_cplx_pnts,
        IsPolylineCovered=True,
        IsPolylineClosed=True,
        Name=coil_name,
        Color=coil_color,
        MaterialName="copper")
      
      CoilPolyline.Create(oEditor)
    #the following instructions create a single coil side (END)

      if CLIP_COILS:
        if Type=='II':
          if i%2==0:
            coil_cplx_pnts*=np.exp(1j*np.radians(0.05))
          else:
            coil_cplx_pnts*=np.exp(-1j*np.radians(0.05))    
    
      oModule.AssignCoil(["NAME:%s" % coil_name,
                        "Objects:=", ["%s" % coil_name],
                        "Conductor number:=", "%s" % N,
                        "PolarityType:=", "%s" % coil_polarity])

      oModule.EditCoil("%s" % coil_name,
                     ["NAME:%s" % coil_name,
                      "ParentBndID:=", "Winding_%s" % coil_phase,
                      "Winding:=", "Winding_%s" % coil_phase])
    
      EddyEffectVector.Append(coil_name,True)

      if Type=='II':
        if coil_polarity=='Negative':
          coil_cplx_pnts*=np.exp(1j*2*Delta)
        else:
          coil_cplx_pnts*=np.exp(2j*(pi/Q-Delta))
      else:
        coil_cplx_pnts*=np.exp(1j*pi/Q)
    
        
        
  elif coil_shape == SQUARE_SLOTS: 
    for k in range(0,6):
#      i = i-6 if i>=6 else i
      circuit = circuit+1 if (k % 6 == 0) else circuit
      coil_name='Coil_%s%d' % (LABEL[k % 6],circuit)
      coil_names.append(coil_name)
      coil_color=COLOR[k % 6]
      coil_polarity='Positive' if coil_name[-2]=='p' else 'Negative'
      coil_phase=coil_name[-3]        
        
      #the following instructions create a single coil side (START)
      CoilPolyline=pyMaxwell2D.Polyline(coil_cplx_pnts,
        IsPolylineCovered=True,
        IsPolylineClosed=True,
        Name=coil_name,
        Color=coil_color,
        MaterialName="copper")
    
      CoilPolyline.Create(oEditor)
      #the following instructions create a single coil side (END)
     
      oModule.AssignCoil(["NAME:%s" % coil_name,
                        "Objects:=", ["%s" % coil_name],
                        "Conductor number:=", "%s" % N,
                        "PolarityType:=", "%s" % coil_polarity])

      oModule.EditCoil("%s" % coil_name,
                     ["NAME:%s" % coil_name,
                      "ParentBndID:=", "Winding_%s" % coil_phase,
                      "Winding:=", "Winding_%s" % coil_phase])      
      
      
      
#      coil_cplx_pnts*=(-1j) #-90 degree rotation
#      coil_cplx_pnts*=np.exp(-1j*pi) #180 degree rotation
#      coil_cplx_pnts*=np.exp(1j*pi) #180 degree rotation
#      coil_cplx_pnts*=np.exp(-1j*(pi/2)) #-90 degree rotation

      if k==0:
         coil_cplx_pnts*=np.exp(-2j*(pi/Q))  

      if k==1:
         coil_cplx_pnts*=np.exp(+4j*(pi/Q)) 
      
      if k==2:
         coil_cplx_pnts = start
         for j in range(0,len(coil_cplx_pnts)):
           coil_cplx_pnts[j] = complex(coil_cplx_pnts[j].real,-1*coil_cplx_pnts[j].imag)         

      if k==3:
         coil_cplx_pnts*=np.exp(2j*(pi/Q))
       
      if k==4:
         coil_cplx_pnts*=np.exp(2j*(pi/Q))         



        
# ############## #	
# Stator Teeth   #
# ############## #
#print "dont_draw_stator_teeth = %d"%dont_draw_stator_teeth
#print "dont_draw_stator_teeth_air = %d"%dont_draw_stator_teeth_air
#print "hs_ratio = %.3f"%hs_ratio

#if coil_shape == RADIALLY_STRAIGHT:
if dont_draw_stator_teeth == 0: 
#    print "ja1"
    stator_teeth_names = []
    angle_temp = 0
    for i in range(3):
        stator_teeth_names.append('Stator_Teeth_%d' % (i+1))
        if coil_shape == RADIALLY_STRAIGHT:
            teeth_cplx_pnts=poly.peel(stator_yoke_outer_radius, hc*hs_ratio, alpha+2*Delta-pi*sim_slice+angle_temp, alpha+coil_pitch-2*Delta-pi*sim_slice+angle_temp, vertices=5)  
        elif coil_shape == SQUARE_SLOTS:
            inner_curve_r = stator_yoke_outer_radius
            inner_curve_pitch = ks_pitch
            phi=np.linspace(-0.5*inner_curve_pitch+angle_temp,0.5*inner_curve_pitch+angle_temp,8)     
            
            teeth_cplx_pnts = []

            for k in range(0,len(phi)):
                teeth_cplx_pnts.append(inner_curve_r*np.exp(1j*phi[k]))

            outer_curve_r = stator_yoke_outer_radius+(stator_coil_radius-stator_yoke_outer_radius)*hs_square_slots_ratio
            outer_curve_pitch = 2*np.arcsin(half_teeth_width/stator_coil_radius)
            phi=np.linspace(0.5*outer_curve_pitch+angle_temp,-0.5*outer_curve_pitch+angle_temp,8)    

            for k in range(0,len(phi)):
                teeth_cplx_pnts.append(outer_curve_r*np.exp(1j*phi[k]))

            teeth_cplx_pnts.append(teeth_cplx_pnts[0])
    
            
            
        StatorTeeth=pyMaxwell2D.Polyline(teeth_cplx_pnts,
          IsPolylineCovered=True,
          IsPolylineClosed=True,
          Name=stator_teeth_names[i],
          MaterialName=yoke_material_name)
        
        StatorTeeth.Create(oEditor)
        EddyEffectVector.Append(stator_teeth_names[i],True)
    
    #    teeth_cplx_pnts*=np.exp(1j*pi/Q)
        if coil_shape == RADIALLY_STRAIGHT:
            angle_temp = angle_temp + coil_pitch
        elif coil_shape == SQUARE_SLOTS:
            if i == 0:
                angle_temp = angle_temp + coil_pitch
            elif i == 1:
                angle_temp = angle_temp -2*coil_pitch
                
if dont_draw_stator_teeth_air == 0:    
#    print "ja2"
    stator_teeth_air_names = []
    angle_temp = 0
    if coil_shape == RADIALLY_STRAIGHT:
      for i in range(3):
          stator_teeth_air_names.append('Stator_Teeth_Air_%d' % (i+1))
          teeth_air_cplx_pnts=poly.peel(stator_yoke_outer_radius+hc*hs_ratio, stator_coil_radius-(stator_yoke_outer_radius+hc*hs_ratio), alpha+2*Delta-pi*sim_slice+angle_temp, alpha+coil_pitch-2*Delta-pi*sim_slice+angle_temp, vertices=5)  
          StatorTeethAir=pyMaxwell2D.Polyline(teeth_air_cplx_pnts,
            IsPolylineCovered=True,
            IsPolylineClosed=True,
            Name=stator_teeth_air_names[i],
            Color=GRAY,
            MaterialName="vacuum")
        
          StatorTeethAir.Create(oEditor)
          EddyEffectVector.Append(stator_teeth_air_names[i],True)
    
    #    teeth_cplx_pnts*=np.exp(1j*pi/Q)
          angle_temp = angle_temp + coil_pitch        
 
     
# ############## #	
# Stator Surface #
# ############## #
statvert=(2*hipg_seg+mag_seg)*int(sim_slice*poles)+1
#
##this code-section defines the overall area in which the stator exists, so that later on we can define mesh properties specific to this region. This is the only use of this code as far as I'm aware of.
#stator_cplx_pnts=poly.peel(stator_yoke_outer_radius+0.5*g, hc, -pi*sim_slice, pi*sim_slice, vertices=statvert)
#
#Stator=pyMaxwell2D.Polyline(stator_cplx_pnts,
#  IsPolylineCovered=True,
#  IsPolylineClosed=True,
#  Name="Stator",
#  Color=GRAY)
#
#Stator.Create(oEditor)
#
#EddyEffectVector.Append("Stator",True)
#
#EddyEffectVector.Set()

# ################# #	
# Moving Band Setup #
# ################# #
oModule=oDesign.GetModule("ModelSetup")

#oModule.AssignBand(["NAME:Data",
#                    "Move Type:=", "Rotate",
#                    "Coordinate System:=", "Global",
#                    "Axis:=", "Z",
#                    "Is Positive:=", True,
#                    "InitPos:=", "0deg",
#                    "HasRotateLimit:=", False,
#                    "NegativePos:=", "0deg",
#                    "PositivePos:=", "-90deg",
#                    "NonCylindrical:=", False,
#                    "Consider Mechanical Transient:=", False,
#                    "Angular Velocity:=", "%grpm" % n,
#                    "Objects:=", ["InnerRotor"]])

oModule.AssignBand(["NAME:Data",
                    "Move Type:=", "Rotate",
                    "Coordinate System:=", "Global",
                    "Axis:=", "Z",
                    "Is Positive:=", True,
                    "InitPos:=", "%gdeg"%initial_angle,
                    "HasRotateLimit:=", False,
                    "NegativePos:=", "0deg",
                    "PositivePos:=", "-90deg",
                    "NonCylindrical:=", False,
                    "Consider Mechanical Transient:=", False,
                    "Angular Velocity:=", "%grpm" % n_rpm,
                    "Objects:=", ["OuterRotor"]]) #this instruction causes error--NOT ANYMORE! :) this was due to band region (OuterRotor) being to small

# ################# #
# Flux Density Arcs #
# ################# #
arcvert=int((statvert-1)/2)

Br_rn_cplx_pnts=poly.arc(magnet_radially_inner_radius-0.5*g,-pi*sim_slice,pi*sim_slice,vertices=arcvert)
Br_rn=pyMaxwell2D.Polyline(Br_rn_cplx_pnts,True,False,"Br_rn",GRAY)
Br_rn.Create(oEditor)

#Br_cim_cplx_pnts=poly.arc(magnet_radially_inner_radius-g,-pi*sim_slice,pi*sim_slice,vertices=arcvert)
#Br_cim=pyMaxwell2D.Polyline(Br_cim_cplx_pnts,True,False,"Br_cim",GRAY)
#Br_cim.Create(oEditor)

#Br_com_cplx_pnts=poly.arc(magnet_radially_inner_radius,-pi*sim_slice,pi*sim_slice,vertices=arcvert)
##Br_com_cplx_pnts=poly.arc(magnet_radially_inner_radius-intersect_spacing,-pi*sim_slice,pi*sim_slice,vertices=arcvert)
#Br_com=pyMaxwell2D.Polyline(Br_com_cplx_pnts,True,False,"Br_com",GRAY)
#Br_com.Create(oEditor)    

#if not WIND:
#  Br_r_ciy_cplx_pnts=poly.arc(r_n-h/2-l_g-h_ip-h_iy/2,-pi*sim_slice,pi*sim_slice,vertices=arcvert*8)
#  Br_r_ciy=pyMaxwell2D.Polyline(Br_r_ciy_cplx_pnts,True,False,"Br_r_ciy",GRAY)
#  Br_r_ciy.Create(oEditor)
#
#  Br_r_coy_cplx_pnts=poly.arc(r_n+h/2+l_g+h_op+h_oy/2,-pi*sim_slice,pi*sim_slice,vertices=arcvert*8)
#  Br_r_coy=pyMaxwell2D.Polyline(Br_r_coy_cplx_pnts,True,False,"Br_r_coy",GRAY)
#  Br_r_coy.Create(oEditor)
#
#  Br_rnph2_cplx_pnts=poly.arc(r_n+h/2,-pi*sim_slice,pi*sim_slice,vertices=arcvert)
#  Br_rnph2=pyMaxwell2D.Polyline(Br_rnph2_cplx_pnts,True,False,"Br_rnph2",GRAY)
#  Br_rnph2.Create(oEditor)
#
#  Br_rnmh2_cplx_pnts=poly.arc(r_n-h/2,-pi*sim_slice,pi*sim_slice,vertices=arcvert)
#  Br_rnmh2=pyMaxwell2D.Polyline(Br_rnmh2_cplx_pnts,True,False,"Br_rnmh2",GRAY)
#  Br_rnmh2.Create(oEditor)

# ########## #	
# Boundaries # outer region of simulation
# ########## #

boundary_cplx_pnts=poly.peel(0.98*stator_yoke_inner_radius, (1.02*ryo)-(0.98*stator_yoke_inner_radius), -pi*sim_slice, pi*sim_slice, vertices=int(80*sim_slice+1))
#boundary_cplx_pnts=poly.peel(0.95*stator_yoke_inner_radius, (ryo)-(stator_yoke_inner_radius), -pi*sim_slice, pi*sim_slice, vertices=int(80*sim_slice+1))

Boundary=pyMaxwell2D.Polyline(boundary_cplx_pnts,
  IsPolylineCovered=True,
  IsPolylineClosed=True,
  Name="Boundary",
  Color=GRAY)

boundary_edges=Boundary.Create(oEditor)

num_boundary_edges=len(boundary_edges)
slave_boundary_edge=boundary_edges.pop(num_boundary_edges//2-1)
master_boundary_edge=boundary_edges.pop(-1)

master_boudary_name="Master"
slave_boudary_name="Slave"

oModule=oDesign.GetModule("BoundarySetup")

oModule.AssignMaster(["NAME:%s" % (master_boudary_name),
                      "Edges:=", [master_boundary_edge],
                      "ReverseV:=", True])

oModule.AssignSlave(["NAME:%s" % (slave_boudary_name),
                     "Edges:=", [slave_boundary_edge],
                     "ReverseU:=", False,
                     "Master:=", "%s" % (master_boudary_name),
                     "SameAsMaster:=", True])

oModule.AssignVectorPotential(["NAME:VectorPotential",
                               "Edges:=", boundary_edges,
                               "Value:=", "0",
                               "CoordinateSystem:=", ""])

# ########################### #
# Core Loss & Eddy Loss Setup #
# ########################### #
if WIND:
  oModule = oDesign.GetModule("BoundarySetup")
  oModule.SetEddyEffect(
	[
		"NAME:Eddy Effect Setting",
		[
			"NAME:EddyEffectVector",
			[
				"NAME:Data",
				"Object Name:="		, "Inner_Yoke", #OK so according to F1 help, eddy effects should be disabled for laminated objects!! Solves my whole massive SOLIDLOSSES problem
				"Eddy Effect:="		, False
			],
			[
				"NAME:Data",
				"Object Name:="		, "Outer_Yoke", #OK so according to F1 help, eddy effects should be disabled for laminated objects!! Solves my whole massive SOLIDLOSSES problem
				"Eddy Effect:="		, False
			],
			[
				"NAME:Data",
				"Object Name:="		, "Coil_Bp0",
				"Eddy Effect:="		, EDDY_LOSSES
			],
			[
				"NAME:Data",
				"Object Name:="		, "Coil_Bn0",
				"Eddy Effect:="		, EDDY_LOSSES
			],
			[
				"NAME:Data",
				"Object Name:="		, "Coil_Ap0",
				"Eddy Effect:="		, EDDY_LOSSES
			],
			[
				"NAME:Data",
				"Object Name:="		, "Coil_An0",
				"Eddy Effect:="		, EDDY_LOSSES
			],
			[
				"NAME:Data",
				"Object Name:="		, "Coil_Cp0",
				"Eddy Effect:="		, EDDY_LOSSES
			],
			[
				"NAME:Data",
				"Object Name:="		, "Coil_Cn0",
				"Eddy Effect:="		, EDDY_LOSSES
			],
			[
				"NAME:Data",
				"Object Name:="		, "Stator_Teeth_1", #OK so according to F1 help, eddy effects should be disabled for laminated objects!! Solves my whole massive SOLIDLOSSES problem
				"Eddy Effect:="		, False
			],   
			[
				"NAME:Data",
				"Object Name:="		, "Stator_Teeth_2", #OK so according to F1 help, eddy effects should be disabled for laminated objects!! Solves my whole massive SOLIDLOSSES problem
				"Eddy Effect:="		, False
			],   
			[
				"NAME:Data",
				"Object Name:="		, "Stator_Teeth_3", #OK so according to F1 help, eddy effects should be disabled for laminated objects!! Solves my whole massive SOLIDLOSSES problem
				"Eddy Effect:="		, False
			],
			[
				"NAME:Data",
				"Object Name:="		, "Stator_Teeth_Air_1",
				"Eddy Effect:="		, EDDY_LOSSES
			],   
			[
				"NAME:Data",
				"Object Name:="		, "Stator_Teeth_Air_2",
				"Eddy Effect:="		, EDDY_LOSSES
			],   
			[
				"NAME:Data",
				"Object Name:="		, "Stator_Teeth_Air_3",
				"Eddy Effect:="		, EDDY_LOSSES
			],           
		]
	])
  oModule.SetCoreLoss([], True)
#  oModule.SetCoreLoss(["Outer_Magnet_0", "Outer_Magnet_1", "Outer_Magnet_2", "Outer_Magnet_3", "Inner_Yoke", "Outer_Yoke", "Coil_Bp0", "Coil_Bn0", "Coil_Ap0", "Coil_An0", "Coil_Cp0", "Coil_Cn0", "Stator_Teeth_1","Stator_Teeth_2","Stator_Teeth_3"], CORE_LOSSES)


# ########## #	
# Mesh Setup #
# ########## #

# Magnet lines to mesh on
M_outer_cplx_pnts = poly.arc(0.5*(magnet_radially_inner_radius+magnet_radially_outer_radius), -pi*sim_slice,pi*sim_slice,(2*hipg_seg+mag_seg)*4+1) #I think this line caused an error because the arc was situated on the inner arc of the moving band. This caused band to deform.
M_outer = pyMaxwell2D.Polyline(M_outer_cplx_pnts,True,False,"M_outer",GRAY)
M_outer.Create(oEditor)

#Stator teeth radial mesh lines
stator_teeth_mesh_line_cplx_pnts = []
stator_teeth_mesh_line_names = []
angle = 0
for i in range(0,3):
    stator_teeth_mesh_line_names.append('Stator_Teeth_Mesh_Line_%d' % (i+1))
    stator_teeth_mesh_line_cplx_pnts.append([stator_yoke_outer_radius*np.exp(1j*angle),stator_coil_radius*np.exp(1j*angle)])
#    stator_teeth_mesh_line_cplx_pnts.append(stator_coil_radius*np.exp(1j*angle))
    
    if i == 0:
        angle = angle + coil_pitch
    elif i == 1:
        angle = angle - 2*coil_pitch
        
    StatorTeethLineMesh=pyMaxwell2D.Polyline(stator_teeth_mesh_line_cplx_pnts[i],
          IsPolylineCovered=True,
          IsPolylineClosed=False,
          Name=stator_teeth_mesh_line_names[i],
          Color=GRAY)
        
    StatorTeethLineMesh.Create(oEditor)   

#Mesh lines within coil sides
coil_side_mesh_line_cplx_pnts = 0
coil_side_mesh_line_names = []

y_pnt = half_teeth_width+0.8*(y-half_teeth_width)

coil_side_mesh_line_cplx_pnts = [complex(stator_yoke_outer_radius,y_pnt),complex(x,y_pnt)]
                                 
for i in range(0,6):
    coil_side_mesh_line_names.append('Coil_Side_Mesh_Line_%d' % (i+1))

#    print coil_side_mesh_line_cplx_pnts
    
    CoilSideLineMesh=pyMaxwell2D.Polyline(coil_side_mesh_line_cplx_pnts,
          IsPolylineCovered=True,
          IsPolylineClosed=False,
          Name=coil_side_mesh_line_names[i],
          Color=GRAY)
        
    CoilSideLineMesh.Create(oEditor)

    if i==0:
        for j in range(0,len(coil_side_mesh_line_cplx_pnts)):
            coil_side_mesh_line_cplx_pnts[j]*=np.exp(-2j*(pi/Q))  

    if i==1:
        for j in range(0,len(coil_side_mesh_line_cplx_pnts)):
            coil_side_mesh_line_cplx_pnts[j]*=np.exp(+4j*(pi/Q)) 
      
    if i==2:
        coil_cplx_pnts = start
        for j in range(0,len(coil_side_mesh_line_cplx_pnts)):
           coil_side_mesh_line_cplx_pnts[j] = complex(coil_side_mesh_line_cplx_pnts[j].real,-1*coil_side_mesh_line_cplx_pnts[j].imag)         

    if i==3:
        for j in range(0,len(coil_side_mesh_line_cplx_pnts)):
            coil_side_mesh_line_cplx_pnts[j]*=np.exp(2j*(pi/Q))
       
    if i==4:
        for j in range(0,len(coil_side_mesh_line_cplx_pnts)):
            coil_side_mesh_line_cplx_pnts[j]*=np.exp(2j*(pi/Q)) 

#Mesh line within stator yoke    
stator_yoke_mesh_line_cplx_pnts = poly.arc(0.5*(stator_yoke_inner_radius+stator_yoke_outer_radius), -pi*sim_slice,pi*sim_slice,(2*hipg_seg+mag_seg)*4+1) #I think this line caused an error because the arc was situated on the inner arc of the moving band. This caused band to deform.
StatorYokeMeshLine = pyMaxwell2D.Polyline(stator_yoke_mesh_line_cplx_pnts,True,False,"StatorYokeMeshLine",GRAY)
StatorYokeMeshLine.Create(oEditor)

#Mesh line within rotor yoke
rotor_yoke_mesh_line_cplx_pnts = poly.arc(0.5*(magnet_radially_outer_radius+ryo), -pi*sim_slice,pi*sim_slice,(2*hipg_seg+mag_seg)*4+1) #I think this line caused an error because the arc was situated on the inner arc of the moving band. This caused band to deform.
RotorYokeMeshLine = pyMaxwell2D.Polyline(rotor_yoke_mesh_line_cplx_pnts,True,False,"RotorYokeMeshLine",GRAY)
RotorYokeMeshLine.Create(oEditor)

mesh_max_len = 0.2

oModule=oDesign.GetModule("MeshSetup")

oModule.AssignLengthOp(["NAME:MagnetLineMesh",
                        "RefineInside:=", False,
                        "Objects:=", ["M_outer"],
                        "RestrictElem:=", False,
                        "NumMaxElem:=", "5000",
                        "RestrictLength:=", True,
                        "MaxLength:=", "%gmm" % (mesh_max_len)])

oModule.AssignLengthOp(["NAME:StatorTeethLineMesh",
                        "RefineInside:=", False,
                        "Objects:=", stator_teeth_mesh_line_names,
                        "RestrictElem:=", False,
                        "NumMaxElem:=", "5000",
                        "RestrictLength:=", True,
                        "MaxLength:=", "%gmm" % (mesh_max_len)])

oModule.AssignLengthOp(["NAME:CoilSideLineMesh",
                        "RefineInside:=", False,
                        "Objects:=", coil_side_mesh_line_names,
                        "RestrictElem:=", False,
                        "NumMaxElem:=", "5000",
                        "RestrictLength:=", True,
                        "MaxLength:=", "%gmm" % (mesh_max_len)])

oModule.AssignLengthOp(["NAME:StatorYokeMeshLine",
                        "RefineInside:=", False,
                        "Objects:=", ["StatorYokeMeshLine"],
                        "RestrictElem:=", False,
                        "NumMaxElem:=", "5000",
                        "RestrictLength:=", True,
                        "MaxLength:=", "%gmm" % (mesh_max_len)])

oModule.AssignLengthOp(["NAME:RotorYokeMeshLine",
                        "RefineInside:=", False,
                        "Objects:=", ["RotorYokeMeshLine"],
                        "RestrictElem:=", False,
                        "NumMaxElem:=", "5000",
                        "RestrictLength:=", True,
                        "MaxLength:=", "%gmm" % (mesh_max_len)])

oModule.AssignLengthOp(["NAME:Br_rn",
                        "RefineInside:=", False,
                        "Objects:=", ["Br_rn"],
                        "RestrictElem:=", False,
                        "NumMaxElem:=", "5000",
                        "RestrictLength:=", True,
                        "MaxLength:=", "%gmm" % (0.05)])

oModule.AssignLengthOp(["NAME:Boundary",
                        "RefineInside:=", False,
                        "Objects:=", ["Boundary"], #mesh density of overall outside region of simulation
                        "RestrictElem:=", False,
                        "NumMaxElem:=", "30000",#was always "%d"%(int(3000/mesh_scale))
                        "RestrictLength:=", True,
                        "MaxLength:=", "%gmm" %(mesh_max_len)])#was ((hyi/2)/(4/mesh_scale))
                        
oModule.AssignLengthOp(["NAME:InnerYoke",
                        "RefineInside:=", False,
                        "Objects:=", [inner_yoke_name],
                        "RestrictElem:=", False,
                        "NumMaxElem:=", "5000",#was always "%d"%(int(3000/mesh_scale))
                        "RestrictLength:=", True,
                        "MaxLength:=", "%gmm" %(mesh_max_len)]) #was ((hyi/8)/(4/mesh_scale))])

oModule.AssignLengthOp(["NAME:OuterYoke",
                        "RefineInside:=", False,
                        "Objects:=", [outer_yoke_name],
                        "RestrictElem:=", False,
                        "NumMaxElem:=", "5000",#was always "%d"%(int(3000/mesh_scale))
                        "RestrictLength:=", True,
                        "MaxLength:=", "%gmm" %(mesh_max_len)]) #was ((hyo/8)/(4/mesh_scale))

if dont_draw_stator_teeth == 0:
    if coil_shape == RADIALLY_STRAIGHT:
        oModule.AssignLengthOp(["NAME:StatorTeeth",
                            "RefineInside:=", False,
                            "Objects:=", stator_teeth_names,
                            "RestrictElem:=", False,
                            "NumMaxElem:=", "5000",#was always "%d"%(int(6000/mesh_scale))
                            "RestrictLength:=", True,
                            "MaxLength:=", "%gmm" %(mesh_max_len)]) #was ((hyo/8)/(4/mesh_scale))

if dont_draw_stator_teeth_air == 0:
    if coil_shape == RADIALLY_STRAIGHT:
        oModule.AssignLengthOp(["NAME:StatorTeethAir",
                            "RefineInside:=", False,
                            "Objects:=", stator_teeth_air_names,
                            "RestrictElem:=", False,
                            "NumMaxElem:=", "5000",#was always "%d"%(int(6000/mesh_scale))
                            "RestrictLength:=", True,
                            "MaxLength:=", "%gmm" %(mesh_max_len)]) #was ((hyo/8)/(4/mesh_scale))

if PM:
  oModule.AssignLengthOp(["NAME:Magnets",
                          "RefineInside:=", False,
                          "Objects:=", magnet_names,
                          "RestrictElem:=", False,
                          "NumMaxElem:=", "5000", #was always "%d"%(int(3000/mesh_scale))
                          "RestrictLength:=", True,
                          "MaxLength:=", "%gmm" %(mesh_max_len)]) #was ((hmr/2)/(4/mesh_scale))

if WIND:
  oModule.AssignLengthOp(["NAME:Coils",
                          "RefineInside:=", False,
                          "Objects:=", coil_names,
                          "RestrictElem:=", False,
                          "NumMaxElem:=", "5000", #was always "%d"%(int(3000/mesh_scale))
                          "RestrictLength:=", True,
                          "MaxLength:=", "%gmm" %(mesh_max_len)]) #was ((hc/2)/(8/mesh_scale))

#oModule.AssignLengthOp(["NAME:Stator", 
#                        "RefineInside:=", False,
#                        "Objects:=", ["Stator"], #causes error cause I'm not sure if such an object currently exists.
#                        "RestrictElem:=", False,
#                        "NumMaxElem:=", "1000",
#                        "RestrictLength:=", True,
#                        "MaxLength:=", "%gmm" % (hc/10)])

  #oModule.AssignLengthOp(["NAME:Inner_Air_Gap", 
  #                        "RefineInside:=", False,
  #                        "Objects:=", ["Inner_Air_Gap"],
  #                        "RestrictElem:=", False,
  #                        "NumMaxElem:=", "1000",
  #                        "RestrictLength:=", True,
  #                        "MaxLength:=", "%gmm" % (l_g/2)])

# # oModule.AssignLengthOp(["NAME:Outer_Air_Gap", 
  #                        "RefineInside:=", False,
  #                        "Objects:=", ["Outer_Air_Gap"],
  #                        "RestrictElem:=", False,
  #                        "NumMaxElem:=", "1000",
  #                        "RestrictLength:=", True,
  #                        "MaxLength:=", "%gmm" % (l_g/2)])

#


# ############## #	
# Analysis Setup #
# ############## #

oModule=oDesign.GetModule("AnalysisSetup")

oModule.InsertSetup("Transient",
                    ["NAME:Setup1",
                     "NonlinearSolverResidual:=",  1e-7,#was always 1e-6
                     "TimeIntegrationMethod:=", 0,
                     "StopTime:=", "%g" % (stop_time),
                     "TimeStep:=",  "%g" % (stop_time/sim_steps),
                     "OutputError:=", False,
                     "UseControlProgram:=", False,
                     "ControlProgramName:=", "",
                     "ControlProgramArg:=", " ",
                     "CallCtrlProgAfterLastStep:=", False,
                     "HasSweepSetup:=",  True,
                     "SweepSetupType:=", "LinearStep",
                     "StartValue:=", "%gs"%start_time_value,
                     "StopValue:=", "%g" % (stop_time),
                     "StepSize:=", "%g" % (step_size),
                     "OutputVarCalTimeStep:=", "%g" % (stop_time/out_steps),
                     "OutputVarCalNumOfSolveSteps:=", 15,#was always 10
                     "OutputVarCalTimeMethod:=", 0,
                     "NumberOfOutputVars:=", 0,
                     "TransientHcNonLinearBH:=", True,
                     "TransientComputeHc:=", False,
                     "PreAdaptMesh:=", False,
                     "UseAdaptiveTimeStep:=", False,
                     "InitialTimeStep:=", "0.0002s",
                     "MinTimeStep:=", "%gs"%min_time_step,
                     "MaxTimeStep:=", "%gs"%max_time_step,
                     "TimeStepErrTolerance:=", "%g"%(time_step_err_tolerance)])
oDesktop=oAnsoftApp.GetAppDesktop()
oDesktop.RestoreWindow()

# ################################## #
# Save project with unique file name #
# ################################## #

pathname="D:/Documents/Meesters/Ansys_Maxwell_Simulations/2d/results/"
filename="SORSPM"
#filename+=Type
filename+="_l=%.2f" % l
#filename+="_r=%.2f" % r_n
#filename+="_Ip=%g" % I_p
filename+="_Jrms=%g" % J
filename+="_kmr=%.2f" % kmr
#filename+="_N=%g" % N
filename+="_hmr=%.1f" % hmr
filename+="_p=%g" % poles
filename+="_hc=%.1f" % hc

if not WIND:
    filename+="_noWIND"
#  filename+="_CLIP"
  
oProject.SaveAs("%s%s.mxwl" % (pathname,filename), True)
#oProject.SaveAs("C:\Users\Gert\Documents\akademies\finale jaar\SKRIPSIE\simData")

# #################################################################
# Close the project and reopen it in order to see the field plots #
# #################################################################
oProject.Close()
oDesktop = oAnsoftApp.GetAppDesktop()
#oDesktop.RestoreWindow
oDesktop.OpenProject("%s%s.mxwl" % (pathname,filename))
oProject = oDesktop.SetActiveProject(filename)
oDesign = oProject.SetActiveDesign(design_name)
# #################################################################

if run_band_solver == 1:
    oDesign.AnalyzeAll()

oModule = oDesign.GetModule("FieldsReporter")

# ################## #
# Mesh Display Setup #
# ################## #

oModule.CreateFieldPlot(["NAME:Mesh1",
                         "SolutionName:=", "Setup1 : Transient",
                         "QuantityName:=", "Mesh",
                         "PlotFolder:=", "MeshPlots",
                         "FieldType:=", "Fields",
                         "UserSpecifyName:=", 0,
                         "UserSpecifyFolder:=", 0,
                         "IntrinsicVar:=", "Time='-1s'",
                         "PlotGeomInfo:=", [1, "Surface", "CutPlane", 1, "Global:XY"],
                         "FilterBoxes:=", [0],
                         "Real time mode:=", True,
                         ["NAME:MeshSettings",
                          "Scale factor:=", 100,
                          "Transparency:=", 0,
                          "Mesh type:=",  "Shaded",
                          "Surface only:=", True,
                          "Add grid:=", True,
                          "Mesh line color:=", [0, 0, 255],
                          "Filled color:=", [255, 255, 255]]])

oModule.SetPlotsViewSolutionContext(["Mesh1"], "Setup1 : Transient","Time='0s'")

# ####################### #
# Flux Line Display Setup #
# ####################### #

oModule.CreateFieldPlot(["NAME:Flux_Lines1",
                         "SolutionName:=", "Setup1 : Transient",
                         "QuantityName:=", "Flux_Lines",
                         "PlotFolder:=", "A",
                         "UserSpecifyName:=", 0,
                         "UserSpecifyFolder:=", 0,
                         "IntrinsicVar:=", "Time='0.00325s''",
                         "PlotGeomInfo:=", [1, "Surface", "CutPlane", 1, "Global:XY"],
                         "FilterBoxes:=", [0],
                         ["NAME:PlotOnSurfaceSettings",
                          "Filled:=", False,
                          "IsoValType:=", "Line",
                          "SmoothShade:=", True,
                          "AddGrid:=", False,
                          "MapTransparency:=", True,
                          "Refinement:=", 0,
                          "Transparency:=", 0,
                          ["NAME:Arrow3DSpacingSettings",
                           "ArrowUniform:=", True,
                           "ArrowSpacing:=", 0,
                           "MinArrowSpacing:=", 0,
                           "MaxArrowSpacing:=", 0],
                          "GridColor:=", [255, 255, 255]]])

oModule.SetPlotsViewSolutionContext(["Flux_Lines1"], "Setup1 : Transient","Time='0s'")

# ############################# #
# Flux Density Display Settings #
# ############################# #

oModule.CreateFieldPlot(["NAME:Mag_B1",
                         "SolutionName:=", "Setup1 : Transient",
                         "QuantityName:=", "Mag_B",
                         "PlotFolder:=", "B",
                         "UserSpecifyName:=", 0,
                         "UserSpecifyFolder:=", 0,
                         "IntrinsicVar:=", "Time='0s'",
                         "PlotGeomInfo:=", [1, "Surface", "CutPlane", 1, "Global:XY"],
                         "FilterBoxes:=", [0],
                         ["NAME:PlotOnSurfaceSettings",
                          "Filled:=", False,
                          "IsoValType:=", "Fringe",
                          "SmoothShade:=", True,
                          "AddGrid:=", False,
                          "MapTransparency:=", True,
                          "Refinement:=", 0,
                          "Transparency:=", 0,
                          ["NAME:Arrow3DSpacingSettings",
                           "ArrowUniform:=", True,
                           "ArrowSpacing:=", 0,
                           "MinArrowSpacing:=", 0,
                           "MaxArrowSpacing:=", 0],
                          "GridColor:=", [255, 255, 255]]])

oModule.SetPlotsViewSolutionContext(["Mag_B1"], "Setup1 : Transient","Time='0s'")

# ############################## #
# Calculate area of coil surface #
# ############################## #
if run_band_solver == 1:
    if WIND:
        oModule.EnterSurf("Coil_An0")
        oModule.EnterScalar(1)
        oModule.CalcStack("exch")
        oModule.CalcOp("Integrate")
        oModule.ClcEval("Setup1 : Transient", 
                        [
                         "Time:="		, "0s"
                         ])
        oModule.CalcStack("pop")
        oModule.EnterScalar(1000000)
        oModule.CalcOp("*")
        oModule.AddNamedExpression("coil_surface_area_mm2", "Fields")
        oModule.CopyNamedExprToStack("coil_surface_area_mm2")
        oModule.ClcEval("Setup1 : Transient", 
                        [
                         "Time:="		, "0s"
                         ])

# ############################### #
# Radial Flux Density Caluclation #
# ############################### #

    oModule.CalcStack("clear")
    oModule.EnterQty("B")
    oModule.CalcOp("ScalarX")
    oModule.EnterScalarFunc("PHI")
    oModule.CalcOp("Cos")
    oModule.CalcOp("*")
    oModule.EnterQty("B")
    oModule.CalcOp("ScalarY")
    oModule.EnterScalarFunc("PHI")
    oModule.CalcOp("Sin")
    oModule.CalcOp("*")
    oModule.CalcOp("+")
    oModule.AddNamedExpression("B_radial", "Fields")

    oModule.CopyNamedExprToStack("B_radial")
    oModule.EnterLine("Br_rn")
    oModule.CalcOp("Integrate")
    oModule.EnterScalar(1)
    oModule.EnterLine("Br_rn")
    oModule.CalcOp("Integrate")
    oModule.CalcOp("/")
    oModule.AddNamedExpression("Br_rn", "Fields")

#oModule.CopyNamedExprToStack("B_radial")
#oModule.EnterLine("Br_cim")
#oModule.CalcOp("Integrate")
#oModule.EnterScalar(1)
#oModule.EnterLine("Br_cim")
#oModule.CalcOp("Integrate")
#oModule.CalcOp("/")
#oModule.AddNamedExpression("Br_cim", "Fields")

#oModule.CopyNamedExprToStack("B_radial")
#oModule.EnterLine("Br_com")
#oModule.CalcOp("Integrate")
#oModule.EnterScalar(1)
#oModule.EnterLine("Br_com")
#oModule.CalcOp("Integrate")
#oModule.CalcOp("/")
#oModule.AddNamedExpression("Br_com", "Fields")

#if not WIND:
#  oModule.CopyNamedExprToStack("B_radial")
#  oModule.EnterLine("Br_r_ciy")
#  oModule.CalcOp("Integrate")
#  oModule.EnterScalar(1)
#  oModule.EnterLine("Br_r_ciy")
#  oModule.CalcOp("Integrate")
#  oModule.CalcOp("/")
#  oModule.AddNamedExpression("Br_r_ciy", "Fields")
#
#  oModule.CopyNamedExprToStack("B_radial")
#  oModule.EnterLine("Br_r_coy")
#  oModule.CalcOp("Integrate")
#  oModule.EnterScalar(1)
#  oModule.EnterLine("Br_r_coy")
#  oModule.CalcOp("Integrate")
#  oModule.CalcOp("/")
#  oModule.AddNamedExpression("Br_r_coy", "Fields")
#
#  oModule.CopyNamedExprToStack("B_radial")
#  oModule.EnterLine("Br_rnph2")
#  oModule.CalcOp("Integrate")
#  oModule.EnterScalar(1)
#  oModule.EnterLine("Br_rnph2")
#  oModule.CalcOp("Integrate")
#  oModule.CalcOp("/")
#  oModule.AddNamedExpression("Br_rnph2", "Fields")
#
#  oModule.CopyNamedExprToStack("B_radial")
#  oModule.EnterLine("Br_rnmh2")
#  oModule.CalcOp("Integrate")
#  oModule.EnterScalar(1)
#  oModule.EnterLine("Br_rnmh2")
#  oModule.CalcOp("Integrate")
#  oModule.CalcOp("/")
#  oModule.AddNamedExpression("Br_rnmh2", "Fields")

# ################################## #
# Azimuthal Flux Density Caluclation #
# ################################## #

    oModule.CalcStack("clear")
    oModule.EnterQty("B")
    oModule.CalcOp("ScalarX")
    oModule.EnterScalarFunc("PHI")
    oModule.CalcOp("Sin")
    oModule.CalcOp("*")
    oModule.EnterQty("B")
    oModule.CalcOp("ScalarY")
    oModule.EnterScalarFunc("PHI")
    oModule.CalcOp("Cos")
    oModule.CalcOp("*")
    oModule.CalcOp("-")
    oModule.AddNamedExpression("B_azimuthal", "Fields")

    oModule.CopyNamedExprToStack("B_azimuthal")
    oModule.EnterLine("Br_rn")
    oModule.CalcOp("Integrate")
    oModule.EnterScalar(1)
    oModule.EnterLine("Br_rn")
    oModule.CalcOp("Integrate")
    oModule.CalcOp("/")
    oModule.AddNamedExpression("Bt_ag", "Fields")

#oModule.CopyNamedExprToStack("B_azimuthal")
#oModule.EnterLine("Br_cim")
#oModule.CalcOp("Integrate")
#oModule.EnterScalar(1)
#oModule.EnterLine("Br_cim")
#oModule.CalcOp("Integrate")
#oModule.CalcOp("/")
#oModule.AddNamedExpression("Bt_mci", "Fields")

#oModule.CopyNamedExprToStack("B_azimuthal")
#oModule.EnterLine("Br_com")
#oModule.CalcOp("Integrate")
#oModule.EnterScalar(1)
#oModule.EnterLine("Br_com")
#oModule.CalcOp("Integrate")
#oModule.CalcOp("/")
#oModule.AddNamedExpression("Bt_mco", "Fields")

#if not WIND:
#  oModule.CopyNamedExprToStack("B_azimuthal")
#  oModule.EnterLine("Br_r_ciy")
#  oModule.CalcOp("Integrate")
#  oModule.EnterScalar(1)
#  oModule.EnterLine("Br_r_ciy")
#  oModule.CalcOp("Integrate")
#  oModule.CalcOp("/")
#  oModule.AddNamedExpression("Bt_r_ciy", "Fields")
#
#  oModule.CopyNamedExprToStack("B_azimuthal")
#  oModule.EnterLine("Br_r_coy")
#  oModule.CalcOp("Integrate")
#  oModule.EnterScalar(1)
#  oModule.EnterLine("Br_r_coy")
#  oModule.CalcOp("Integrate")
#  oModule.CalcOp("/")
#  oModule.AddNamedExpression("Bt_r_coy", "Fields")




if PM and AR and run_band_solver == 1:
  pathname+=filename
  pathname+=".mxwlresults/"    
  # ####################### #
  # Torque Rectangular Plot #
  # ####################### #    
    
  oModule = oDesign.GetModule("OutputVariable")

#  oModule.CreateOutputVariable("Output_Torque",
#                               "Moving1.Torque+Moving2.Torque",
#                               "Setup1 : Transient", "Transient",
#                               [])

  oModule = oDesign.GetModule("ReportSetup")

  torque_plot_name="Torque Rectangular Plot"
  oModule.CreateReport("%s" % torque_plot_name,
                       "Transient",
                       "Rectangular Plot",
                       "Setup1 : Transient",
                       [],
                       ["Time:=", ["All"]],
                       ["X Component:=", "Time",
                        "Y Component:=", ["Moving1.Torque"]],
                       [])

  oModule.ChangeProperty(["NAME:AllTabs",
                          ["NAME:Scaling",
                           ["NAME:PropServers", "%s:AxisY1" % torque_plot_name],
                           ["NAME:ChangedProps",
                            ["NAME:Specify Min", "Value:=", True],
                            ["NAME:Min", "MustBeInt:=", False, "Value:=", "-10"]]]])

  oModule.AddTraceCharacteristics("Torque Rectangular Plot", "pk2pk", [], ["Full"])
  oModule.AddTraceCharacteristics("Torque Rectangular Plot", "avg", [], ["Full"])
  oModule.AddTraceCharacteristics("Torque Rectangular Plot", "ripple", [], ["Full"])  
  oModule.AddTraceCharacteristics("Torque Rectangular Plot", "max", [], ["Full"]) 
  oModule.AddTraceCharacteristics("Torque Rectangular Plot", "min", [], ["Full"]) 
  
  
  
  oModule.ChangeProperty(
	[
		"NAME:AllTabs",
		[
			"NAME:Scaling",
			[
				"NAME:PropServers", 
				"Torque Rectangular Plot:AxisY1"
			],
			[
				"NAME:ChangedProps",
				[
					"NAME:Min",
					"MustBeInt:="		, False,
					"Value:="		, "0NewtonMeter"
				],
				[
					"NAME:Specify Min",
					"Value:="		, True
				]
			]
		]
	])


  # ####################### #
  # Losses Rectangular Plot #
  # ####################### #

  losses_data_plot_name="Losses Rectangular Plot"  
  
  oModule.CreateReport(losses_data_plot_name, "Transient", "Rectangular Plot", "Setup1 : Transient", 
	[
		"Domain:="		, "Sweep"
	], 
	[
		"Time:="		, ["All"]
	], 
	[
		"X Component:="		, "Time",
		"Y Component:="		, ["CoreLoss"]
	], [])
  oModule.AddTraces(losses_data_plot_name, "Setup1 : Transient", 
	[
		"Domain:="		, "Sweep"
	], 
	[
		"Time:="		, ["All"]
	], 
	[
		"X Component:="		, "Time",
		"Y Component:="		, ["EddyCurrentLoss"]
	], [])
  oModule.AddTraces(losses_data_plot_name, "Setup1 : Transient", 
	[
		"Domain:="		, "Sweep"
	], 
	[
		"Time:="		, ["All"]
	], 
	[
		"X Component:="		, "Time",
		"Y Component:="		, ["ExcessLoss"]
	], [])
  oModule.AddTraces(losses_data_plot_name, "Setup1 : Transient", 
	[
		"Domain:="		, "Sweep"
	], 
	[
		"Time:="		, ["All"]
	], 
	[
		"X Component:="		, "Time",
		"Y Component:="		, ["HysteresisLoss"]
	], [])
  oModule.AddTraces(losses_data_plot_name, "Setup1 : Transient", 
	[
		"Domain:="		, "Sweep"
	], 
	[
		"Time:="		, ["All"]
	], 
	[
		"X Component:="		, "Time",
		"Y Component:="		, ["SolidLoss"]
	], [])
  oModule.AddTraces(losses_data_plot_name, "Setup1 : Transient", 
	[
		"Domain:="		, "Sweep"
	], 
	[
		"Time:="		, ["All"]
	], 
	[
		"X Component:="		, "Time",
		"Y Component:="		, ["StrandedLoss"]
	], [])
  oModule.AddTraces(losses_data_plot_name, "Setup1 : Transient", 
	[
		"Domain:="		, "Sweep"
	], 
	[
		"Time:="		, ["All"]
	], 
	[
		"X Component:="		, "Time",
		"Y Component:="		, ["StrandedLossR"]
	], [])









  
#  oModule = oDesign.GetModule("ReportSetup")

if WIND and run_band_solver:
  # ######################## #
  # Winding Rectangular Plot #
  # ######################## #

  winding_data_plot_name="Winding Rectangular Plot"

  oModule.CreateReport("%s" % winding_data_plot_name,
                       "Transient",
                       "Rectangular Plot",
                       "Setup1 : Transient",
                       ["Domain:=", "Sweep"],
                       ["Time:=", ["All"]],
                       ["X Component:=", "Time",
                        "Y Component:=", ["FluxLinkage(Winding_A)",
                                          "FluxLinkage(Winding_B)",
                                          "FluxLinkage(Winding_C)",
                                          "InducedVoltage(Winding_A)",
                                          "InducedVoltage(Winding_B)",
                                          "InducedVoltage(Winding_C)",
                                          "InputCurrent(Winding_A)",
                                          "InputCurrent(Winding_B)",
                                          "InputCurrent(Winding_C)"]],
                       [])

  oModule.AddTraceCharacteristics("Winding Rectangular Plot", "pk2pk", [], ["Full"])
  oModule.AddTraceCharacteristics("Winding Rectangular Plot", "max", [], ["Full"])
  oModule.AddTraceCharacteristics("Winding Rectangular Plot", "distortion", ["0"], ["Full"])
  oModule.ChangeProperty(
  	[
		"NAME:AllTabs",
  		[
			"NAME:Trace Characteristics",
			[
				"NAME:PropServers", 
				"Winding Rectangular Plot:distortion"
			],
			[
				"NAME:ChangedProps",
				[
					"NAME:Frequency",
					"Value:="		, "135"#change to local variable
				]
			]
		]
	])

  oModule.ExportToFile("%s" % winding_data_plot_name,"%s%s-lambda.txt" % (pathname,filename)) 
  time_stamps,L=np.loadtxt('%s%s-lambda.txt' % (pathname,filename) ,skiprows=7,usecols=(0,1),unpack=True)

  
if LMC and WIND and run_band_solver:
  # ################################## #
  # Inductance Matrix Rectangular Plot #
  # ################################## #

  inductance_matrix_data_plot_name="Inductance Matrix Rectangular Plot"

  oModule.CreateReport("%s" % inductance_matrix_data_plot_name,
                       "Transient",
                       "Rectangular Plot",
                       "Setup1 : Transient",
                       ["Domain:=", "Sweep"],
                       ["Time:=", ["All"]],
                       ["X Component:=", "Time",
                        "Y Component:=", ["L(Winding_A,Winding_A)",
                                          "L(Winding_A,Winding_B)",
                                          "L(Winding_A,Winding_C)",
                                          "L(Winding_B,Winding_A)",
                                          "L(Winding_B,Winding_B)",
                                          "L(Winding_B,Winding_C)",
                                          "L(Winding_C,Winding_A)",
                                          "L(Winding_C,Winding_B)",
                                          "L(Winding_C,Winding_C)"]],
                       [])
  
# ##################################### #
# Radial Flux Density Rectangular  Plot #
# ##################################### #

if run_band_solver == 1:
  radial_flux_density_plot_name="Radial Flux Density Rectangular Plot"

  oModule.CreateReport("%s" % radial_flux_density_plot_name,
                     "Fields",
                     "Rectangular Plot",
                     "Setup1 : Transient",
                     ["Context:=", "Br_rn", "PointCount:=", 721, "Domain:=", "Sweep"],
                     ["Distance:=", ["All"],
                      "Time:=", ["0.00s"]],
                     ["X Component:=", "normalize(Distance)",
                      "Y Component:=", ["B_radial"]],
                     [])

  oModule.AddTraceCharacteristics("Radial Flux Density Rectangular Plot", "pk2pk", [], ["Full"])
  oModule.AddTraceCharacteristics("Radial Flux Density Rectangular Plot", "max", [], ["Full"])
  oModule.AddTraceCharacteristics("Radial Flux Density Rectangular Plot", "min", [], ["Full"])
  oModule.AddTraceCharacteristics("Radial Flux Density Rectangular Plot", "distortion", ["0"], ["Full"])
  oModule.ChangeProperty(
	[
		"NAME:AllTabs",
		[
			"NAME:Trace Characteristics",
			[
				"NAME:PropServers", 
				"Radial Flux Density Rectangular Plot:distortion"
			],
			[
				"NAME:ChangedProps",
				[
					"NAME:Frequency",
					"Value:="		, "135"
				]
			]
		]
	])

#print "time_stamps to be used"
  for i in range(0,len(analyse_specific_step)):
#    print time_stamps[analyse_specific_step[i]]/1000
    
    oModule.AddTraces("Radial Flux Density Rectangular Plot", "Setup1 : Transient", 
	[
		"Context:="		, "Br_rn",
		"PointCount:="		, 1001,
		"Domain:="		, "Sweep"
	], 
	[
		"Distance:="		, ["All"],
		[
			"NAME:VariableValues",
			"Time:="		, "%.9fs"%(time_stamps[analyse_specific_step[i]]/1000)
		]
	], 
	[
		"X Component:="		, "normalize(Distance)",
		"Y Component:="		, ["B_radial"]
	], [])

#oModule.AddTraces("%s" % radial_flux_density_plot_name,
#                  "Setup1 : Transient",
#                  ["Context:=", "Br_cim", "PointCount:=", 721, "Domain:=", "Sweep"],
#                  ["Distance:=", ["All"],
#                   "Time:=", ["0.00s"]],
#                  ["X Component:=", "normalize(Distance)",
#                   "Y Component:=", ["Br_cim"]],
#                  [])

#oModule.AddTraces("%s" % radial_flux_density_plot_name,
#                  "Setup1 : Transient",
#                  ["Context:=", "Br_com", "PointCount:=", 721, "Domain:=", "Sweep"],
#                  ["Distance:=", ["All"],
#                   "Time:=", ["0.00s"]],
#                  ["X Component:=", "normalize(Distance)",
#                   "Y Component:=", ["Br_com"]],
#                  [])

#if not WIND:
#  oModule.AddTraces("%s" % radial_flux_density_plot_name,
#                    "Setup1 : Transient",
#                    ["Context:=", "Br_r_ciy", "PointCount:=", 721, "Domain:=", "Sweep"],
#                    ["Distance:=", ["All"],
#                     "Time:=", ["0.00s"]],
#                    ["X Component:=", "normalize(Distance)",
#                     "Y Component:=", ["Br_r_ciy"]],
#                    [])
#
#  oModule.AddTraces("%s" % radial_flux_density_plot_name,
#                    "Setup1 : Transient",
#                    ["Context:=", "Br_r_coy", "PointCount:=", 721, "Domain:=", "Sweep"],
#                    ["Distance:=", ["All"],
#                     "Time:=", ["0.00s"]],
#                    ["X Component:=", "normalize(Distance)",
#                     "Y Component:=", ["Br_r_coy"]],
#                    [])
#
#  oModule.AddTraces("%s" % radial_flux_density_plot_name,
#                    "Setup1 : Transient",
#                    ["Context:=", "Br_rnmh2", "PointCount:=", 721, "Domain:=", "Sweep"],
#                    ["Distance:=", ["All"],
#                     "Time:=", ["0.00s"]],
#                    ["X Component:=", "normalize(Distance)",
#                     "Y Component:=", ["Br_rnmh2"]],
#                    [])
#
#  oModule.AddTraces("%s" % radial_flux_density_plot_name,
#                    "Setup1 : Transient",
#                    ["Context:=", "Br_rnph2", "PointCount:=", 721, "Domain:=", "Sweep"],
#                    ["Distance:=", ["All"],
#                     "Time:=", ["0.00s"]],
#                    ["X Component:=", "normalize(Distance)",
#                     "Y Component:=", ["Br_rnph2"]],
#                    [])

# ######################################## #
# Azimuthal Flux Density Rectangular  Plot #
# ######################################## #
if run_band_solver == 1:
  azimuthal_flux_density_plot_name="Azimuthal Flux Density Rectangular Plot"

  oModule.CreateReport("%s" % azimuthal_flux_density_plot_name,
                     "Fields",
                     "Rectangular Plot",
                     "Setup1 : Transient",
                     ["Context:=", "Br_rn", "PointCount:=", 721, "Domain:=", "Sweep"],
                     ["Distance:=", ["All"],
                      "Time:=", ["0.00s"]],
                     ["X Component:=", "normalize(Distance)",
                      "Y Component:=", ["B_azimuthal"]],
                     [])

  oModule.AddTraceCharacteristics("Azimuthal Flux Density Rectangular Plot", "pk2pk", [], ["Full"])
  oModule.AddTraceCharacteristics("Azimuthal Flux Density Rectangular Plot", "max", [], ["Full"])
  oModule.AddTraceCharacteristics("Azimuthal Flux Density Rectangular Plot", "min", [], ["Full"])

#oModule.AddTraces("%s" % azimuthal_flux_density_plot_name,
#                  "Setup1 : Transient",
#                  ["Context:=", "Br_cim", "PointCount:=", 721, "Domain:=", "Sweep"],
#                  ["Distance:=", ["All"],
#                   "Time:=", ["0.00s"]],
#                  ["X Component:=", "normalize(Distance)",
#                   "Y Component:=", ["Ba_cim"]],
#                  [])

#oModule.AddTraces("%s" % azimuthal_flux_density_plot_name,
#                  "Setup1 : Transient",
#                  ["Context:=", "Br_com", "PointCount:=", 721, "Domain:=", "Sweep"],
#                  ["Distance:=", ["All"],
#                   "Time:=", ["0.00s"]],
#                  ["X Component:=", "normalize(Distance)",
#                   "Y Component:=", ["Ba_com"]],
#                  [])

#if not WIND:
#  oModule.AddTraces("%s" % azimuthal_flux_density_plot_name,
#                    "Setup1 : Transient",
#                    ["Context:=", "Br_r_ciy", "PointCount:=", 721, "Domain:=", "Sweep"],
#                    ["Distance:=", ["All"],
#                     "Time:=", ["0.00s"]],
#                    ["X Component:=", "normalize(Distance)",
#                     "Y Component:=", ["Ba_r_ciy"]],
#                    [])
#
#  oModule.AddTraces("%s" % azimuthal_flux_density_plot_name,
#                    "Setup1 : Transient",
#                    ["Context:=", "Br_r_coy", "PointCount:=", 721, "Domain:=", "Sweep"],
#                    ["Distance:=", ["All"],
#                     "Time:=", ["0.00s"]],
#                    ["X Component:=", "normalize(Distance)",
#                     "Y Component:=", ["Ba_r_coy"]],
#                    [])

if run_band_solver == 1:
  oProject.SaveAs("%s%s.mxwl" % (pathname,filename), True)

# ####################### #
# Quick Report CSV Export #
# ####################### #
if run_band_solver == 1:    
  
    oModule.ExportToFile(losses_data_plot_name, "%s%s-Plosses.txt" % (pathname,filename))
    
#    if WIND:
#      oModule.ExportToFile("%s" % winding_data_plot_name,
#                           "%s%s-lambda.txt" % (pathname,filename))
                           
    if LMC:                       
      oModule.ExportToFile("%s" % inductance_matrix_data_plot_name,
                           "%s%s-LM.txt" % (pathname,filename))
    
    if AR and PM:
      oModule.ExportToFile("%s" % torque_plot_name,
                           "%s%s-Tm.txt" % (pathname,filename))
    
    oModule.ExportToFile("%s" % radial_flux_density_plot_name,
                         "%s%s-Br.txt" % (pathname,filename))
    
    oModule.ExportToFile("%s" % azimuthal_flux_density_plot_name,
                         "%s%s-Bt.txt" % (pathname,filename))

# ########################## #
# Pretty Table Quick Outputs #
# ########################## #
if enable_terminal_printout == 1:
  pretty_terminal_table.add_row(["-----TOPOLOGY --------------------", "--------", "------"]) 
  if coil_shape == SQUARE_SLOTS:
    pretty_terminal_table.add_row(["coil_shape","SQUARE","shape"])  
  elif coil_shape == RADIALLY_STRAIGHT:
    pretty_terminal_table.add_row(["coil_shape","RADIAL","shape"])
  
  if square_magnets == 0:
    pretty_terminal_table.add_row(["magnet_shape","CURVED","shape"])  
  elif square_magnets == 1:
    pretty_terminal_table.add_row(["magnet_shape","SQUARE","shape"])  

  if magnetisation_radial0_normal1 == 0:
    pretty_terminal_table.add_row(["magnetisation","RADIAL","direction"])  
  elif magnetisation_radial0_normal1 == 1:
    pretty_terminal_table.add_row(["magnetisation","NORMAL","direction"])  
  
  pretty_terminal_table.add_row(["", "", ""])
  pretty_terminal_table.add_row(["-----MACHINE SIMULATION SETUP-----", "--------", "------"])  
  pretty_terminal_table.add_row(["symmetry_multiplier",periodicity_in_one_full_rotation,"[p.u.]"])  
  pretty_terminal_table.add_row(["q",int(Q/3),"coils (slots) per phase"])  
  pretty_terminal_table.add_row(["Q",Q,"total coils (slots)"])
  pretty_terminal_table.add_row(["poles",poles,"total poles"])
#  pretty_terminal_table.add_row(["slots_per_phase_per_pole",slots_per_phase_per_pole,"ratio"])
#  pretty_terminal_table.add_row(["pole_pairs_per_slot_per_phase",pole_pairs_per_slot_per_phase,"ratio"])
  pretty_terminal_table.add_row(["kc",kc,""])
  pretty_terminal_table.add_row(["ks",ks,""])
  #pretty_terminal_table.add_row(["kq",kq,"coils pole ratio"])
#  pretty_terminal_table.add_row(["M",magnet_ns_total,"total magnet N-S poles"])
#  pretty_terminal_table.add_row(["M/2",magnet_ns_total/2,"total magnet N-s pole pairs"])
  pretty_terminal_table.add_row(["f_e",f_e,"[Hz]"])  
  pretty_terminal_table.add_row(["steps_simulated",steps,"[Hz]"])
  pretty_terminal_table.add_row(["machine_speed",n_rpm,"[rpm]"]) 
  
  pretty_terminal_table.add_row(["J_rms",J,"[A/mm^2]"])
  pretty_terminal_table.add_row(["id_peak",I_p*np.sin(initial_electrical_angle),"[A]"])
  pretty_terminal_table.add_row(["iq_peak",I_p*np.cos(initial_electrical_angle),"[A]"])
  pretty_terminal_table.add_row(["Br_PM",Brem,"[T]"])
  pretty_terminal_table.add_row(["", "", ""])
  pretty_terminal_table.add_row(["-----DIMENSIONS ------------------", "--------", "------"])
  pretty_terminal_table.add_row(["active_stack_length", l, "[mm]"])
  pretty_terminal_table.add_row(["stator_yoke_inner_radius", stator_yoke_inner_radius, "[mm]"])
  pretty_terminal_table.add_row(["stator_yoke_height", (stator_yoke_outer_radius-stator_yoke_inner_radius), "[mm]"])
  pretty_terminal_table.add_row(["airgap", g, "[mm]"])  
  pretty_terminal_table.add_row(["rotor_yoke_height", (ryo-magnet_radially_outer_radius), "[mm]"])
  pretty_terminal_table.add_row(["rotor_yoke_outer_radius", ryo, "[mm]"])

  if coil_shape == SQUARE_SLOTS:
    pretty_terminal_table.add_row(["stator_teeth_width", teeth_width, "[mm]"])
  pretty_terminal_table.add_row(["stator_teeth_height", (stator_coil_radius-stator_yoke_outer_radius), "[mm]"])
  
  if coil_shape == SQUARE_SLOTS:
    pretty_terminal_table.add_row(["coil_side_width", coil_side_width, "[mm]"])
    pretty_terminal_table.add_row(["coil_side_height", coil_side_height, "[mm]"])
  else:
    pretty_terminal_table.add_row(["coil_side_height", (stator_coil_radius-stator_yoke_outer_radius-coil_yoke_spacing), "[mm]"])  

  pretty_terminal_table.add_row(["coil_teeth_spacing", coil_teeth_spacing, "[mm]"])
  pretty_terminal_table.add_row(["coil_yoke_spacing", coil_yoke_spacing, "[mm]"])
  pretty_terminal_table.add_row(["opposing_coil_to_coil_spacing", coil_opposing_spacing, "[mm]"])       
    
  pretty_terminal_table.add_row(["coil_side_area",coil_side_area_in_mm,"[mm^2]"])
  
  if square_magnets == 1:
    pretty_terminal_table.add_row(["magnet_width", magnet_side_length, "[mm]"])
  pretty_terminal_table.add_row(["magnet_height", (magnet_radially_outer_radius-magnet_radially_inner_radius), "[mm]"])    
  
  
#  pretty_terminal_table.add_row(["stator_yoke_area",stator_yoke_area,"[mm2]"])
#  pretty_terminal_table.add_row(["rotor_yoke_area",rotor_yoke_area,"[mm2]"])
#  pretty_terminal_table.add_row(["copper_area",copper_area,"[mm2]"])
#  pretty_terminal_table.add_row(["magnet_area",magnet_area,"[mm2]"]) 
  
  pretty_terminal_table.add_row(["stator_yoke_mass",stator_yoke_mass,"[kg]"])
  pretty_terminal_table.add_row(["rotor_yoke_mass",rotor_yoke_mass,"[kg]"])
  pretty_terminal_table.add_row(["copper_mass",copper_mass,"[kg]"])
  pretty_terminal_table.add_row(["magnet_mass",magnet_mass,"[kg]"])
  pretty_terminal_table.add_row(["total_mass",total_mass,"[kg]"])
  pretty_terminal_table.add_row(["", "", ""])

  pretty_terminal_table.add_row(["number_of_parallel_circuits", a, "[p.u.]"])
  pretty_terminal_table.add_row(["number_of_turns_per_coil", turnsPerCoil, "[p.u.]"])
  pretty_terminal_table.add_row(["fill_factor_assumption", fill_factor, "[p.u.]"])
  pretty_terminal_table.add_row(["system_voltage", system_voltage, "[V]"])
  if inverter_squarewave0_sinusoidalPWM1 == 0: pretty_terminal_table.add_row(["inverter_topology", "SQUARE WAVE (factor 1.103)", ""])
  elif inverter_squarewave0_sinusoidalPWM1 == 1: pretty_terminal_table.add_row(["inverter_topology", "SINUSOIDAL PWM (factor 0.866)", ""])
  pretty_terminal_table.add_row(["inverter_voltage_drop_factor", 0.95, "[p.u.]"])  
  pretty_terminal_table.add_row(["peak_terminal_line_voltage_ALLOWED", peak_voltage_allowed, "[V]"])  
  pretty_terminal_table.add_row(["", "", ""])
  
  pretty_terminal_table.add_row(["----------POST ANALYSIS-----------", "--------", "------"])
#  pretty_terminal_table.add_row(["DESIGN NUMBER OF TURNS", "-----------", "-----------"])  
#  pretty_terminal_table.add_row(["---STAGE 1 ---", "using MORE than 1 step", "obtain single turn voltage and current"])
#  pretty_terminal_table.add_row(["1 max_input_phase_current (single_turn)", max_input_phase_current, "[A]"])
#  pretty_terminal_table.add_row(["1 max_induced_phase_voltage (single_turn)", max_induced_phase_voltage, "[V]"])

  if AR and PM:
    time,fl_A,induced_phase_voltage_A,induced_phase_voltage_B=np.loadtxt('%s%s-lambda.txt' % (pathname,filename) ,skiprows=7,usecols=(0,1,4,5),unpack=True)
    
    fl_A_peak = max(fl_A)
    induced_phase_voltage_A_peak = max(induced_phase_voltage_A)
    
    induced_line_voltage_AB = np.zeros(len(induced_phase_voltage_A))
    for i in range(0,len(induced_phase_voltage_A)):
        induced_line_voltage_AB[i]=induced_phase_voltage_A[i]-induced_phase_voltage_B[i]

    induced_line_voltage_AB_peak = max(abs(max(induced_line_voltage_AB)),abs(min(induced_line_voltage_AB)))
    
    pretty_terminal_table.add_row(["----VOLTAGE---", "--------", "------"])
    pretty_terminal_table.add_row(["fl_A_peak", fl_A_peak, "[Wb]"])
    pretty_terminal_table.add_row(["peak_induced_phase_voltage", induced_phase_voltage_A_peak, "[V]"])
    pretty_terminal_table.add_row(["peak_induced_line_voltage", induced_line_voltage_AB_peak, "[V]"])  
    
    time,Tm=np.loadtxt('%s%s-Tm.txt' % (pathname,filename) ,skiprows=7,usecols=(0,1),unpack=True)

    average_torque = np.average(Tm)
    steady_state = int(len(Tm)/4)
    average_torque_ss = np.average(Tm[steady_state:])
    max_torque = np.max(Tm)
    min_torque = np.min(Tm)
    torque_ripple = ((max_torque-min_torque)/average_torque)*100

    pretty_terminal_table.add_row(["----TORQUE----", "--------", "------"])
    pretty_terminal_table.add_row(["average_torque", average_torque, "[Nm]"])
    pretty_terminal_table.add_row(["average_torque_steady_state", average_torque_ss, "[Nm]"])
    pretty_terminal_table.add_row(["torque_ripple", torque_ripple, "[%]"])
    pretty_terminal_table.add_row(["torque_pk_pk", max_torque-min_torque, "[Nm]"])
    pretty_terminal_table.add_row(["max_torque", max_torque, "[Nm]"])
    pretty_terminal_table.add_row(["min_torque", min_torque, "[Nm]"])
    
    core_loss,eddy_current_loss,excess_loss,hysteresis_loss,solid_loss,stranded_loss,stranded_loss_R=np.loadtxt('%s%s-Plosses.txt' % (pathname,filename) ,skiprows=7,usecols=(1,2,3,4,5,6,7),unpack=True)
    
    if np.isnan(core_loss[0]): core_loss = 0
    if np.isnan(eddy_current_loss[0]): eddy_current_loss = 0
    if np.isnan(excess_loss[0]): excess_loss = 0
    if np.isnan(hysteresis_loss[0]): hysteresis_loss = 0
    if np.isnan(solid_loss[0]): solid_loss = 0
    if np.isnan(stranded_loss[0]): stranded_loss = 0
    if np.isnan(stranded_loss_R[0]): stranded_loss_R = 0

    core_loss = np.average(core_loss)
    eddy_current_loss = np.average(eddy_current_loss)
    hysteresis_loss = np.average(hysteresis_loss)
    steady_state = int(len(solid_loss)/4)
    solid_loss = solid_loss[steady_state:]
    solid_loss = np.average(solid_loss)
    stranded_loss = np.average(stranded_loss)
    stranded_loss_R = np.average(stranded_loss_R)
    total_losses = core_loss+eddy_current_loss+hysteresis_loss+solid_loss+stranded_loss+stranded_loss_R

    pretty_terminal_table.add_row(["----LOSSES----", "--------", "------"])
    pretty_terminal_table.add_row(["core_loss", core_loss, "[W]"])
    pretty_terminal_table.add_row(["eddy_current_loss", eddy_current_loss, "[W]"])
    pretty_terminal_table.add_row(["hysteresis_loss", hysteresis_loss, "[W]"])
    pretty_terminal_table.add_row(["solid_loss", solid_loss, "[W]"])
    pretty_terminal_table.add_row(["stranded_loss", stranded_loss, "[W]"])
    pretty_terminal_table.add_row(["stranded_loss_R", stranded_loss_R, "[W]"])
    pretty_terminal_table.add_row(["total_losses", total_losses, "[W]"])
  
    p_out_torque = ((n_rpm*(np.pi/30)) * average_torque_ss)/1000
    p_in_indirect = p_out_torque+(total_losses/1000)
    efficiency = (p_out_torque/p_in_indirect)*100

    pretty_terminal_table.add_row(["-INPUT POWER--", "--------", "------"])
    pretty_terminal_table.add_row(["p_in_indirect", p_in_indirect, "[kW]"])
    
    pretty_terminal_table.add_row(["-OUTPUT POWER-", "--------", "------"])
    pretty_terminal_table.add_row(["p_out_torque", p_out_torque, "[kW]"])
    
    pretty_terminal_table.add_row(["--EFFICIENCY--", "--------", "------"])
    pretty_terminal_table.add_row(["efficiency", efficiency, "[%]"])
    
if plot_all == 1 and run_band_solver == 1:
    # Post Processing: Draw plots
    
    #os.chdir(pathname)
    
    #from pylab import *
    
    #rcParams['font.family'] = 'serif'
    #
    #rcParams['text.usetex'] = 'True'
    #rcParams['text.latex.preamble']='\usepackage{times}'    
    
    pl.figure(1)
    dist,Br=np.loadtxt('%s%s-Br.txt' % (pathname,filename) ,skiprows=7,usecols=(0,1),unpack=True)
    
    pl.figure(1)
    pl.plot(dist*720,Br,label="$B_r$")
    pl.xlabel('Electrical Degrees [$^\circ$]')
    pl.ylabel('Radial Flux Density [T]')
    #legend()
    pl.axis(xmax=720)
    pl.grid(True)
    
    #pl.savefig('%srn%.2fhc%.2fRadial_flux_density_distribution.pdf' % (pathname,r_n,h))
    pl.savefig('%sRadial_flux_density_distribution.pdf' % (pathname))
    
    N=np.size(Br[:-1])      # Only want 720 samples, not 721
#    Brh=2/N*abs(np.fft(Br)) # The FFT function does not include the divide by N/2
    Brh=(2/N)*abs(np.fft.rfft(Br,N)) # The FFT function does not include the divide by N/2
    h=np.arange(0,40,2)     # Because our waveform is over two periods, the harmonics is every second value
    
    print("Br|max=%g T" % max(Br))
    print("Br1   =%g T" % Brh[2]) # The third value is the first harmonic, 0=DC, 2=1st, 4=2nd, etc.
    
    pl.figure(2)
    pl.bar(h/2,Brh[h],width=0.5)
    pl.xlabel('Radial Density Harmonic Number')
    pl.ylabel('Flux Density [T]')
    
    
    pl.annotate("%.3f" % Brh[2],
             size='x-large',
             xy=(0,Brh[2]),
             xytext=(3,Brh[2]*0.95),
             arrowprops='',
             color='black',
             horizontalalignment='center')
    
    pl.savefig('%sHarmonic_spectrum.pdf' % (pathname))

if enable_terminal_printout == 1:
  print pretty_terminal_table    

text_file = open("%s%smaxwell_terminal_output.txt"%(pathname,filename), "w")
text_file.write(pretty_terminal_table.get_string())
text_file.close()    
    
conn.close()

print "Time elapsed in Maxwell 2D: %.2f [s]"%(time_mod.time()-begin)

if close_project == 1:
    oProject.Close()

#oDesktop.CloseProject(my_project_name)
#oDesktop.QuitApplication()